Slides and subtitle files for OpenSecurityTraining2 ["Architecture 1005: RISC-V Assembly"](https://ost2.fyi/Arch1005) class originally created by Xeno Kovah

Corrections to the subtitles are always welcome via merge requests
