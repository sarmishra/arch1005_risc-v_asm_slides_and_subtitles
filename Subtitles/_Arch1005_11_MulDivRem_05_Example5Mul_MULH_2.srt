1
00:00:00,199 --> 00:00:03,520
so yeah probably would have been good if

2
00:00:01,800 --> 00:00:06,000
I would have used this example earlier

3
00:00:03,520 --> 00:00:09,160
instead of that more complicated 128 bit

4
00:00:06,000 --> 00:00:11,120
time 12 128bit thing but I just come up

5
00:00:09,160 --> 00:00:12,519
with examples randomly as I thrash

6
00:00:11,120 --> 00:00:14,920
around and then I just put them in

7
00:00:12,519 --> 00:00:17,039
whatever order I found them so here we

8
00:00:14,920 --> 00:00:19,240
go this is a situation where the

9
00:00:17,039 --> 00:00:21,480
compiler generates a MULH instead of a

10
00:00:19,240 --> 00:00:23,160
MULHU and for that to occur the

11
00:00:21,480 --> 00:00:25,880
compiler has to be thinking that it is

12
00:00:23,160 --> 00:00:28,599
multiplying two signed XLEN sized

13
00:00:25,880 --> 00:00:30,400
arguments times each other now in this

14
00:00:28,599 --> 00:00:33,920
case it appears that the comp compiler

15
00:00:30,400 --> 00:00:37,160
recognized that the a2l the asky string

16
00:00:33,920 --> 00:00:39,040
to Long which is a signed long it

17
00:00:37,160 --> 00:00:41,719
recognized that the maximum value for

18
00:00:39,040 --> 00:00:45,039
this would be a 32-bit value and so

19
00:00:41,719 --> 00:00:47,360
although it's storing that into the a18

20
00:00:45,039 --> 00:00:49,879
as the initializing value it's really

21
00:00:47,360 --> 00:00:52,280
just a 32-bit value sign extended up to

22
00:00:49,879 --> 00:00:55,039
a 64-bit value sign extended up to a

23
00:00:52,280 --> 00:00:56,680
128bit value so when it subsequently

24
00:00:55,039 --> 00:00:59,079
sees that it's being multiplied by

25
00:00:56,680 --> 00:01:01,760
itself it's recognizing that well we're

26
00:00:59,079 --> 00:01:04,239
really just doing a 32-bit value times a

27
00:01:01,760 --> 00:01:06,760
32bit value but just in case that would

28
00:01:04,239 --> 00:01:09,080
have been a 32-bit signed value that

29
00:01:06,760 --> 00:01:11,080
would have got extended up to a 64-bit

30
00:01:09,080 --> 00:01:15,479
signed value that subsequently would be

31
00:01:11,080 --> 00:01:18,560
a 64 * 64 value stored into 128s it's

32
00:01:15,479 --> 00:01:21,200
going to basically just generate a mle h

33
00:01:18,560 --> 00:01:22,960
and a mle the mle H giving you the high

34
00:01:21,200 --> 00:01:25,360
order bits and the mle giving you the

35
00:01:22,960 --> 00:01:27,920
low order bits and it's optimized for

36
00:01:25,360 --> 00:01:30,360
smallest size which is why you know

37
00:01:27,920 --> 00:01:32,560
there's this intermixing of basically

38
00:01:30,360 --> 00:01:33,960
the initial like setup of the stack and

39
00:01:32,560 --> 00:01:37,360
then the tear down of the stack even

40
00:01:33,960 --> 00:01:40,399
starts before they do the final mle a0 *

41
00:01:37,360 --> 00:01:42,799
a0 where a0 was just the output of the

42
00:01:40,399 --> 00:01:45,159
a2l so basically that a0 would have been

43
00:01:42,799 --> 00:01:47,719
the S extended 64-bit version of

44
00:01:45,159 --> 00:01:52,320
whatever 32bit value it took in from the

45
00:01:47,719 --> 00:01:55,600
a to L so 64 * 64 store it back out into

46
00:01:52,320 --> 00:01:58,880
the lower bits a0 being the lower 100 of

47
00:01:55,600 --> 00:02:01,719
the 128 bits a one being the higher of

48
00:01:58,880 --> 00:02:03,680
the 128 bits so just per the ABI

49
00:02:01,719 --> 00:02:07,799
conventions placing the return values in

50
00:02:03,680 --> 00:02:10,399
a0 and a1 for a 128bit value if this was

51
00:02:07,799 --> 00:02:13,080
not a 128bit value and if you just Chang

52
00:02:10,399 --> 00:02:15,800
this to like a 64-bit value like signed

53
00:02:13,080 --> 00:02:18,160
long long you would not actually still

54
00:02:15,800 --> 00:02:19,360
get a mle h to get the upper bits

55
00:02:18,160 --> 00:02:21,680
because it would have been truncated

56
00:02:19,360 --> 00:02:24,080
down to 64 bits and a mle would have

57
00:02:21,680 --> 00:02:26,040
done the job so that's the takeaway from

58
00:02:24,080 --> 00:02:28,440
this you need to have something that can

59
00:02:26,040 --> 00:02:31,519
at least store two to the XLEN bits for

60
00:02:28,440 --> 00:02:35,360
you to see the calculation of the upper

61
00:02:31,519 --> 00:02:37,560
XLEN bits okay and that is our MULH so

62
00:02:35,360 --> 00:02:39,760
we've got M we have MULH for the high

63
00:02:37,560 --> 00:02:42,599
bits we saw previously m h for the high

64
00:02:39,760 --> 00:02:47,599
bits treating the arguments as unsigned

65
00:02:42,599 --> 00:02:47,599
and coming up next signed times unsigned

