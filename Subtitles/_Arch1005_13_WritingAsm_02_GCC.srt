1
00:00:00,480 --> 00:00:06,640
okay in this class we are going to do

2
00:00:02,760 --> 00:00:08,400
GCC inline assembly and so GCC as

3
00:00:06,640 --> 00:00:11,559
mentioned previously uses the G

4
00:00:08,400 --> 00:00:14,719
assembler gas syntax and the most basic

5
00:00:11,559 --> 00:00:17,680
form is simply ASM quotes and

6
00:00:14,719 --> 00:00:19,320
instructions separated by/ ends so it

7
00:00:17,680 --> 00:00:22,439
could be a single instruction like

8
00:00:19,320 --> 00:00:25,480
putting in an ASM quotes noop you could

9
00:00:22,439 --> 00:00:28,640
also have ASM quotes load immediate into

10
00:00:25,480 --> 00:00:30,840
t0 hex1 123 and a single instruction

11
00:00:28,640 --> 00:00:32,800
like that is perfectly valid

12
00:00:30,840 --> 00:00:36,800
but you could also have instructions

13
00:00:32,800 --> 00:00:40,719
like ASM compressed ad I

14
00:00:36,800 --> 00:00:42,840
sp-32 and then sln compressed sdsp and

15
00:00:40,719 --> 00:00:46,160
so forth so you can put multiple

16
00:00:42,840 --> 00:00:48,879
instructions and just put a explicit sln

17
00:00:46,160 --> 00:00:51,199
in the format specifier between them and

18
00:00:48,879 --> 00:00:52,960
each of them on their own line so the

19
00:00:51,199 --> 00:00:55,480
reason that I find inline assembly

20
00:00:52,960 --> 00:00:57,280
preferable to Standalone assembly is

21
00:00:55,480 --> 00:01:00,239
because it allows us to do things like

22
00:00:57,280 --> 00:01:02,079
use C variables so we can go along be

23
00:01:00,239 --> 00:01:04,320
running our normal C code and then all

24
00:01:02,079 --> 00:01:06,640
of a sudden drop down into assembly make

25
00:01:04,320 --> 00:01:09,200
use of some existing variables and then

26
00:01:06,640 --> 00:01:12,439
pop back up into highle code again so

27
00:01:09,200 --> 00:01:14,920
for the extended assembly syntax you

28
00:01:12,439 --> 00:01:17,040
will now have ASM and an assembly

29
00:01:14,920 --> 00:01:19,240
template that will again be the quotes

30
00:01:17,040 --> 00:01:20,840
and some assembly instructions but it is

31
00:01:19,240 --> 00:01:23,000
going to be a template and you can think

32
00:01:20,840 --> 00:01:24,119
of it like it's sort of like a format

33
00:01:23,000 --> 00:01:27,159
string where you're going to have

34
00:01:24,119 --> 00:01:29,200
percent signs and numbers and those will

35
00:01:27,159 --> 00:01:32,520
be filled in with these subsequent

36
00:01:29,200 --> 00:01:35,320
things subsequent things after a colon

37
00:01:32,520 --> 00:01:38,159
there will be output operand so if the

38
00:01:35,320 --> 00:01:40,640
assembly does some sort of output like

39
00:01:38,159 --> 00:01:42,920
writing to memory then the output would

40
00:01:40,640 --> 00:01:45,159
be specified here and if it does some

41
00:01:42,920 --> 00:01:47,000
sort of input then the input would be

42
00:01:45,159 --> 00:01:48,799
specified here but these are all

43
00:01:47,000 --> 00:01:51,040
optional it could have none of these as

44
00:01:48,799 --> 00:01:53,479
in the example we already saw or it

45
00:01:51,040 --> 00:01:56,240
could have no output but some input it

46
00:01:53,479 --> 00:02:00,000
could have output but no input and so

47
00:01:56,240 --> 00:02:02,159
the syntax is just quotes and whatever

48
00:02:00,000 --> 00:02:03,799
uh whatever operands are necessary

49
00:02:02,159 --> 00:02:06,200
additionally there's this list of cloud

50
00:02:03,799 --> 00:02:08,759
registers that's where you would give a

51
00:02:06,200 --> 00:02:11,120
hint to the compiler that and the

52
00:02:08,759 --> 00:02:13,319
assembler that hey I just inserted some

53
00:02:11,120 --> 00:02:15,400
little snippet of assembly and just FYI

54
00:02:13,319 --> 00:02:18,120
I'm going to smash some registers like

55
00:02:15,400 --> 00:02:20,599
t0 T1 whatever and that's a hint for the

56
00:02:18,120 --> 00:02:22,440
assembler that any code that comes

57
00:02:20,599 --> 00:02:25,120
before it or any code that comes after

58
00:02:22,440 --> 00:02:26,800
it should avoid using those registers so

59
00:02:25,120 --> 00:02:29,800
let's look at a simple example of this

60
00:02:26,800 --> 00:02:33,040
ASM example one so we start out with in

61
00:02:29,800 --> 00:02:35,560
includes standard i.h and main and we

62
00:02:33,040 --> 00:02:38,800
have a long long myar set to

63
00:02:35,560 --> 00:02:41,760
01122 and so forth then I like to put a

64
00:02:38,800 --> 00:02:44,000
little noop to signify the start of my

65
00:02:41,760 --> 00:02:46,000
inline assembly so that that when I look

66
00:02:44,000 --> 00:02:48,120
at the disassembled code I can very

67
00:02:46,000 --> 00:02:51,440
easily pick out where my code starts and

68
00:02:48,120 --> 00:02:53,920
where my code ends now here we can place

69
00:02:51,440 --> 00:02:57,360
a value into a register from a c

70
00:02:53,920 --> 00:03:01,560
variable with this syntax we have move

71
00:02:57,360 --> 00:03:04,040
and percent Z is the the source input

72
00:03:01,560 --> 00:03:06,640
and we are moving the source input into

73
00:03:04,040 --> 00:03:09,920
t0 now first of all I just want to say

74
00:03:06,640 --> 00:03:11,720
this is not valid RISC-V assembly

75
00:03:09,920 --> 00:03:13,519
this is and we know that move was a

76
00:03:11,720 --> 00:03:16,760
pseudo instruction anyways for like an

77
00:03:13,519 --> 00:03:19,319
ADDI but even still this is not valid

78
00:03:16,760 --> 00:03:21,920
RISC-V assembly because the ADDI does

79
00:03:19,319 --> 00:03:24,319
not have a form that takes a value from

80
00:03:21,920 --> 00:03:26,560
memory or anything like that and this my

81
00:03:24,319 --> 00:03:29,159
variable fundamentally is a memory

82
00:03:26,560 --> 00:03:31,840
storage location it's a local variable

83
00:03:29,159 --> 00:03:34,159
on this back so although this is not

84
00:03:31,840 --> 00:03:35,640
valid RISC-V assembly in and of itself

85
00:03:34,159 --> 00:03:38,360
it is basically you can think of it like

86
00:03:35,640 --> 00:03:41,599
a pseudo instruction or a you know just

87
00:03:38,360 --> 00:03:43,360
helper instruction that the assembler

88
00:03:41,599 --> 00:03:45,640
understands and the assembler will

89
00:03:43,360 --> 00:03:48,879
figure out whatever it needs to do in

90
00:03:45,640 --> 00:03:50,879
order to make this work so in this case

91
00:03:48,879 --> 00:03:53,519
what it would do is it'll generate

92
00:03:50,879 --> 00:03:55,720
multiple instructions and we have this

93
00:03:53,519 --> 00:03:58,640
um so we have the template here this is

94
00:03:55,720 --> 00:04:00,840
kind of our format string so that is the

95
00:03:58,640 --> 00:04:03,280
assembly template we have a colon and

96
00:04:00,840 --> 00:04:05,319
then we have the output operand which is

97
00:04:03,280 --> 00:04:07,959
none of them that is optional we don't

98
00:04:05,319 --> 00:04:10,519
have to have any but we do need to have

99
00:04:07,959 --> 00:04:12,640
the colons in this syntax so even if we

100
00:04:10,519 --> 00:04:15,200
have no output we just put an empty

101
00:04:12,640 --> 00:04:17,919
space there then we have another colon

102
00:04:15,200 --> 00:04:20,840
and now we are to the input operands and

103
00:04:17,919 --> 00:04:24,479
so the input operand is here with this R

104
00:04:20,840 --> 00:04:26,680
for register specifier and the myar to

105
00:04:24,479 --> 00:04:29,680
say that it's pulling something from the

106
00:04:26,680 --> 00:04:31,400
my variable in the C source code so the

107
00:04:29,680 --> 00:04:34,199
net result of this is that it'll take

108
00:04:31,400 --> 00:04:37,600
the value from the the local variable

109
00:04:34,199 --> 00:04:39,520
and place it into t0 then we could do

110
00:04:37,600 --> 00:04:43,400
some assembly to change the value such

111
00:04:39,520 --> 00:04:46,120
as an add I where we do t 0 plus hex1 23

112
00:04:43,400 --> 00:04:50,080
store the result back into t0 and then

113
00:04:46,120 --> 00:04:52,360
write the value out to registers so here

114
00:04:50,080 --> 00:04:54,240
again completely wrong Syntax for RISC-V

115
00:04:52,360 --> 00:04:57,759
we would expect that the output is going

116
00:04:54,240 --> 00:05:00,840
to be from left to right but no for this

117
00:04:57,759 --> 00:05:03,960
it is going to say t0 and it's moving it

118
00:05:00,840 --> 00:05:06,520
into this percent Z the percent Z is now

119
00:05:03,960 --> 00:05:10,479
filling in this zeroth argument from the

120
00:05:06,520 --> 00:05:13,800
output operands so template format

121
00:05:10,479 --> 00:05:15,680
string and colon and then the next thing

122
00:05:13,800 --> 00:05:17,960
after the colon is the output operand

123
00:05:15,680 --> 00:05:21,880
and so it's telling you set this equal

124
00:05:17,960 --> 00:05:23,199
to the myar so we will always see these

125
00:05:21,880 --> 00:05:24,639
when you're pulling things in in the

126
00:05:23,199 --> 00:05:26,319
input registers it doesn't have that

127
00:05:24,639 --> 00:05:28,680
equals and when you're writing it out to

128
00:05:26,319 --> 00:05:31,800
an output it does have that equals again

129
00:05:28,680 --> 00:05:34,639
I linked to more information about ganu

130
00:05:31,800 --> 00:05:37,440
or gas assembler extended syntax

131
00:05:34,639 --> 00:05:40,440
honestly it's hard reading and it's uh

132
00:05:37,440 --> 00:05:43,639
it's all very x86 specific like most of

133
00:05:40,440 --> 00:05:44,880
the examples are x86 specific so you

134
00:05:43,639 --> 00:05:47,280
that's why I'm just showing you you know

135
00:05:44,880 --> 00:05:49,039
a few templates to get you started that

136
00:05:47,280 --> 00:05:51,120
you can use to do most of the

137
00:05:49,039 --> 00:05:53,720
interesting stuff you might want to do

138
00:05:51,120 --> 00:05:56,319
so with this instruction or pseudo

139
00:05:53,720 --> 00:05:59,720
instruction writing the value of t0 out

140
00:05:56,319 --> 00:06:01,199
to myar then we throw a noop on to say

141
00:05:59,720 --> 00:06:03,199
when we look at the assembly okay that's

142
00:06:01,199 --> 00:06:05,639
the end of what I was writing and then

143
00:06:03,199 --> 00:06:08,759
we can do a print F and we can confirm

144
00:06:05,639 --> 00:06:11,360
that myar was actually changed and that

145
00:06:08,759 --> 00:06:14,440
it is going to be this Value plus hex1 2

146
00:06:11,360 --> 00:06:17,000
3 so let's go ahead and look at that

147
00:06:14,440 --> 00:06:19,759
based on the disassembly so all that

148
00:06:17,000 --> 00:06:22,280
code ASM example 1. C compile it with

149
00:06:19,759 --> 00:06:24,639
optimization level zero and we get this

150
00:06:22,280 --> 00:06:26,840
code I've went ahead and highlighted in

151
00:06:24,639 --> 00:06:28,919
green all of the code that this inline

152
00:06:26,840 --> 00:06:31,720
assembly is responsible for so we have

153
00:06:28,919 --> 00:06:34,240
the no and then we have this not real

154
00:06:31,720 --> 00:06:37,080
move instruction that has been turned

155
00:06:34,240 --> 00:06:39,280
into a load and a move so the assembler

156
00:06:37,080 --> 00:06:41,800
just figured out what it needed to do to

157
00:06:39,280 --> 00:06:44,960
grab the myar variable and put it into

158
00:06:41,800 --> 00:06:47,880
t0 and what it did was it pulled it from

159
00:06:44,960 --> 00:06:51,160
frame pointer minus 24 and loaded that

160
00:06:47,880 --> 00:06:54,800
to a5 frame pointer minus 4 stored right

161
00:06:51,160 --> 00:06:58,759
here coming from a5 a5 comes from this

162
00:06:54,800 --> 00:07:00,440
wepc add ey and load and so that of

163
00:06:58,759 --> 00:07:02,360
course is going to be grabbing this

164
00:07:00,440 --> 00:07:05,280
constant value that's stored somewhere

165
00:07:02,360 --> 00:07:07,400
after this code so myar is at frame

166
00:07:05,280 --> 00:07:11,080
pointer minus 24 and it pulled the value

167
00:07:07,400 --> 00:07:14,560
in put it into t0 then we have the add I

168
00:07:11,080 --> 00:07:16,599
of t0 plus 123 and storing it back into

169
00:07:14,560 --> 00:07:20,639
t0 that's exactly what we see there it's

170
00:07:16,599 --> 00:07:23,360
just a decimal hex 123 is 291 and then

171
00:07:20,639 --> 00:07:26,520
finally this again not real at all move

172
00:07:23,360 --> 00:07:28,560
instruction for it's not real valid uh

173
00:07:26,520 --> 00:07:31,280
RISC-V assembly but it turned it into

174
00:07:28,560 --> 00:07:34,039
valid assembly which is take the t0

175
00:07:31,280 --> 00:07:36,919
place it into a5 and then store the

176
00:07:34,039 --> 00:07:40,000
dword from this register back out to

177
00:07:36,919 --> 00:07:42,160
memory at frame pointer minus 24 and

178
00:07:40,000 --> 00:07:44,560
that is going to actually affect the

179
00:07:42,160 --> 00:07:47,720
myar value so that when we print it out

180
00:07:44,560 --> 00:07:50,240
later on we get a changed value okay

181
00:07:47,720 --> 00:07:54,120
let's see a slightly more real and RISC

182
00:07:50,240 --> 00:07:56,520
fiveish way to load things from a local

183
00:07:54,120 --> 00:07:59,720
C variable into a register and that is

184
00:07:56,520 --> 00:08:02,319
to instead use the m specifier M

185
00:07:59,720 --> 00:08:04,080
constraint to say this is a memory value

186
00:08:02,319 --> 00:08:06,759
and I want you to put this memory value

187
00:08:04,080 --> 00:08:09,840
that's one of my input values so it was

188
00:08:06,759 --> 00:08:12,599
template output input and this is the

189
00:08:09,840 --> 00:08:14,000
input on that side of the second colon

190
00:08:12,599 --> 00:08:17,479
and then this is going to say take the

191
00:08:14,000 --> 00:08:21,000
memory value and load it as a dword into

192
00:08:17,479 --> 00:08:23,440
the t0 register so when I compiled that

193
00:08:21,000 --> 00:08:25,639
the first thing I saw is that apparently

194
00:08:23,440 --> 00:08:28,240
the use of this memory specifier this

195
00:08:25,639 --> 00:08:30,479
memory constraint led to the inclusion

196
00:08:28,240 --> 00:08:32,200
of Stack cookies in a way that it didn't

197
00:08:30,479 --> 00:08:34,719
for the previous example the previous

198
00:08:32,200 --> 00:08:36,839
example was exactly the same except it

199
00:08:34,719 --> 00:08:39,240
used that fake move and the fake move

200
00:08:36,839 --> 00:08:41,680
and it used register specifiers R for

201
00:08:39,240 --> 00:08:43,959
register instead of M for memory so

202
00:08:41,680 --> 00:08:45,399
because of that I added in the no stack

203
00:08:43,959 --> 00:08:48,240
protector and I just threw on

204
00:08:45,399 --> 00:08:50,880
optimization level two for funsies and

205
00:08:48,240 --> 00:08:53,560
that gave us this code and this code is

206
00:08:50,880 --> 00:08:58,519
exactly what we've written here we have

207
00:08:53,560 --> 00:09:00,399
a load from memory to the t0 register so

208
00:08:58,519 --> 00:09:01,959
load and it it's up to the assembler to

209
00:09:00,399 --> 00:09:04,040
figure out where my VAR is stored in

210
00:09:01,959 --> 00:09:06,640
memory here it's now saying it's at

211
00:09:04,040 --> 00:09:09,000
stack pointer plus 8 as opposed to frame

212
00:09:06,640 --> 00:09:10,800
pointer minus 24 in the previous one so

213
00:09:09,000 --> 00:09:12,600
at optimization 2 it's just changing

214
00:09:10,800 --> 00:09:15,760
around how it's choosing to reference

215
00:09:12,600 --> 00:09:19,480
local variables so load it up into t0

216
00:09:15,760 --> 00:09:20,959
add to t0 and store it back out from t0

217
00:09:19,480 --> 00:09:23,440
one other miscellaneous thing that I

218
00:09:20,959 --> 00:09:25,760
noticed about this is that also now

219
00:09:23,440 --> 00:09:28,320
there's this print F check included

220
00:09:25,760 --> 00:09:30,040
instead of print F or put s like the

221
00:09:28,320 --> 00:09:32,160
previous example

222
00:09:30,040 --> 00:09:35,160
and what I can kind of infer here is

223
00:09:32,160 --> 00:09:38,240
that it seems like when the compiler or

224
00:09:35,160 --> 00:09:40,800
assembler sees that we've got uh the use

225
00:09:38,240 --> 00:09:42,880
of inline assembly utilizing memory all

226
00:09:40,800 --> 00:09:44,959
of a sudden it gets paranoid on us it

227
00:09:42,880 --> 00:09:47,600
adds in stack cookies and it starts

228
00:09:44,959 --> 00:09:49,279
using the print F check which is more

229
00:09:47,600 --> 00:09:52,079
security conscious and actually looks

230
00:09:49,279 --> 00:09:54,720
for uh buffer overflows occurring as a

231
00:09:52,079 --> 00:09:56,760
result of the print F so that's just a

232
00:09:54,720 --> 00:09:58,240
kind of interesting observation that's

233
00:09:56,760 --> 00:10:01,640
going on here with the use of inline

234
00:09:58,240 --> 00:10:03,680
assembly and then finally ASM example 3

235
00:10:01,640 --> 00:10:06,440
this is just here because I want to give

236
00:10:03,680 --> 00:10:09,480
you an example of how to reference

237
00:10:06,440 --> 00:10:11,240
targets like labels within code so you

238
00:10:09,480 --> 00:10:13,880
know that if you're writing C code you

239
00:10:11,240 --> 00:10:15,720
could just put a goto and then a label

240
00:10:13,880 --> 00:10:18,959
well you can do basically the same thing

241
00:10:15,720 --> 00:10:20,839
of using these labels in inline assembly

242
00:10:18,959 --> 00:10:23,480
so here for instance we're going to once

243
00:10:20,839 --> 00:10:26,320
again just grab the myar but we have

244
00:10:23,480 --> 00:10:28,040
chosen to make the myar come in from the

245
00:10:26,320 --> 00:10:30,720
command line so we can take different

246
00:10:28,040 --> 00:10:32,240
possible values so pass a command line

247
00:10:30,720 --> 00:10:34,640
value it's going to be treated as a

248
00:10:32,240 --> 00:10:38,040
signed long long it's going to be

249
00:10:34,640 --> 00:10:40,959
assumed to be base 16 load myar as

250
00:10:38,040 --> 00:10:43,279
memory up into the t0 register and then

251
00:10:40,959 --> 00:10:47,320
use the pseudo instruction branch if

252
00:10:43,279 --> 00:10:50,160
greater than zero so is t0 greater than

253
00:10:47,320 --> 00:10:51,959
zero if so go to target 1 and we don't

254
00:10:50,160 --> 00:10:54,200
have to calculate like how far away

255
00:10:51,959 --> 00:10:56,440
target 1 is we don't have to manually

256
00:10:54,200 --> 00:10:58,519
specify an immediate of the right size

257
00:10:56,440 --> 00:11:00,920
to get to target one we just say branch

258
00:10:58,519 --> 00:11:03,720
of greater than than greater than zero

259
00:11:00,920 --> 00:11:05,760
t0 and go to target 1 so target one is

260
00:11:03,720 --> 00:11:09,000
down here it's just target 1 colon just

261
00:11:05,760 --> 00:11:11,040
like a c label and so if you pass an

262
00:11:09,000 --> 00:11:12,720
input in here that is greater than one

263
00:11:11,040 --> 00:11:14,440
or greater than zero rather it's going

264
00:11:12,720 --> 00:11:17,279
to branch it's going to go to target one

265
00:11:14,440 --> 00:11:19,800
and it will print my VAR is greater than

266
00:11:17,279 --> 00:11:21,839
zero if it is not greater than zero so

267
00:11:19,800 --> 00:11:24,000
if it's a negative value or if it's

268
00:11:21,839 --> 00:11:25,959
equal to zero then instead this branch

269
00:11:24,000 --> 00:11:29,040
will not be taken it will fall through

270
00:11:25,959 --> 00:11:31,360
it will execute the code my VAR equals

271
00:11:29,040 --> 00:11:33,440
the actual value is less than or equal

272
00:11:31,360 --> 00:11:35,519
to zero and then it will fall through to

273
00:11:33,440 --> 00:11:39,000
the next assembly instruction the pseudo

274
00:11:35,519 --> 00:11:42,000
instruction J Morpheus here and it will

275
00:11:39,000 --> 00:11:44,160
jump to the target two and target 2 is

276
00:11:42,000 --> 00:11:45,560
down here and then it's done so we want

277
00:11:44,160 --> 00:11:47,480
that because we don't want it to just

278
00:11:45,560 --> 00:11:48,920
fall through and say you know it's less

279
00:11:47,480 --> 00:11:50,639
than or equal to zero and then fall

280
00:11:48,920 --> 00:11:52,320
through and print again and also it's

281
00:11:50,639 --> 00:11:54,200
greater than zero so we have to jump

282
00:11:52,320 --> 00:11:57,360
back over that to get to the end of this

283
00:11:54,200 --> 00:11:59,519
code so compiling that and we see

284
00:11:57,360 --> 00:12:01,440
basically exactly what we wrote modulo

285
00:11:59,519 --> 00:12:04,800
the fact that of course we've got these

286
00:12:01,440 --> 00:12:07,120
inline print FS so load is going to just

287
00:12:04,800 --> 00:12:09,040
load it up from stack pointer plus 8 and

288
00:12:07,120 --> 00:12:11,040
we can see you know that that was

289
00:12:09,040 --> 00:12:13,680
written to stack pointer plus 8 the

290
00:12:11,040 --> 00:12:16,240
output argument the a z coming out of

291
00:12:13,680 --> 00:12:18,320
string two long long a z is the output

292
00:12:16,240 --> 00:12:21,880
output's written to stack pointer plus 8

293
00:12:18,320 --> 00:12:24,160
so yeah that's myar load it up into t0

294
00:12:21,880 --> 00:12:26,839
check the branch if greater than zero

295
00:12:24,160 --> 00:12:30,680
and if it's greater than zero go to main

296
00:12:26,839 --> 00:12:33,279
plus 44 so main plus 44 down here and

297
00:12:30,680 --> 00:12:35,760
then it would load up again from it

298
00:12:33,279 --> 00:12:38,480
would load up myar again from the stack

299
00:12:35,760 --> 00:12:40,360
stack pointer plus 8 put it into a2

300
00:12:38,480 --> 00:12:43,279
that's going to be argument two to the

301
00:12:40,360 --> 00:12:45,480
print F argument one is that it's going

302
00:12:43,279 --> 00:12:47,560
to pull in the pointer it's going to

303
00:12:45,480 --> 00:12:50,320
calculate in argument one the pointer to

304
00:12:47,560 --> 00:12:53,240
the format string for the print F that's

305
00:12:50,320 --> 00:12:56,199
the pointer to this string right here

306
00:12:53,240 --> 00:12:57,880
and then argument zero is one but I

307
00:12:56,199 --> 00:12:59,440
literally don't know what the arguments

308
00:12:57,880 --> 00:13:01,519
to print F check are so I don't know

309
00:12:59,440 --> 00:13:04,040
what the indication of that is I'd have

310
00:13:01,519 --> 00:13:06,279
to go check read the fund manual and

311
00:13:04,040 --> 00:13:08,800
find out what the argument the zeroth

312
00:13:06,279 --> 00:13:10,600
argument to print F check is but anyways

313
00:13:08,800 --> 00:13:13,320
the point is you can see this code over

314
00:13:10,600 --> 00:13:15,279
here it's basically this code over here

315
00:13:13,320 --> 00:13:17,480
and so this is just an example of how

316
00:13:15,279 --> 00:13:19,800
you can now go and play around with

317
00:13:17,480 --> 00:13:21,800
control flow modifications by

318
00:13:19,800 --> 00:13:24,360
referencing labels as opposed to having

319
00:13:21,800 --> 00:13:26,560
to do any sort of calculation of

320
00:13:24,360 --> 00:13:29,000
offsets and the final thing that I want

321
00:13:26,560 --> 00:13:31,040
to show you about having full control

322
00:13:29,000 --> 00:13:33,360
and absolute control over exactly the

323
00:13:31,040 --> 00:13:36,360
instructions you generate is writing raw

324
00:13:33,360 --> 00:13:38,480
bytes to get assembly so you can specify

325
00:13:36,360 --> 00:13:40,680
them with the byte keyword the short

326
00:13:38,480 --> 00:13:42,600
keyword or the long keyword if you

327
00:13:40,680 --> 00:13:44,440
choose to use the byte keyword then you

328
00:13:42,600 --> 00:13:47,880
need to make sure that you write them in

329
00:13:44,440 --> 00:13:49,880
little endian order so basically if we

330
00:13:47,880 --> 00:13:53,480
do these bytes in that order then it'll

331
00:13:49,880 --> 00:13:54,800
be joller T1 T3 if we instead do a word

332
00:13:53,480 --> 00:13:56,800
then we need to make sure that it's you

333
00:13:54,800 --> 00:13:58,839
know big Endy in order so we put the

334
00:13:56,800 --> 00:14:01,040
most significant bits here and the least

335
00:13:58,839 --> 00:14:05,160
significant bits there all as one word

336
00:14:01,040 --> 00:14:06,959
that's also jll T1 T3 and if we wanted a

337
00:14:05,160 --> 00:14:09,839
compressed assembly instruction we could

338
00:14:06,959 --> 00:14:11,279
use doshort and then encode that

339
00:14:09,839 --> 00:14:14,959
instruction and this particular one

340
00:14:11,279 --> 00:14:18,040
happens to be CI stack pointer minus 32

341
00:14:14,959 --> 00:14:20,600
AKA add I stack pointer minus 32 back

342
00:14:18,040 --> 00:14:22,560
into stack pointer so you can see that

343
00:14:20,600 --> 00:14:24,519
if you think you understand how

344
00:14:22,560 --> 00:14:27,480
instruction en coding works then go

345
00:14:24,519 --> 00:14:30,440
ahead and just ASM output a word or some

346
00:14:27,480 --> 00:14:32,600
bytes or a short and then see what the

347
00:14:30,440 --> 00:14:34,279
disassembler says you've output and then

348
00:14:32,600 --> 00:14:36,920
you'll know for sure whether or not you

349
00:14:34,279 --> 00:14:38,759
encoded the instruction you expected so

350
00:14:36,920 --> 00:14:40,639
here's an example of that and we've got

351
00:14:38,759 --> 00:14:43,920
a surprise instruction for you right

352
00:14:40,639 --> 00:14:46,959
here aw we read in from the register we

353
00:14:43,920 --> 00:14:49,600
do something with thew and then we write

354
00:14:46,959 --> 00:14:50,880
it back out to the uh variable I said

355
00:14:49,600 --> 00:14:52,759
read in from a register I should have

356
00:14:50,880 --> 00:14:54,759
said read in from a variable write it

357
00:14:52,759 --> 00:14:56,480
back out to a variable it's a surprise

358
00:14:54,759 --> 00:14:58,920
except it's not that surprising because

359
00:14:56,480 --> 00:15:01,560
I'm lazy and therefore I literally just

360
00:14:58,920 --> 00:15:03,920
took the encoding for ADDI from the raw

361
00:15:01,560 --> 00:15:05,920
byes stuck it into here and we get the

362
00:15:03,920 --> 00:15:09,759
exact same code as before it is the ad

363
00:15:05,920 --> 00:15:12,560
ey hex1 23 t0 t0 you can actually even

364
00:15:09,759 --> 00:15:16,160
see the hex1 23 in the most significant

365
00:15:12,560 --> 00:15:18,000
12 bits that isn't signed immediate 12

366
00:15:16,160 --> 00:15:19,279
encoded in the assembly instruction and

367
00:15:18,000 --> 00:15:21,720
it's showing up at those most

368
00:15:19,279 --> 00:15:23,720
significant 12 Bits And just because of

369
00:15:21,720 --> 00:15:26,079
the day and age and which we live I want

370
00:15:23,720 --> 00:15:29,160
to point out that if you go off and you

371
00:15:26,079 --> 00:15:31,959
try to use a large language model to

372
00:15:29,160 --> 00:15:33,680
help you do inline assembly because most

373
00:15:31,959 --> 00:15:36,319
of the assembly out there is definitely

374
00:15:33,680 --> 00:15:39,240
going to be in Intel x86 syntax that's

375
00:15:36,319 --> 00:15:41,680
the majority of what GCC has been used

376
00:15:39,240 --> 00:15:43,959
to compile historically so you'll find

377
00:15:41,680 --> 00:15:46,720
lots and lots of examples of x86 but you

378
00:15:43,959 --> 00:15:49,000
will not find very many examples of RISC

379
00:15:46,720 --> 00:15:50,440
5 now we know that large language models

380
00:15:49,000 --> 00:15:51,839
are just trained on a whole bunch of

381
00:15:50,440 --> 00:15:54,360
data and of course that means there's

382
00:15:51,839 --> 00:15:56,160
going to be a major bias towards x86

383
00:15:54,360 --> 00:15:58,759
because its training data will be biased

384
00:15:56,160 --> 00:16:01,000
towards x86 and if you ask it to do

385
00:15:58,759 --> 00:16:04,040
something like hey I want to use the ASM

386
00:16:01,000 --> 00:16:06,800
keyword to include code and you know I

387
00:16:04,040 --> 00:16:08,759
want to use a byte assembler directive

388
00:16:06,800 --> 00:16:10,880
it'll say like or sorry what I actually

389
00:16:08,759 --> 00:16:13,040
set up here was uh what is the GCC

390
00:16:10,880 --> 00:16:15,160
inline assembly syntax to emit four

391
00:16:13,040 --> 00:16:18,800
bytes of data in instruction stream like

392
00:16:15,160 --> 00:16:21,160
hex 11122 33 44 it says hey you can use

393
00:16:18,800 --> 00:16:23,880
byte but then it gives you an example

394
00:16:21,160 --> 00:16:26,800
that literally does not even use. byte

395
00:16:23,880 --> 00:16:30,000
and it also has x86 assembly so that's

396
00:16:26,800 --> 00:16:33,040
again just showing the bias here so you

397
00:16:30,000 --> 00:16:35,440
can't expect that the llms will help you

398
00:16:33,040 --> 00:16:37,240
particularly much but as always if you

399
00:16:35,440 --> 00:16:39,279
play with it a bit you may get something

400
00:16:37,240 --> 00:16:40,839
that gets you close enough that you can

401
00:16:39,279 --> 00:16:42,319
eventually fumble your way around in the

402
00:16:40,839 --> 00:16:44,880
dark and actually figure out what you

403
00:16:42,319 --> 00:16:47,319
need okay well now we're going to do

404
00:16:44,880 --> 00:16:49,480
some instruction exercises where you go

405
00:16:47,319 --> 00:16:51,399
off and read the fun manual and write

406
00:16:49,480 --> 00:16:53,839
the fun instructions I'm going to put a

407
00:16:51,399 --> 00:16:55,160
series of exercises on the website I

408
00:16:53,839 --> 00:16:57,279
don't want to encode it in the video cuz

409
00:16:55,160 --> 00:16:59,120
I'm going to change it over time so go

410
00:16:57,279 --> 00:17:02,079
ahead and check the website for what

411
00:16:59,120 --> 00:17:02,079
you're supposed to do

