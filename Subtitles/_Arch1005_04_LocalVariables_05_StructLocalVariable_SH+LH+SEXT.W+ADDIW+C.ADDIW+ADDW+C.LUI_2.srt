1
00:00:00,719 --> 00:00:04,920
okay so what went where well if you

2
00:00:02,800 --> 00:00:06,799
stepped through the last store in the

3
00:00:04,920 --> 00:00:08,519
code you would have seen a stack that

4
00:00:06,799 --> 00:00:10,960
looks something like this we have

5
00:00:08,519 --> 00:00:13,480
alignment for our local variables we

6
00:00:10,960 --> 00:00:15,599
have alignment because of the fact that

7
00:00:13,480 --> 00:00:18,160
our total size of our local variables is

8
00:00:15,599 --> 00:00:20,720
not a multiple of hex 16 or hex 10

9
00:00:18,160 --> 00:00:23,000
rather uh decimal 16 and we have

10
00:00:20,720 --> 00:00:24,760
alignment as we've seen associated with

11
00:00:23,000 --> 00:00:27,039
our frame pointer throughout this class

12
00:00:24,760 --> 00:00:30,920
thus far then at the lowest address we

13
00:00:27,039 --> 00:00:33,680
saw short a was written with the value B

14
00:00:30,920 --> 00:00:36,640
and I'm claiming here that we have int B

15
00:00:33,680 --> 00:00:39,200
0 here and B1 up here and some alignment

16
00:00:36,640 --> 00:00:41,760
in between and you wouldn't necessarily

17
00:00:39,200 --> 00:00:43,640
be able to directly infer where B 0

18
00:00:41,760 --> 00:00:45,960
actually is so the only things we

19
00:00:43,640 --> 00:00:49,640
actually see written are B of one and we

20
00:00:45,960 --> 00:00:52,359
see that written at this address FS 8 c0

21
00:00:49,640 --> 00:00:55,760
and B of four and we see that written at

22
00:00:52,359 --> 00:00:58,800
8 C C so this is basically trying to

23
00:00:55,760 --> 00:01:00,760
show that 8 C8 would have be these lower

24
00:00:58,800 --> 00:01:02,680
things and on my diagram I show the

25
00:01:00,760 --> 00:01:05,320
lower bytes to the right and higher

26
00:01:02,680 --> 00:01:08,640
bytes to the left so

27
00:01:05,320 --> 00:01:11,600
8cc is going to be the 4 byte offset

28
00:01:08,640 --> 00:01:13,720
from 8 C8 where we will find this value

29
00:01:11,600 --> 00:01:15,680
B of four was the result of some math

30
00:01:13,720 --> 00:01:17,640
that was done and so only having the

31
00:01:15,680 --> 00:01:19,520
value of B of4 and B of one we would

32
00:01:17,640 --> 00:01:21,560
have to infer okay B of four is an

33
00:01:19,520 --> 00:01:24,920
integer size 32-bit value here that

34
00:01:21,560 --> 00:01:27,439
means B of3 must be here b of2 b of one

35
00:01:24,920 --> 00:01:29,560
and then B of Zer down here but unless

36
00:01:27,439 --> 00:01:32,600
you go off and change the code and

37
00:01:29,560 --> 00:01:35,280
change it so that for instance this uh

38
00:01:32,600 --> 00:01:38,119
writing of a to B of one if you change

39
00:01:35,280 --> 00:01:39,840
the code writing of a to B of Z then

40
00:01:38,119 --> 00:01:41,200
instead you'll see it down here and

41
00:01:39,840 --> 00:01:44,000
you'll see that there's some sort of

42
00:01:41,200 --> 00:01:46,159
alignment padding between the short a

43
00:01:44,000 --> 00:01:47,680
and the B of zero and you could imagine

44
00:01:46,159 --> 00:01:49,439
if you don't actually do that you could

45
00:01:47,680 --> 00:01:50,960
imagine the B of zero got pushed right

46
00:01:49,439 --> 00:01:53,439
up against the babe and then there was

47
00:01:50,960 --> 00:01:55,520
two bites of leftover padding here but

48
00:01:53,439 --> 00:01:57,640
if you actually change the code then you

49
00:01:55,520 --> 00:01:58,960
can go ahead and see exactly here

50
00:01:57,640 --> 00:02:00,640
doesn't really matter for now I'm just

51
00:01:58,960 --> 00:02:02,520
mentioning it because because I made a

52
00:02:00,640 --> 00:02:05,079
mistake in my original code or in my

53
00:02:02,520 --> 00:02:07,240
original stack diagram uh and that I

54
00:02:05,079 --> 00:02:09,640
didn't have B of Z here likewise you

55
00:02:07,240 --> 00:02:12,360
won't actually see anything written to B

56
00:02:09,640 --> 00:02:15,200
of five and therefore you you know only

57
00:02:12,360 --> 00:02:16,680
can realize that b of five is here based

58
00:02:15,200 --> 00:02:20,040
on the fact that b of four is there and

59
00:02:16,680 --> 00:02:21,840
the pattern is 1 2 3 four five so five

60
00:02:20,040 --> 00:02:24,400
should be there and again you have some

61
00:02:21,840 --> 00:02:26,400
sort of alignment padding on initialized

62
00:02:24,400 --> 00:02:28,800
space that's never ever written to

63
00:02:26,400 --> 00:02:30,400
before you move on to the C so what's

64
00:02:28,800 --> 00:02:33,640
the takeaway from this particular

65
00:02:30,400 --> 00:02:36,879
example the takeaway is that it seems

66
00:02:33,640 --> 00:02:38,800
that the values are on the stack in the

67
00:02:36,879 --> 00:02:41,239
exact order that they were declared in

68
00:02:38,800 --> 00:02:45,000
the structure except that it's basically

69
00:02:41,239 --> 00:02:48,840
upside down so we have a b six integers

70
00:02:45,000 --> 00:02:51,599
and then C so a b six integers and then

71
00:02:48,840 --> 00:02:53,920
C with some amount of alignment and

72
00:02:51,599 --> 00:02:55,360
padding going in between things so you

73
00:02:53,920 --> 00:02:57,239
could imagine that the compiler could

74
00:02:55,360 --> 00:02:59,120
like squish everything together and put

75
00:02:57,239 --> 00:03:01,879
no sort of padding in between things

76
00:02:59,120 --> 00:03:04,319
potentially but uh we'll come back to

77
00:03:01,879 --> 00:03:07,200
that sort of situation in future

78
00:03:04,319 --> 00:03:09,599
examples but for now let's keep track of

79
00:03:07,200 --> 00:03:14,239
what we just collected and that was the

80
00:03:09,599 --> 00:03:19,280
LH load half word sh store half word add

81
00:03:14,239 --> 00:03:23,400
IW so add immediate to a word-sized

82
00:03:19,280 --> 00:03:26,040
register and add W add two word-sized

83
00:03:23,400 --> 00:03:29,040
registers together putting it like this

84
00:03:26,040 --> 00:03:32,080
we got our halfword and halfword store

85
00:03:29,040 --> 00:03:35,400
and load we've got our adiw with the

86
00:03:32,080 --> 00:03:38,720
compressed adiw we've got our ADW with

87
00:03:35,400 --> 00:03:40,519
the compressed ADW and we picked up a

88
00:03:38,720 --> 00:03:43,560
compressed LUI to go along with our

89
00:03:40,519 --> 00:03:43,560
LUI over here

