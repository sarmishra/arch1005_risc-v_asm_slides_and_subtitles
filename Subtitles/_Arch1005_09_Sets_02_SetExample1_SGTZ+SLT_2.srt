1
00:00:00,199 --> 00:00:04,720
so the takeaway from set example one is

2
00:00:02,760 --> 00:00:07,480
that it's using the set greater than or

3
00:00:04,720 --> 00:00:09,240
the salt to optimize the condition of

4
00:00:07,480 --> 00:00:11,480
whether it's going to return one or

5
00:00:09,240 --> 00:00:13,080
whether it's going to return zero if a

6
00:00:11,480 --> 00:00:15,040
is greater than zero it's going to

7
00:00:13,080 --> 00:00:18,240
return one otherwise all the rest of the

8
00:00:15,040 --> 00:00:21,080
time return zero so basically the a z

9
00:00:18,240 --> 00:00:23,199
coming back from the a2i the a z

10
00:00:21,080 --> 00:00:25,760
register is the return register we can

11
00:00:23,199 --> 00:00:28,920
see here that there's a zero being added

12
00:00:25,760 --> 00:00:31,720
with a word with an add IW but this is

13
00:00:28,920 --> 00:00:34,360
really the same thing as sex. W which is

14
00:00:31,720 --> 00:00:36,160
just doing a word sign extension so

15
00:00:34,360 --> 00:00:38,920
basically you get the return value back

16
00:00:36,160 --> 00:00:41,840
out from string to long then you're

17
00:00:38,920 --> 00:00:43,600
going to sign extend it because we have

18
00:00:41,840 --> 00:00:46,000
an integer value that's going to be

19
00:00:43,600 --> 00:00:47,800
returned here but it's using 64-bit

20
00:00:46,000 --> 00:00:52,239
registers and then it's essentially

21
00:00:47,800 --> 00:00:55,079
doing is0 less than the return value so

22
00:00:52,239 --> 00:00:59,600
if the return value was one or two or

23
00:00:55,079 --> 00:01:02,320
three or four then it will set the a0 to

24
00:00:59,600 --> 00:01:06,560
one and it'll return one if it's any

25
00:01:02,320 --> 00:01:09,400
other value like1 -23 then instead it

26
00:01:06,560 --> 00:01:11,240
will return zero so picking up a new

27
00:01:09,400 --> 00:01:14,439
assembly instruction this is now the

28
00:01:11,240 --> 00:01:18,400
signed salt so we have salt unsigned

29
00:01:14,439 --> 00:01:20,720
salt U and salt and putting it up here

30
00:01:18,400 --> 00:01:23,680
we've got salt along with the pseudo

31
00:01:20,720 --> 00:01:26,280
instruction of set greater than zero so

32
00:01:23,680 --> 00:01:29,560
we've got two more salts to find two

33
00:01:26,280 --> 00:01:29,560
salty salts

