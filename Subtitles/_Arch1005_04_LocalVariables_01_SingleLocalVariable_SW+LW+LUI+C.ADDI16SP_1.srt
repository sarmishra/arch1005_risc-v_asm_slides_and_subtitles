1
00:00:00,840 --> 00:00:05,720
in this section we're going to focus on

2
00:00:03,280 --> 00:00:08,320
the introduction of local variables and

3
00:00:05,720 --> 00:00:10,360
seeing how they appear on the stack so

4
00:00:08,320 --> 00:00:12,559
previously we had main and it just

5
00:00:10,360 --> 00:00:14,320
returned a hard-coded constant now we're

6
00:00:12,559 --> 00:00:17,359
going to have a hardcoded constant

7
00:00:14,320 --> 00:00:21,640
scalable placed into the local variable

8
00:00:17,359 --> 00:00:25,320
I which is an assigned integer so this

9
00:00:21,640 --> 00:00:28,000
code with this compilation gives us this

10
00:00:25,320 --> 00:00:30,359
assembly and it looks like we have three

11
00:00:28,000 --> 00:00:33,719
new assembly instructions to learn about

12
00:00:30,359 --> 00:00:35,760
so the first one is s SW store word from

13
00:00:33,719 --> 00:00:37,719
register to memory so this is very

14
00:00:35,760 --> 00:00:40,559
similar to the store double word that we

15
00:00:37,719 --> 00:00:44,719
saw before same sort of form you've got

16
00:00:40,559 --> 00:00:47,559
storing of rs1 the source plus some

17
00:00:44,719 --> 00:00:49,800
signed 12bit value sign extended of

18
00:00:47,559 --> 00:00:51,960
course and then that is going to be used

19
00:00:49,800 --> 00:00:55,719
as a memory address where you're going

20
00:00:51,960 --> 00:00:58,600
to store the rs2 value so rs2 is being

21
00:00:55,719 --> 00:01:00,600
stored into word memory so word is 32

22
00:00:58,600 --> 00:01:03,480
bits word memory so if you're running

23
00:01:00,600 --> 00:01:05,680
64-bit code and rs2 is a 64-bit register

24
00:01:03,480 --> 00:01:08,040
that'll be truncated down to just the

25
00:01:05,680 --> 00:01:11,320
bottom 32bits which is what will be

26
00:01:08,040 --> 00:01:13,119
stored in memory at rs1 address plus

27
00:01:11,320 --> 00:01:14,920
immediate 12 and that's what's going to

28
00:01:13,119 --> 00:01:18,479
go in memory again unlike most

29
00:01:14,920 --> 00:01:20,799
instructions stores are red from left to

30
00:01:18,479 --> 00:01:23,200
right so this register over here is

31
00:01:20,799 --> 00:01:25,640
being moved to the right so the arrow

32
00:01:23,200 --> 00:01:28,280
goes that direction left to right and

33
00:01:25,640 --> 00:01:30,759
again parentheses in the disassembler

34
00:01:28,280 --> 00:01:32,720
these parentheses around RS one that is

35
00:01:30,759 --> 00:01:35,240
your hint that something is going to be

36
00:01:32,720 --> 00:01:37,479
used as memory and those are only with

37
00:01:35,240 --> 00:01:40,000
loads in stores so here store we're

38
00:01:37,479 --> 00:01:41,920
storing out to memory then load load

39
00:01:40,000 --> 00:01:44,479
Word same sort of thing opposite

40
00:01:41,920 --> 00:01:46,719
direction instead now we read it like

41
00:01:44,479 --> 00:01:49,640
most things from right to left so you

42
00:01:46,719 --> 00:01:52,119
take rs1 as an address add the sign

43
00:01:49,640 --> 00:01:54,719
extended 12bit immediate and you load

44
00:01:52,119 --> 00:01:56,880
that value from memory into the register

45
00:01:54,719 --> 00:01:59,360
Rd and again like most instructions this

46
00:01:56,880 --> 00:02:01,479
one is red with the arrow facing from

47
00:01:59,360 --> 00:02:03,840
right to left left and also the

48
00:02:01,479 --> 00:02:06,320
parentheses in the disassembler is your

49
00:02:03,840 --> 00:02:10,200
hint that there is memory dereferencing

50
00:02:06,320 --> 00:02:15,720
occurring and then we have LUI LUI

51
00:02:10,200 --> 00:02:18,120
Lou oh no s we gotta go yeah yeah yeah

52
00:02:15,720 --> 00:02:19,959
yeah yeah yeah that's right I reserve

53
00:02:18,120 --> 00:02:21,760
the right to break into song at any

54
00:02:19,959 --> 00:02:24,200
point in this class for any reason

55
00:02:21,760 --> 00:02:26,040
should I see the need so what is LUI

56
00:02:24,200 --> 00:02:28,480
LUI is load upper immediate and what

57
00:02:26,040 --> 00:02:31,800
do we do with it we use it to create

58
00:02:28,480 --> 00:02:34,000
32-bit immediate we saw that Li before

59
00:02:31,800 --> 00:02:36,720
load immediate can only load the bottom

60
00:02:34,000 --> 00:02:39,319
12 bits of a register well this is how

61
00:02:36,720 --> 00:02:41,080
we're going to load the upper 20 bits so

62
00:02:39,319 --> 00:02:42,440
I mentioned this sort of syntax earlier

63
00:02:41,080 --> 00:02:44,560
in the class but this is the first time

64
00:02:42,440 --> 00:02:47,120
we're actually seeing it the use of this

65
00:02:44,560 --> 00:02:49,080
uppercase U is going to be what I will

66
00:02:47,120 --> 00:02:51,920
use to indicate that we are loading

67
00:02:49,080 --> 00:02:55,319
upper bits of some register so upper

68
00:02:51,920 --> 00:02:57,519
immediate 20 bits and so I chose to draw

69
00:02:55,319 --> 00:03:00,680
this sort of like this so it is loading

70
00:02:57,519 --> 00:03:02,440
the upper immediate 20 bits into Rd some

71
00:03:00,680 --> 00:03:05,159
destination register and specifically

72
00:03:02,440 --> 00:03:07,640
those 20 bits are filling in bits 31

73
00:03:05,159 --> 00:03:10,560
through 12 so those are the upper 20

74
00:03:07,640 --> 00:03:13,280
bits so it sets the upper 20 bits and

75
00:03:10,560 --> 00:03:15,799
then implicitly it also sets the lower

76
00:03:13,280 --> 00:03:18,000
12 bits to zero so that means if you're

77
00:03:15,799 --> 00:03:20,840
trying to create a 32-bit constant you

78
00:03:18,000 --> 00:03:23,400
need to do a LUI and then an ADDI to

79
00:03:20,840 --> 00:03:25,440
set the bottom 12 bits not an ADDI and

80
00:03:23,400 --> 00:03:27,360
then a LUI because the LUI would

81
00:03:25,440 --> 00:03:31,040
wipe out the bottom 12 bits and set them

82
00:03:27,360 --> 00:03:33,560
to zero so LUI and Ed is how you set a

83
00:03:31,040 --> 00:03:36,959
32-bit value into a register whether

84
00:03:33,560 --> 00:03:39,120
it's a 64 32-bit register and spoiler

85
00:03:36,959 --> 00:03:40,959
alert shifts are how you can create the

86
00:03:39,120 --> 00:03:43,480
full 64 bits but we'll learn about

87
00:03:40,959 --> 00:03:45,959
shifts later in the class okay so now it

88
00:03:43,480 --> 00:03:48,599
is time for you to go off and draw your

89
00:03:45,959 --> 00:03:53,040
own stack diagram so I'm going to want

90
00:03:48,599 --> 00:03:57,000
you to go and put single local variable

91
00:03:53,040 --> 00:03:58,879
o0 into GDB and I want you to debug

92
00:03:57,000 --> 00:04:00,560
through it like single step through it

93
00:03:58,879 --> 00:04:02,680
and keep track of of what's going on on

94
00:04:00,560 --> 00:04:04,799
the stack and fill in values as you see

95
00:04:02,680 --> 00:04:06,360
them I'm giving you this as an initial

96
00:04:04,799 --> 00:04:08,400
starting state you should see this if

97
00:04:06,360 --> 00:04:10,239
you don't see this then something

98
00:04:08,400 --> 00:04:11,599
probably went wrong so your initial

99
00:04:10,239 --> 00:04:15,360
starting point is your frame pointer

100
00:04:11,599 --> 00:04:17,600
should be pointing at s fs and then a88

101
00:04:15,360 --> 00:04:20,880
your initial stack pointer should be 7

102
00:04:17,600 --> 00:04:22,680
fs and then 90 there's going to be a

103
00:04:20,880 --> 00:04:24,880
value there at the stack pointer but we

104
00:04:22,680 --> 00:04:27,120
don't actually care about that we care

105
00:04:24,880 --> 00:04:28,960
that's already set before main executes

106
00:04:27,120 --> 00:04:30,840
a single assembly instruction we're

107
00:04:28,960 --> 00:04:33,759
going to care about the stuff that

108
00:04:30,840 --> 00:04:35,440
starts out at lower addresses below

109
00:04:33,759 --> 00:04:36,639
where the stack pointer starts because

110
00:04:35,440 --> 00:04:38,680
the stack pointer is going to

111
00:04:36,639 --> 00:04:40,360
necessarily be manipulated by main the

112
00:04:38,680 --> 00:04:42,919
frame pointer is going to be manipulated

113
00:04:40,360 --> 00:04:45,039
by main so I want you to draw a diagram

114
00:04:42,919 --> 00:04:47,600
that keeps track of what gets written

115
00:04:45,039 --> 00:04:49,759
where on the stack and what is left

116
00:04:47,600 --> 00:04:52,639
uninitialized and that's not actually

117
00:04:49,759 --> 00:04:54,520
touched and basically after you see all

118
00:04:52,639 --> 00:04:55,919
of the rights to the stack occur you

119
00:04:54,520 --> 00:04:59,120
should have a pretty good idea of what

120
00:04:55,919 --> 00:05:01,560
this looks like so go ahead and stop now

121
00:04:59,120 --> 00:05:04,039
step through the assembly in GTB and

122
00:05:01,560 --> 00:05:06,880
draw your stock diagram and we'll come

123
00:05:04,039 --> 00:05:08,639
back and see what it should look like

124
00:05:06,880 --> 00:05:11,160
now I don't care how you draw your stack

125
00:05:08,639 --> 00:05:13,199
diagram you could draw it on paper if

126
00:05:11,160 --> 00:05:16,039
you'd like you could make a text file

127
00:05:13,199 --> 00:05:18,400
and do it in asky or I gave a template

128
00:05:16,039 --> 00:05:20,479
form on the website where you can fill

129
00:05:18,400 --> 00:05:22,240
it in to just have a nicer looking

130
00:05:20,479 --> 00:05:23,600
version but one way or another you're

131
00:05:22,240 --> 00:05:26,039
going to be drawing a lot of Stack

132
00:05:23,600 --> 00:05:29,080
diagrams in this class so time to get

133
00:05:26,039 --> 00:05:29,080
started on it

