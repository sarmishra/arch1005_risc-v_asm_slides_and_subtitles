1
00:00:00,160 --> 00:00:03,639
now we don't want to count our chickens

2
00:00:02,000 --> 00:00:06,759
before they've been dashed with a

3
00:00:03,639 --> 00:00:08,880
Sprinkle of chaos magic so running that

4
00:00:06,759 --> 00:00:10,840
we see that behind the scenes the silly

5
00:00:08,880 --> 00:00:14,559
and cly were actually encoded by

6
00:00:10,840 --> 00:00:14,559
compressed silly and compressed

7
00:00:14,599 --> 00:00:20,400
sirly so the takeaways from this example

8
00:00:18,000 --> 00:00:22,960
is that when you have the left shift C

9
00:00:20,400 --> 00:00:24,240
operator you're going to see silly and

10
00:00:22,960 --> 00:00:27,000
when you have the right shift you're

11
00:00:24,240 --> 00:00:29,560
going to see sirly but specifically

12
00:00:27,000 --> 00:00:31,240
these shifting by an immediate implies

13
00:00:29,560 --> 00:00:33,719
that the compiler must know what the

14
00:00:31,240 --> 00:00:35,640
immediate is at compile time and indeed

15
00:00:33,719 --> 00:00:38,040
we can see that here is the four here is

16
00:00:35,640 --> 00:00:39,399
the three because the compiler knew that

17
00:00:38,040 --> 00:00:41,719
these were going to be the values

18
00:00:39,399 --> 00:00:43,600
specifically used for this code if the

19
00:00:41,719 --> 00:00:45,039
compiler didn't know the value ahead of

20
00:00:43,600 --> 00:00:45,760
time then it wouldn't be able to use

21
00:00:45,039 --> 00:00:48,440
these

22
00:00:45,760 --> 00:00:50,920
instructions so we pick up silly and

23
00:00:48,440 --> 00:00:53,600
sirly but we also have silly and sirly

24
00:00:50,920 --> 00:00:56,120
over here so there's a 64-bit version

25
00:00:53,600 --> 00:00:59,000
and there is a 32-bit version why do you

26
00:00:56,120 --> 00:01:01,079
have the two same named things in two

27
00:00:59,000 --> 00:01:03,079
different columns when we know that all

28
00:01:01,079 --> 00:01:05,960
40 of these assembly instructions should

29
00:01:03,079 --> 00:01:09,799
be ostensibly usable as a subset of the

30
00:01:05,960 --> 00:01:12,840
RV64I the answer is because again when

31
00:01:09,799 --> 00:01:14,280
it's silly being operated in 64-bit mode

32
00:01:12,840 --> 00:01:16,920
it's assuming that it's operating on

33
00:01:14,280 --> 00:01:19,880
64-bit registers and therefore the shift

34
00:01:16,920 --> 00:01:21,880
is having six bits to encode the

35
00:01:19,880 --> 00:01:24,640
immediate rather than only five when

36
00:01:21,880 --> 00:01:26,240
you're operating on 32-bit registers so

37
00:01:24,640 --> 00:01:28,640
whereas all these other instructions

38
00:01:26,240 --> 00:01:30,520
like add and subtract and xor and or and

39
00:01:28,640 --> 00:01:32,600
and all of these other instructions can

40
00:01:30,520 --> 00:01:34,119
be used in 64-bit mode and they just

41
00:01:32,600 --> 00:01:36,119
kind of do the right thing because

42
00:01:34,119 --> 00:01:39,200
they're defined in terms of X length the

43
00:01:36,119 --> 00:01:41,960
32 or 64-bit registers but because these

44
00:01:39,200 --> 00:01:45,159
need to do a different uh indication of

45
00:01:41,960 --> 00:01:47,680
how many bits to shift based on 32 or 64

46
00:01:45,159 --> 00:01:49,920
they are actually different instructions

47
00:01:47,680 --> 00:01:51,799
even though they share the same names

48
00:01:49,920 --> 00:01:54,520
all right and on our diagram we're going

49
00:01:51,799 --> 00:01:57,119
to start a new section here for shifts

50
00:01:54,520 --> 00:01:58,439
and we're going to collect uh 18 shifts

51
00:01:57,119 --> 00:02:01,159
before we're all done with it these

52
00:01:58,439 --> 00:02:03,759
count as two the 32 and 64-bit version

53
00:02:01,159 --> 00:02:06,000
32 and 64-bit version and then we've got

54
00:02:03,759 --> 00:02:09,000
our compressed silly and compressed

55
00:02:06,000 --> 00:02:09,000
sirly

