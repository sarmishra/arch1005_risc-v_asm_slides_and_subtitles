1
00:00:00,160 --> 00:00:04,879
moving away from the IF examples of

2
00:00:02,520 --> 00:00:07,600
control flow we move on to switch

3
00:00:04,879 --> 00:00:10,160
examples here we have pulling in some

4
00:00:07,600 --> 00:00:12,719
value from the command line we're going

5
00:00:10,160 --> 00:00:14,679
to put it into an integer a then we're

6
00:00:12,719 --> 00:00:16,920
going to run switch on that and if it's

7
00:00:14,679 --> 00:00:19,080
zero return one if it's one return two

8
00:00:16,920 --> 00:00:21,320
and if it's anything else return three

9
00:00:19,080 --> 00:00:24,439
so of course we can't actually get to

10
00:00:21,320 --> 00:00:26,760
this return of fied compile that as

11
00:00:24,439 --> 00:00:29,439
optimization level zero and we come up

12
00:00:26,760 --> 00:00:33,120
with this two new assembly instructions

13
00:00:29,439 --> 00:00:36,840
be qz and beq let's go see what those

14
00:00:33,120 --> 00:00:39,559
are starting with beq this is the branch

15
00:00:36,840 --> 00:00:44,000
if equal and so it's very simple it's

16
00:00:39,559 --> 00:00:46,559
just is rs1 equal to rs2 if so branch as

17
00:00:44,000 --> 00:00:48,960
always the offset is not going to be

18
00:00:46,559 --> 00:00:50,480
given as a relative displacement you're

19
00:00:48,960 --> 00:00:52,840
going to see an absolute address that

20
00:00:50,480 --> 00:00:54,760
the disassembler shows you so it behaves

21
00:00:52,840 --> 00:00:56,399
like all the other branches and the only

22
00:00:54,760 --> 00:00:59,359
thing you need to know is how to

23
00:00:56,399 --> 00:01:02,039
translate the neonic branch if equal

24
00:00:59,359 --> 00:01:04,600
then we have branch if equal to zero and

25
00:01:02,039 --> 00:01:06,520
so same idea but just specifically

26
00:01:04,600 --> 00:01:09,560
checking if it's zero consequently

27
00:01:06,520 --> 00:01:11,720
there's only one operand past here RS

28
00:01:09,560 --> 00:01:15,119
your source and if that source is equal

29
00:01:11,720 --> 00:01:16,759
to zero then do the branch but as you

30
00:01:15,119 --> 00:01:20,040
might have guessed already only one

31
00:01:16,759 --> 00:01:23,159
operand branch if equal to zero is a bad

32
00:01:20,040 --> 00:01:26,439
reptile this is a pseudo instruction and

33
00:01:23,159 --> 00:01:29,680
it is just a branch if equal with the

34
00:01:26,439 --> 00:01:32,320
register source 2 set to zero hardcoded

35
00:01:29,680 --> 00:01:35,960
is Z and so let's go back to our branch

36
00:01:32,320 --> 00:01:38,720
if equal so you just say your source one

37
00:01:35,960 --> 00:01:41,040
hardcode in a zero here if your source

38
00:01:38,720 --> 00:01:42,840
equal to zero well that's a branch if

39
00:01:41,040 --> 00:01:46,119
equal to

40
00:01:42,840 --> 00:01:48,719
zero so it turns out that that makes

41
00:01:46,119 --> 00:01:50,719
stack cookies unlocked that bad reptile

42
00:01:48,719 --> 00:01:52,240
right there branch if equal to zero that

43
00:01:50,719 --> 00:01:54,680
turns out to be the last assembly

44
00:01:52,240 --> 00:01:57,159
instruction we need in order to be able

45
00:01:54,680 --> 00:01:59,399
to see stack cookies in the future we're

46
00:01:57,159 --> 00:02:01,479
not going to see stack cookies now but

47
00:01:59,399 --> 00:02:03,520
in the past in this class I would

48
00:02:01,479 --> 00:02:05,680
compile with explicitly turning off

49
00:02:03,520 --> 00:02:07,520
stack cookies because I said you didn't

50
00:02:05,680 --> 00:02:09,759
have the knowledge of the necessary

51
00:02:07,520 --> 00:02:11,280
assembly instructions in order to

52
00:02:09,759 --> 00:02:13,200
understand the code so we've got

53
00:02:11,280 --> 00:02:15,879
everything we need now we will come back

54
00:02:13,200 --> 00:02:18,160
to those stack cookies later so

55
00:02:15,879 --> 00:02:19,920
therefore you should go and step through

56
00:02:18,160 --> 00:02:22,239
this code now make sure you understand

57
00:02:19,920 --> 00:02:24,400
what's going on with the branch if equal

58
00:02:22,239 --> 00:02:26,239
branch if equal to zero because you'll

59
00:02:24,400 --> 00:02:29,599
definitely need that later on to

60
00:02:26,239 --> 00:02:29,599
understand stack cookies

