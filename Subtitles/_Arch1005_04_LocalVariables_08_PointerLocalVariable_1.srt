1
00:00:00,120 --> 00:00:04,319
I wanted to cover at least one example

2
00:00:02,000 --> 00:00:06,319
where we used some pointers and saw how

3
00:00:04,319 --> 00:00:08,120
those look on the stack because pointers

4
00:00:06,319 --> 00:00:09,400
are definitely one of the harder things

5
00:00:08,120 --> 00:00:11,920
to wrap your head around when you're

6
00:00:09,400 --> 00:00:14,480
first learning the C language so in this

7
00:00:11,920 --> 00:00:17,320
example we have long long a which will

8
00:00:14,480 --> 00:00:19,439
be initialized to all A's integer B

9
00:00:17,320 --> 00:00:21,720
which will be initialized to all B's and

10
00:00:19,439 --> 00:00:24,039
then we have a long long pointer a

11
00:00:21,720 --> 00:00:26,880
pointer which gets the address of a

12
00:00:24,039 --> 00:00:29,480
stored into this pointer local variable

13
00:00:26,880 --> 00:00:32,239
and we have an integer pointer B pointer

14
00:00:29,480 --> 00:00:34,960
which gets the address of B stored into

15
00:00:32,239 --> 00:00:37,079
this local variable and then we do some

16
00:00:34,960 --> 00:00:39,640
pointer arithmetic for no reason in

17
00:00:37,079 --> 00:00:42,000
particular when we compile this then

18
00:00:39,640 --> 00:00:43,600
we're going to get this code right here

19
00:00:42,000 --> 00:00:45,559
and there are no new assembly

20
00:00:43,600 --> 00:00:47,239
instructions which means you know

21
00:00:45,559 --> 00:00:49,920
everything you need to know to go

22
00:00:47,239 --> 00:00:52,039
understand this code so time for you to

23
00:00:49,920 --> 00:00:56,600
compile up this pointer local variable

24
00:00:52,039 --> 00:00:58,879
o0 no cookie and initialize your stack

25
00:00:56,600 --> 00:01:01,120
and fill in exactly what goes where

26
00:00:58,879 --> 00:01:03,399
where is a where is B where is a pointer

27
00:01:01,120 --> 00:01:06,119
where is B pointer to do that you know

28
00:01:03,399 --> 00:01:09,479
what to do go to the code and step

29
00:01:06,119 --> 00:01:09,479
through the assembly

