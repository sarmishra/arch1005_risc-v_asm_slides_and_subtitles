1
00:00:00,480 --> 00:00:06,200
now for a couple of reminders going back

2
00:00:03,120 --> 00:00:08,840
to array local variable 2 that was the

3
00:00:06,200 --> 00:00:11,480
case where we basically took and changed

4
00:00:08,840 --> 00:00:14,920
this from being a six element long long

5
00:00:11,480 --> 00:00:17,119
array to being three followed by a b

6
00:00:14,920 --> 00:00:19,800
followed by another three element array

7
00:00:17,119 --> 00:00:22,279
and what we took away from that is that

8
00:00:19,800 --> 00:00:24,359
the compiler can go ahead and reorder

9
00:00:22,279 --> 00:00:27,439
these local variables if it likes they

10
00:00:24,359 --> 00:00:29,359
don't have to be exactly the order that

11
00:00:27,439 --> 00:00:31,759
they were declared in the C highle

12
00:00:29,359 --> 00:00:34,640
source code then in our previous example

13
00:00:31,759 --> 00:00:37,480
struct local variable o0 no cookie we

14
00:00:34,640 --> 00:00:39,719
saw that basically the order of the

15
00:00:37,480 --> 00:00:42,559
values on the stack was exactly the

16
00:00:39,719 --> 00:00:45,039
order of the struct it was upside down

17
00:00:42,559 --> 00:00:49,480
but otherwise everything was the same a

18
00:00:45,039 --> 00:00:51,480
b c a b c so what we want to do in this

19
00:00:49,480 --> 00:00:53,480
upcoming example is we want to play the

20
00:00:51,480 --> 00:00:55,559
same game that we played in the previous

21
00:00:53,480 --> 00:00:57,800
array local variable we're going to

22
00:00:55,559 --> 00:00:59,600
still have a as a short as our first

23
00:00:57,800 --> 00:01:02,320
thing so we expect that'll probably

24
00:00:59,600 --> 00:01:04,199
still go there but now with the b

25
00:01:02,320 --> 00:01:05,920
instead of having six integers we're

26
00:01:04,199 --> 00:01:08,000
going to change it to being six long

27
00:01:05,920 --> 00:01:10,320
Longs and then we're going to split it

28
00:01:08,000 --> 00:01:12,200
by putting the C and we want to see is

29
00:01:10,320 --> 00:01:15,040
the compiler going to put it exactly the

30
00:01:12,200 --> 00:01:16,640
order of Three B's and then the C and

31
00:01:15,040 --> 00:01:18,400
then the D because we know that c is

32
00:01:16,640 --> 00:01:21,240
going to have this Balboa bled blood

33
00:01:18,400 --> 00:01:23,600
which will be our sort of trigger of is

34
00:01:21,240 --> 00:01:25,600
things reordered is it been uh placed in

35
00:01:23,600 --> 00:01:26,960
the middle of these things and then just

36
00:01:25,600 --> 00:01:28,960
for good measure I'm going to go ahead

37
00:01:26,960 --> 00:01:31,000
and throw in a bunch of extra stuff just

38
00:01:28,960 --> 00:01:33,000
to see what's sort of effects that has

39
00:01:31,000 --> 00:01:35,360
on padding and placement of all the

40
00:01:33,000 --> 00:01:37,600
various things so here is our new and

41
00:01:35,360 --> 00:01:40,960
more complicated struct local variable

42
00:01:37,600 --> 00:01:43,680
to we defined the my struct now as

43
00:01:40,960 --> 00:01:45,560
having the short the three element array

44
00:01:43,680 --> 00:01:48,159
single scaler three element array and

45
00:01:45,560 --> 00:01:50,200
extra stuff then we do a variety of

46
00:01:48,159 --> 00:01:52,840
initializations with values that are

47
00:01:50,200 --> 00:01:54,880
just there to help you see what's going

48
00:01:52,840 --> 00:01:57,039
on to the stack whereare so these are

49
00:01:54,880 --> 00:02:00,159
obviously just to help you out and

50
00:01:57,039 --> 00:02:04,680
figure out which area on the stack is B

51
00:02:00,159 --> 00:02:06,280
of Z which area is C which area is a to

52
00:02:04,680 --> 00:02:09,000
obviously it won't exactly help you

53
00:02:06,280 --> 00:02:10,599
differentiate B of0 from 1 from two but

54
00:02:09,000 --> 00:02:12,959
you could change that if you'd like to

55
00:02:10,599 --> 00:02:15,280
figure it out more concretely all right

56
00:02:12,959 --> 00:02:17,200
popping up our new assembly for this

57
00:02:15,280 --> 00:02:18,920
compilation and we get a few new

58
00:02:17,200 --> 00:02:20,959
assembly instructions but the text is

59
00:02:18,920 --> 00:02:24,440
getting a little small there so let's

60
00:02:20,959 --> 00:02:28,200
zoom on in and see that we have a new SB

61
00:02:24,440 --> 00:02:30,400
lhu and lbu so what are those assembly

62
00:02:28,200 --> 00:02:32,959
instructions now as a reminder in the

63
00:02:30,400 --> 00:02:35,840
previous section we saw LH load half

64
00:02:32,959 --> 00:02:38,879
word and it took an immediate added it

65
00:02:35,840 --> 00:02:41,159
to the register and then loaded the

66
00:02:38,879 --> 00:02:44,400
value from memory a half word value from

67
00:02:41,159 --> 00:02:47,519
memory into the destination register and

68
00:02:44,400 --> 00:02:49,560
ultimately s extended it to 64 bits

69
00:02:47,519 --> 00:02:52,400
therefore it should be relatively easy

70
00:02:49,560 --> 00:02:55,920
for us to understand lhu because this is

71
00:02:52,400 --> 00:02:57,920
now load half word unsigned so unsigned

72
00:02:55,920 --> 00:02:59,840
from memory to register so the key

73
00:02:57,920 --> 00:03:02,000
difference is it's going to do zero

74
00:02:59,840 --> 00:03:04,680
extension instead of s extension because

75
00:03:02,000 --> 00:03:07,840
it is treating that 16 bits as an

76
00:03:04,680 --> 00:03:09,920
unsigned value so same way that it's

77
00:03:07,840 --> 00:03:13,159
written and it's a load assembly

78
00:03:09,920 --> 00:03:15,080
instruction so know that loads collect

79
00:03:13,159 --> 00:03:17,519
memory from this side and load it into a

80
00:03:15,080 --> 00:03:20,400
register so it loads the halfword or

81
00:03:17,519 --> 00:03:24,440
16bit value from the memory address that

82
00:03:20,400 --> 00:03:27,000
is defined as rs1 plus immediate 12 the

83
00:03:24,440 --> 00:03:29,159
immediate 12 itself is still a signed

84
00:03:27,000 --> 00:03:31,680
value so that will be sign extended and

85
00:03:29,159 --> 00:03:34,599
added to the rs2 so you could be plus or

86
00:03:31,680 --> 00:03:36,439
minus calculate a memory address but

87
00:03:34,599 --> 00:03:38,159
then when you pull it in and store it in

88
00:03:36,439 --> 00:03:40,640
the destination it should be zero

89
00:03:38,159 --> 00:03:43,439
extended should treat just be treated as

90
00:03:40,640 --> 00:03:45,319
unsigned and just like with LH when

91
00:03:43,439 --> 00:03:47,120
you're running 32-bit code the zero

92
00:03:45,319 --> 00:03:49,319
extension to 32 bits you're done and

93
00:03:47,120 --> 00:03:52,040
when you're running 64bit zero extended

94
00:03:49,319 --> 00:03:53,840
to 64 bits and you're done now I am a

95
00:03:52,040 --> 00:03:56,040
broken record and I thought about

96
00:03:53,840 --> 00:03:57,760
deleting this the portion where it says

97
00:03:56,040 --> 00:03:59,720
like most instructions this one is read

98
00:03:57,760 --> 00:04:01,640
from right to left but then I thought

99
00:03:59,720 --> 00:04:03,239
thought well what if someone just comes

100
00:04:01,640 --> 00:04:05,760
and looks at this thing in the

101
00:04:03,239 --> 00:04:07,799
non-animated form in the PDF exported

102
00:04:05,760 --> 00:04:10,879
form I want them to still understand

103
00:04:07,799 --> 00:04:12,280
what's going on so I kept these uh extra

104
00:04:10,879 --> 00:04:14,200
things that I've said for every single

105
00:04:12,280 --> 00:04:16,519
load in every single store throughout

106
00:04:14,200 --> 00:04:19,320
the class just because someone may see

107
00:04:16,519 --> 00:04:21,120
it in the exported class materials and

108
00:04:19,320 --> 00:04:24,080
also should always remember that

109
00:04:21,120 --> 00:04:25,919
reputation is the basis of

110
00:04:24,080 --> 00:04:28,960
memorization okay so now we've got a

111
00:04:25,919 --> 00:04:30,960
store stores store from the left to the

112
00:04:28,960 --> 00:04:34,240
right and this is going to take the

113
00:04:30,960 --> 00:04:36,919
bottom byte store byte take the bottom

114
00:04:34,240 --> 00:04:40,199
byte of rs2 the bottom 8 Bits and store

115
00:04:36,919 --> 00:04:42,880
it to memory where in memory well at rs1

116
00:04:40,199 --> 00:04:45,000
plus sign extended immediate 12 that's

117
00:04:42,880 --> 00:04:47,680
going to be a byte memory address and

118
00:04:45,000 --> 00:04:50,600
again it is stored from left to right

119
00:04:47,680 --> 00:04:53,039
and again parentheses means grab from

120
00:04:50,600 --> 00:04:55,360
memory okay and then there is load byte

121
00:04:53,039 --> 00:04:57,639
unsign so just like load half word

122
00:04:55,360 --> 00:04:59,800
unsigned this is going to use zero

123
00:04:57,639 --> 00:05:01,680
extension rather than sign extension and

124
00:04:59,800 --> 00:05:03,840
it's loading a byte so it is only

125
00:05:01,680 --> 00:05:07,160
grabbing 8 Bits And then zero extending

126
00:05:03,840 --> 00:05:09,960
it to 32 and subsequently 64 bits so

127
00:05:07,160 --> 00:05:11,800
load we calculate a memory address the

128
00:05:09,960 --> 00:05:15,199
immediate is still a sign extended

129
00:05:11,800 --> 00:05:17,240
immediate so rs1 plus sin extended 12

130
00:05:15,199 --> 00:05:19,919
get that as a memory address and pluck

131
00:05:17,240 --> 00:05:23,240
out a byte and zero extended and stick

132
00:05:19,919 --> 00:05:27,400
it into Rd duplicate duplicate same

133
00:05:23,240 --> 00:05:29,880
things as always all right great so time

134
00:05:27,400 --> 00:05:32,120
for you to figure out where all all of

135
00:05:29,880 --> 00:05:34,319
that stuff is stored on the stack and

136
00:05:32,120 --> 00:05:36,440
you do it like you always do it by

137
00:05:34,319 --> 00:05:41,479
stopping stepping through the assembly

138
00:05:36,440 --> 00:05:41,479
and drawing yourself a stack diagram

