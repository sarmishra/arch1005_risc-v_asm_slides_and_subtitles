1
00:00:00,080 --> 00:00:04,359
but what's this now we're not talking

2
00:00:02,120 --> 00:00:07,000
about sign we're talking about size and

3
00:00:04,359 --> 00:00:10,000
the power of size is a Curious Thing

4
00:00:07,000 --> 00:00:12,080
make some men weep make another man sing

5
00:00:10,000 --> 00:00:14,040
don't think too hard about those lyrics

6
00:00:12,080 --> 00:00:16,400
All Right Moving On so let's change the

7
00:00:14,040 --> 00:00:18,840
size from an unsigned long to an INT an

8
00:00:16,400 --> 00:00:21,080
integer 32-bit value instead of a 64-bit

9
00:00:18,840 --> 00:00:23,920
value what are the effects going to be

10
00:00:21,080 --> 00:00:26,480
on the code so here's our new code with

11
00:00:23,920 --> 00:00:32,200
in instead of Longs and we get the

12
00:00:26,480 --> 00:00:35,600
following slw surl w SRA W what's the

13
00:00:32,200 --> 00:00:38,920
difference with those shifts well the s

14
00:00:35,600 --> 00:00:41,760
w is shift left logical as usual but now

15
00:00:38,920 --> 00:00:44,760
it is a shift by register with a word

16
00:00:41,760 --> 00:00:48,920
size register so the W suffix like we

17
00:00:44,760 --> 00:00:53,000
had seen before for ADW and subw is used

18
00:00:48,920 --> 00:00:55,399
when RV64I code wants to use word-sized

19
00:00:53,000 --> 00:00:57,879
registers the 32-bit registers instead

20
00:00:55,399 --> 00:00:59,559
of the full 64-bit register size now of

21
00:00:57,879 --> 00:01:02,039
course the reason why this came up was

22
00:00:59,559 --> 00:01:05,840
because we were using ins not longs

23
00:01:02,039 --> 00:01:08,400
32-bit values so s w is just going to be

24
00:01:05,840 --> 00:01:11,560
like we've seen before we have a source

25
00:01:08,400 --> 00:01:13,360
register a source 2 and now because we

26
00:01:11,560 --> 00:01:15,400
specifically are using word sizes

27
00:01:13,360 --> 00:01:18,040
there's no longer a question of is it

28
00:01:15,400 --> 00:01:19,880
shifting by the bottom five bits or the

29
00:01:18,040 --> 00:01:22,400
bottom six bits based on whether you're

30
00:01:19,880 --> 00:01:24,119
doing 32 or 64-bit it's always treating

31
00:01:22,400 --> 00:01:27,360
it as a word siiz register so it's

32
00:01:24,119 --> 00:01:29,119
always the bottom five bits of rs2 are

33
00:01:27,360 --> 00:01:31,360
interpreted as the number of bits to

34
00:01:29,119 --> 00:01:34,040
shift so we've got the source and

35
00:01:31,360 --> 00:01:35,680
destination and like I want to emphasize

36
00:01:34,040 --> 00:01:38,520
and the destination is going to be

37
00:01:35,680 --> 00:01:40,600
treated as a word siiz register and it's

38
00:01:38,520 --> 00:01:43,360
ultimately going to do the math as if

39
00:01:40,600 --> 00:01:46,880
they were word-sized and then s extend

40
00:01:43,360 --> 00:01:50,719
the results to 64 bits so let's look at

41
00:01:46,880 --> 00:01:54,560
a concrete example we've got slw t0 T1

42
00:01:50,719 --> 00:01:56,479
T2 the bottom five bits are set to C for

43
00:01:54,560 --> 00:01:59,280
a 12-bit shift and now we're going to

44
00:01:56,479 --> 00:02:02,360
use the constant value 1122 33 all the

45
00:01:59,280 --> 00:02:05,240
way up to 88 and now we are going to

46
00:02:02,360 --> 00:02:09,599
take T1 treated as a word size register

47
00:02:05,240 --> 00:02:14,120
so Lop it in half only focus on the 5566

48
00:02:09,599 --> 00:02:16,879
7788 and shift that over by 12 bits so

49
00:02:14,120 --> 00:02:18,440
12 bit shift to the left means that

50
00:02:16,879 --> 00:02:20,720
we're going to take this stuff at the

51
00:02:18,440 --> 00:02:23,080
bottom move it 12 bits to the left and

52
00:02:20,720 --> 00:02:25,360
then ultimately based on whatever our

53
00:02:23,080 --> 00:02:28,400
most significant bit is that will be

54
00:02:25,360 --> 00:02:30,440
sign extended into the upper 32 bits so

55
00:02:28,400 --> 00:02:33,680
treat it like it was a 32 bit register

56
00:02:30,440 --> 00:02:36,319
which would have yielded 6 7788 and then

57
00:02:33,680 --> 00:02:38,040
three Z but then sign extend whatever

58
00:02:36,319 --> 00:02:40,280
the most significant bits are to yield a

59
00:02:38,040 --> 00:02:43,080
final 64-bit result let's see a

60
00:02:40,280 --> 00:02:46,120
different example of that we've got now

61
00:02:43,080 --> 00:02:48,560
D we're going to shift by 13 bits

62
00:02:46,120 --> 00:02:52,280
instead of 12 bits and so if we once

63
00:02:48,560 --> 00:02:56,800
again have 1122 33 up to 88 and we shift

64
00:02:52,280 --> 00:02:59,000
that left logical by 13 bits that is

65
00:02:56,800 --> 00:03:00,720
going to mean that that most significant

66
00:02:59,000 --> 00:03:02,560
bit which was pre previously zero now

67
00:03:00,720 --> 00:03:06,080
it's one over and so this thing that was

68
00:03:02,560 --> 00:03:07,280
six which was 0 1 1 0 for 6 got shifted

69
00:03:06,080 --> 00:03:09,480
over one more and now the most

70
00:03:07,280 --> 00:03:14,480
significant bit is one and so we've got

71
00:03:09,480 --> 00:03:16,879
all FS followed by cf1 and four zeros so

72
00:03:14,480 --> 00:03:19,200
that's the way that this shift left

73
00:03:16,879 --> 00:03:20,879
logical with word size Works treat it

74
00:03:19,200 --> 00:03:24,360
like it's 32-bit but then when you're

75
00:03:20,879 --> 00:03:26,760
done extend it up to a 64-bit

76
00:03:24,360 --> 00:03:28,599
value then let's look at the shift right

77
00:03:26,760 --> 00:03:30,599
logical and of course it's going to be

78
00:03:28,599 --> 00:03:32,560
all the same sort of ideas shifting

79
00:03:30,599 --> 00:03:35,760
right logical so you always fill in with

80
00:03:32,560 --> 00:03:38,280
zeros and word size registers source and

81
00:03:35,760 --> 00:03:40,599
destination word size zero extend the

82
00:03:38,280 --> 00:03:42,840
result to 64 bits you could say that

83
00:03:40,599 --> 00:03:45,480
we're sign extending it except because

84
00:03:42,840 --> 00:03:47,959
this is a logical right shift the upper

85
00:03:45,480 --> 00:03:50,599
bits are always going to be zeros so I'm

86
00:03:47,959 --> 00:03:54,239
just going to say zero extend okay set

87
00:03:50,599 --> 00:03:57,640
it up shift by 12 bits here we go we've

88
00:03:54,239 --> 00:03:59,239
got 1 1 2 3 22 3 3 and so forth but

89
00:03:57,640 --> 00:04:01,840
again we're going to treat it like it's

90
00:03:59,239 --> 00:04:05,519
a bottom 32 bits and we're going to

91
00:04:01,840 --> 00:04:08,439
shift that right by 12 bits and so that

92
00:04:05,519 --> 00:04:10,200
goes right by 12 bits and then we are

93
00:04:08,439 --> 00:04:12,280
doing a logical shift so we don't even

94
00:04:10,200 --> 00:04:14,519
care what the most significant bit was

95
00:04:12,280 --> 00:04:19,359
we just always fill it in with zeros and

96
00:04:14,519 --> 00:04:22,240
so that yields hex decimal 00 055

97
00:04:19,359 --> 00:04:24,520
667 and like I said it's zero extension

98
00:04:22,240 --> 00:04:26,440
or S extension if you want but either

99
00:04:24,520 --> 00:04:28,240
ways it's always going to be zeros

100
00:04:26,440 --> 00:04:30,639
because a logical right shift always

101
00:04:28,240 --> 00:04:34,840
fills the most significant bits with

102
00:04:30,639 --> 00:04:37,280
zero and finally we have SRA same thing

103
00:04:34,840 --> 00:04:40,280
shift right but now it's arithmetic word

104
00:04:37,280 --> 00:04:42,320
size five bits interpreted this Second

105
00:04:40,280 --> 00:04:43,759
source register interpreted as five bits

106
00:04:42,320 --> 00:04:46,639
source and destination word size

107
00:04:43,759 --> 00:04:49,120
register sign extend up to 64 bits let's

108
00:04:46,639 --> 00:04:51,280
just see an example an arithmetic shift

109
00:04:49,120 --> 00:04:53,240
of course as usual is going to take the

110
00:04:51,280 --> 00:04:55,479
most significant bit which in this

111
00:04:53,240 --> 00:04:58,600
context because we're using a word siiz

112
00:04:55,479 --> 00:05:00,800
register it's the bit 31 of the word

113
00:04:58,600 --> 00:05:02,520
siiz register that is our rs1

114
00:05:00,800 --> 00:05:05,400
interpreted so we take that most

115
00:05:02,520 --> 00:05:08,240
significant bit of the 30 we take bit 31

116
00:05:05,400 --> 00:05:10,600
essentially of rs1 and we use that for

117
00:05:08,240 --> 00:05:12,160
the filling in of the shifted bits so

118
00:05:10,600 --> 00:05:15,560
we're going to assume a 12- bit shift

119
00:05:12,160 --> 00:05:18,039
again we've got 1 1 2 2 3 3 I chose to

120
00:05:15,560 --> 00:05:19,880
put an A in here instead of the five

121
00:05:18,039 --> 00:05:21,919
because I want that most significant bit

122
00:05:19,880 --> 00:05:24,600
to be a one so that I can show you that

123
00:05:21,919 --> 00:05:27,319
when we shift over yes they go over by

124
00:05:24,600 --> 00:05:29,759
12 bits but then we take that one and

125
00:05:27,319 --> 00:05:32,080
it's an arithmetic shift we fill in all

126
00:05:29,759 --> 00:05:34,560
of the ones up here and then above and

127
00:05:32,080 --> 00:05:39,120
beyond that we sign extend the result to

128
00:05:34,560 --> 00:05:41,840
64 bits yielding all FS a

129
00:05:39,120 --> 00:05:44,120
5667 had we left our

130
00:05:41,840 --> 00:05:45,880
4455 that five would have had a most

131
00:05:44,120 --> 00:05:48,600
significant bit of zero and that would

132
00:05:45,880 --> 00:05:51,039
have been filled in instead okay well

133
00:05:48,600 --> 00:05:53,160
that is just another permutation on the

134
00:05:51,039 --> 00:05:56,840
shifting we've got shift right shift

135
00:05:53,160 --> 00:05:58,280
left arithmetic and logical for right no

136
00:05:56,840 --> 00:06:00,400
arithmetic for left because it's going

137
00:05:58,280 --> 00:06:01,960
to be just the same thing so go ahead

138
00:06:00,400 --> 00:06:03,960
and step through the assembly and make

139
00:06:01,960 --> 00:06:05,600
sure you understand what's going on to

140
00:06:03,960 --> 00:06:08,039
double check things you can of course

141
00:06:05,600 --> 00:06:10,080
provide your own values in the command

142
00:06:08,039 --> 00:06:11,960
line arguments and see the different

143
00:06:10,080 --> 00:06:15,960
values that result so that you can

144
00:06:11,960 --> 00:06:15,960
confirm it behaves the way you expect

