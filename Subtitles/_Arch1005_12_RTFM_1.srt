1
00:00:00,199 --> 00:00:03,719
now when it comes to reading the

2
00:00:01,439 --> 00:00:05,560
documentation you may feel like Kylo

3
00:00:03,719 --> 00:00:06,720
here and you know what you have to do

4
00:00:05,560 --> 00:00:09,639
but you don't know if you have the

5
00:00:06,720 --> 00:00:12,040
strength to do it well unlike Kylo I

6
00:00:09,639 --> 00:00:14,360
like to read the manual and because I

7
00:00:12,040 --> 00:00:16,520
have repeatedly misled you through

8
00:00:14,360 --> 00:00:18,320
simplification you're going to have to

9
00:00:16,520 --> 00:00:20,800
read the manual to know how things

10
00:00:18,320 --> 00:00:24,599
actually work so it is time to learn the

11
00:00:20,800 --> 00:00:26,599
fascinating truth it is time to read the

12
00:00:24,599 --> 00:00:29,080
fun

13
00:00:26,599 --> 00:00:30,560
manual all right reading the fun manuals

14
00:00:29,080 --> 00:00:31,960
where do you get them from first of all

15
00:00:30,560 --> 00:00:34,440
well of course they were including your

16
00:00:31,960 --> 00:00:36,000
class materials but going forward

17
00:00:34,440 --> 00:00:38,440
because things couldn't change and

18
00:00:36,000 --> 00:00:40,360
because this class was built on the 2019

19
00:00:38,440 --> 00:00:43,239
version of the manual which I would hope

20
00:00:40,360 --> 00:00:45,680
and expect will be updated soon you're

21
00:00:43,239 --> 00:00:47,840
going to want to consult with this URL

22
00:00:45,680 --> 00:00:49,320
in the future to find the unprivileged

23
00:00:47,840 --> 00:00:51,520
specification and the privileged

24
00:00:49,320 --> 00:00:53,559
specification unprivileged is what this

25
00:00:51,520 --> 00:00:55,840
class is all about and privileged is

26
00:00:53,559 --> 00:00:58,199
what a future class like architecture

27
00:00:55,840 --> 00:01:00,559
2005 would be about in the same way that

28
00:00:58,199 --> 00:01:01,800
architecture 2001 in is about OS

29
00:01:00,559 --> 00:01:04,720
internals for

30
00:01:01,800 --> 00:01:08,000
x86 2005 will be about OS internals for

31
00:01:04,720 --> 00:01:10,280
RISC-V and you can also find many of the

32
00:01:08,000 --> 00:01:12,520
not yet ratified but mostly final

33
00:01:10,280 --> 00:01:14,600
specifications linked to from that page

34
00:01:12,520 --> 00:01:16,960
as well the basic outline of the table

35
00:01:14,600 --> 00:01:18,680
of contents for the document that was

36
00:01:16,960 --> 00:01:21,680
provided for this class is going to look

37
00:01:18,680 --> 00:01:26,680
something like this we start with the RV

38
00:01:21,680 --> 00:01:32,320
32i and include the zeny ma a

39
00:01:26,680 --> 00:01:36,399
zixer f& d or I prefer Ma D and zeny and

40
00:01:32,320 --> 00:01:39,320
zixer and those comprise the RV 32G

41
00:01:36,399 --> 00:01:41,399
general purpose instruction set then you

42
00:01:39,320 --> 00:01:46,159
can also Circle the following to get the

43
00:01:41,399 --> 00:01:49,600
RV64G version so you have the RV64I

44
00:01:46,159 --> 00:01:52,880
plus zeny zixer and

45
00:01:49,600 --> 00:01:55,640
mafd if you scroll down you can also

46
00:01:52,880 --> 00:01:58,079
find the C standard extension for

47
00:01:55,640 --> 00:02:00,039
compressed instructions which we covered

48
00:01:58,079 --> 00:02:02,000
some of but not all of the possible

49
00:02:00,039 --> 00:02:04,920
compressed instructions and if you

50
00:02:02,000 --> 00:02:08,560
forget what exactly goes into your rv32

51
00:02:04,920 --> 00:02:12,080
or 64g that is mentioned down in this

52
00:02:08,560 --> 00:02:13,400
section okay so I said that my data that

53
00:02:12,080 --> 00:02:15,360
I gave you in this class is

54
00:02:13,400 --> 00:02:17,360
fundamentally a simplification and it's

55
00:02:15,360 --> 00:02:19,840
misleading so you need to read the

56
00:02:17,360 --> 00:02:23,200
manual to know what's true here's what I

57
00:02:19,840 --> 00:02:25,280
said but the truth is like this look at

58
00:02:23,200 --> 00:02:27,959
that that's ridiculous the manual is so

59
00:02:25,280 --> 00:02:30,519
complicated oh wait hold on a second

60
00:02:27,959 --> 00:02:33,040
yeah that's that's Intel that's X8 6

61
00:02:30,519 --> 00:02:35,200
okay what the manual says is like this

62
00:02:33,040 --> 00:02:38,120
and well you know actually compared to

63
00:02:35,200 --> 00:02:41,120
x86 that's pretty nice like that's not a

64
00:02:38,120 --> 00:02:43,959
whole lot of stuff about ad I it's just

65
00:02:41,120 --> 00:02:45,720
a little you know couple sentences three

66
00:02:43,959 --> 00:02:47,440
sentences and some description of how

67
00:02:45,720 --> 00:02:49,480
the instructions en coding that's

68
00:02:47,440 --> 00:02:53,040
actually pretty nice that makes me feel

69
00:02:49,480 --> 00:02:55,000
happy so here's what we can get out of

70
00:02:53,040 --> 00:02:56,640
this basic description of an assembly

71
00:02:55,000 --> 00:02:58,040
instruction like ADI first of all you'll

72
00:02:56,640 --> 00:03:01,040
see something like this and there will

73
00:02:58,040 --> 00:03:02,400
be multiple instructions Al together and

74
00:03:01,040 --> 00:03:05,080
it's basically saying they're all

75
00:03:02,400 --> 00:03:07,440
encoded in a similar fashion but when we

76
00:03:05,080 --> 00:03:10,159
read the description it says add I adds

77
00:03:07,440 --> 00:03:12,760
the S extended 12-bit immediate and you

78
00:03:10,159 --> 00:03:14,400
can find the 12-bit immediate mentioned

79
00:03:12,760 --> 00:03:18,000
right here and what it's fundamentally

80
00:03:14,400 --> 00:03:21,560
giving you is the 32bit encoding of the

81
00:03:18,000 --> 00:03:24,720
ADI instruction so bits 20 through 31 of

82
00:03:21,560 --> 00:03:27,159
that 32 bits that encode ADI are going

83
00:03:24,720 --> 00:03:29,040
to be the sin extended 12-bit immediate

84
00:03:27,159 --> 00:03:31,640
then what you can see is that that

85
00:03:29,040 --> 00:03:34,480
immediate is added to the value that is

86
00:03:31,640 --> 00:03:37,720
stored in register rs1 so this is

87
00:03:34,480 --> 00:03:39,959
register source one and it says the

88
00:03:37,720 --> 00:03:41,799
arithmetic overflow is ignored so if you

89
00:03:39,959 --> 00:03:44,720
add the two together and it's too big

90
00:03:41,799 --> 00:03:47,760
for a 64-bit or 32-bit value if it's too

91
00:03:44,720 --> 00:03:49,720
big for the register oh well too bad it

92
00:03:47,760 --> 00:03:52,360
just ignores that in this instruction it

93
00:03:49,720 --> 00:03:54,599
doesn't go set any Flags or other bits

94
00:03:52,360 --> 00:03:57,239
anywhere else it would be up to the

95
00:03:54,599 --> 00:03:59,799
compiler to generate code that checks

96
00:03:57,239 --> 00:04:01,760
for whether an overflow would occur uh

97
00:03:59,799 --> 00:04:04,400
before the addition is actually done if

98
00:04:01,760 --> 00:04:07,879
the code actually cares about or depends

99
00:04:04,400 --> 00:04:10,599
upon the result of an overflow occurring

100
00:04:07,879 --> 00:04:13,159
and then the result is simply the low

101
00:04:10,599 --> 00:04:14,640
XLEN BS bits of the result because

102
00:04:13,159 --> 00:04:16,880
basically anything that overflows is

103
00:04:14,640 --> 00:04:18,639
ignored and you just keep the lower XLEN

104
00:04:16,880 --> 00:04:21,199
bits and you can see here that the

105
00:04:18,639 --> 00:04:23,240
manual is defining things in terms of XLEN

106
00:04:21,199 --> 00:04:27,479
this is your hint that this instruction

107
00:04:23,240 --> 00:04:30,280
ADDI can operate in xll equal to 32 XLEN

108
00:04:27,479 --> 00:04:33,000
equal to 64 or in the future X equal to

109
00:04:30,280 --> 00:04:36,199
128 for instance and that destination

110
00:04:33,000 --> 00:04:38,600
register is right here Rd so that's

111
00:04:36,199 --> 00:04:40,240
pretty good the couple of sentences give

112
00:04:38,600 --> 00:04:44,600
us a description of what's going on with

113
00:04:40,240 --> 00:04:47,120
this M12 this rs1 and the Rd but it

114
00:04:44,600 --> 00:04:50,039
doesn't tell us for instance how we can

115
00:04:47,120 --> 00:04:52,560
use these bits rs1 or Rd they appear to

116
00:04:50,039 --> 00:04:54,720
be five bits like because this was 12

117
00:04:52,560 --> 00:04:56,479
and this is five so it appears to be

118
00:04:54,720 --> 00:05:00,600
five bits and actually it's exactly five

119
00:04:56,479 --> 00:05:02,600
bits because you can say 15 16 17 18 19

120
00:05:00,600 --> 00:05:04,320
five bits encode the source register

121
00:05:02,600 --> 00:05:06,880
five bits encode the destination

122
00:05:04,320 --> 00:05:09,560
register so it doesn't tell us yet what

123
00:05:06,880 --> 00:05:11,720
those five bits how those are used and

124
00:05:09,560 --> 00:05:14,240
it also doesn't tell us what this op

125
00:05:11,720 --> 00:05:15,800
code is but we'll be describing that

126
00:05:14,240 --> 00:05:17,880
later on when we talk about instruction

127
00:05:15,800 --> 00:05:20,199
en coding and decoding so now I want to

128
00:05:17,880 --> 00:05:21,840
just jump into the manual with you and I

129
00:05:20,199 --> 00:05:23,800
want to walk through it a little bit to

130
00:05:21,840 --> 00:05:25,840
hopefully make you feel more comfortable

131
00:05:23,800 --> 00:05:28,039
because the goal is after this class for

132
00:05:25,840 --> 00:05:30,319
you to be comfortable and able to

133
00:05:28,039 --> 00:05:32,360
continue on on your own and to be able

134
00:05:30,319 --> 00:05:34,280
to read the fun manual okay so there's

135
00:05:32,360 --> 00:05:36,120
the table of contents that we already

136
00:05:34,280 --> 00:05:39,280
talked about but let's just you know

137
00:05:36,120 --> 00:05:41,199
start with the rv32 I and walk through

138
00:05:39,280 --> 00:05:43,560
it one of the first things it says

139
00:05:41,199 --> 00:05:46,520
programmer's model for base integer isos

140
00:05:43,560 --> 00:05:48,680
talks about XLEN and the registers so

141
00:05:46,520 --> 00:05:50,800
we already talked about that there are

142
00:05:48,680 --> 00:05:53,720
32 general purpose registers plus the

143
00:05:50,800 --> 00:05:55,720
program counter and they are x l wide so

144
00:05:53,720 --> 00:05:58,720
not that interesting and a lot of times

145
00:05:55,720 --> 00:06:00,440
the this italic bit in the manual is

146
00:05:58,720 --> 00:06:02,960
lots of just interesting sort of

147
00:06:00,440 --> 00:06:05,199
architectural choices and asides they

148
00:06:02,960 --> 00:06:07,479
just kind of side comments that they put

149
00:06:05,199 --> 00:06:08,840
in there to explain some things but

150
00:06:07,479 --> 00:06:10,599
sometimes they're kind of a little bit

151
00:06:08,840 --> 00:06:12,319
premature like all of a sudden it's

152
00:06:10,599 --> 00:06:13,479
telling you about particular assembly

153
00:06:12,319 --> 00:06:15,479
instructions which if you're just

154
00:06:13,479 --> 00:06:18,080
reading top to bottom you have never

155
00:06:15,479 --> 00:06:20,240
ever seen before but regardless it's

156
00:06:18,080 --> 00:06:21,919
still you know somewhat useful then it

157
00:06:20,240 --> 00:06:24,280
starts talking about base instruction

158
00:06:21,919 --> 00:06:26,120
formats so one of the first things it

159
00:06:24,280 --> 00:06:28,479
talks about is how instructions are

160
00:06:26,120 --> 00:06:30,880
encoded but again like if you're new to

161
00:06:28,479 --> 00:06:32,800
it you don't care yet you just want to

162
00:06:30,880 --> 00:06:35,400
see the assembly instructions first or

163
00:06:32,800 --> 00:06:37,639
at least we do th software types maybe a

164
00:06:35,400 --> 00:06:38,800
hardware person actually cares about

165
00:06:37,639 --> 00:06:41,400
this maybe they care about how

166
00:06:38,800 --> 00:06:43,680
complicated the instruction fetch decode

167
00:06:41,400 --> 00:06:44,720
cycle is stuff like that but we don't

168
00:06:43,680 --> 00:06:46,400
they just talk about a bunch of

169
00:06:44,720 --> 00:06:48,160
different encodings and they mean

170
00:06:46,400 --> 00:06:50,360
absolutely nothing to us because we

171
00:06:48,160 --> 00:06:52,759
really don't care until we find some

172
00:06:50,360 --> 00:06:54,440
assembly instructions so great a whole

173
00:06:52,759 --> 00:06:57,639
bunch of stuff we don't care about but

174
00:06:54,440 --> 00:06:59,840
finally by section 2.4 we get to integer

175
00:06:57,639 --> 00:07:01,440
computation instructions and once again

176
00:06:59,840 --> 00:07:03,360
it talks about some instructions that

177
00:07:01,440 --> 00:07:05,360
you can't necessarily have ever seen

178
00:07:03,360 --> 00:07:06,520
before in your life if this is the first

179
00:07:05,360 --> 00:07:08,639
time you're just you know jumping into

180
00:07:06,520 --> 00:07:09,960
RISC-V had you not taken this class

181
00:07:08,639 --> 00:07:12,280
had you not seen all of these

182
00:07:09,960 --> 00:07:14,000
instructions already this would be kind

183
00:07:12,280 --> 00:07:15,120
of annoying that you're jumping in here

184
00:07:14,000 --> 00:07:17,240
and it's talking about all these

185
00:07:15,120 --> 00:07:19,879
instructions that don't mean anything to

186
00:07:17,240 --> 00:07:21,800
you but then once you get down to about

187
00:07:19,879 --> 00:07:25,160
here this is where we see the stuff that

188
00:07:21,800 --> 00:07:27,360
we already covered in the uh screenshots

189
00:07:25,160 --> 00:07:29,120
and stuff is the idea of there will be

190
00:07:27,360 --> 00:07:32,400
multiple instructions they will all have

191
00:07:29,120 --> 00:07:34,720
a similar encoding and then below this

192
00:07:32,400 --> 00:07:36,160
collection of instructions we will see

193
00:07:34,720 --> 00:07:38,680
the descriptions of how they work and

194
00:07:36,160 --> 00:07:40,879
usually it's only a few sentences and

195
00:07:38,680 --> 00:07:42,960
this tends to make me feel like at least

196
00:07:40,879 --> 00:07:45,680
when I was starting it felt like okay

197
00:07:42,960 --> 00:07:47,440
great it's tur it's not giant manual but

198
00:07:45,680 --> 00:07:49,520
it sometimes left things a little

199
00:07:47,440 --> 00:07:51,280
ambiguous and I had to go out and write

200
00:07:49,520 --> 00:07:53,639
assembly instructions as you'll find in

201
00:07:51,280 --> 00:07:55,919
the next section to actually clear up

202
00:07:53,639 --> 00:07:58,199
what the ambiguity was so here again

203
00:07:55,919 --> 00:08:01,319
it's talking about ADDI then we can

204
00:07:58,199 --> 00:08:03,800
have this set if less than immediate and

205
00:08:01,319 --> 00:08:06,639
immediate unsigned then we have the Andy

206
00:08:03,800 --> 00:08:08,479
Ori and zor and all of these just have

207
00:08:06,639 --> 00:08:11,120
our general descriptions of how they

208
00:08:08,479 --> 00:08:14,599
work frequently they will also include

209
00:08:11,120 --> 00:08:17,639
like little mentions of okay well ADI Rd

210
00:08:14,599 --> 00:08:22,560
RS Z is used to implement the move

211
00:08:17,639 --> 00:08:26,879
assembler instruction or this salt IU

212
00:08:22,560 --> 00:08:30,520
with the one as the source 2 is going to

213
00:08:26,879 --> 00:08:32,240
implement the set if equal to to0 and we

214
00:08:30,520 --> 00:08:35,680
didn't actually see that pseudo

215
00:08:32,240 --> 00:08:37,279
instruction in our class there will be a

216
00:08:35,680 --> 00:08:39,320
table of all the various pseudo

217
00:08:37,279 --> 00:08:41,440
instructions elsewhere in the class in

218
00:08:39,320 --> 00:08:44,640
the manual so if we wanted to search for

219
00:08:41,440 --> 00:08:47,040
that for instance set eqz then we could

220
00:08:44,640 --> 00:08:49,560
find that elsewhere and this has a bunch

221
00:08:47,040 --> 00:08:51,120
of different RISC VI pseudo instructions

222
00:08:49,560 --> 00:08:57,480
so sometimes you know if you would have

223
00:08:51,120 --> 00:08:58,839
seen set eqz or NE W or NE things like

224
00:08:57,480 --> 00:09:00,640
that you would have searched the manual

225
00:08:58,839 --> 00:09:03,040
and you would have perhaps found them

226
00:09:00,640 --> 00:09:04,880
here in the pseudo instructions table or

227
00:09:03,040 --> 00:09:06,959
you may have seen them elsewhere in the

228
00:09:04,880 --> 00:09:09,680
manual such as being mentioned in the

229
00:09:06,959 --> 00:09:12,399
context of the instruction that is used

230
00:09:09,680 --> 00:09:14,240
behind the scenes so just uh continuing

231
00:09:12,399 --> 00:09:16,680
to walk through here we've got now some

232
00:09:14,240 --> 00:09:20,839
of our shifts we got silly sirly and

233
00:09:16,680 --> 00:09:23,720
sray uh moving on we got LUI and

234
00:09:20,839 --> 00:09:25,959
wepc they will mention in the

235
00:09:23,720 --> 00:09:28,839
instruction like hey this uses the U

236
00:09:25,959 --> 00:09:30,200
type format but you don't really care

237
00:09:28,839 --> 00:09:31,880
and you don't want to go back and like

238
00:09:30,200 --> 00:09:33,880
consult with the U type format that's

239
00:09:31,880 --> 00:09:35,560
not really useful to us like all that's

240
00:09:33,880 --> 00:09:36,839
useful is just being able to read this

241
00:09:35,560 --> 00:09:38,920
thing right here I don't care if it's a

242
00:09:36,839 --> 00:09:40,720
u type format it's just I need to know

243
00:09:38,920 --> 00:09:42,320
what the op code is I need to know how

244
00:09:40,720 --> 00:09:44,760
to specify the five bits of the

245
00:09:42,320 --> 00:09:47,399
destination register and I need to know

246
00:09:44,760 --> 00:09:49,079
that these upper 20 bits are the upper

247
00:09:47,399 --> 00:09:52,959
20 bits that are going to be used in a

248
00:09:49,079 --> 00:09:54,360
LUI or a wi PC so yeah any like

249
00:09:52,959 --> 00:09:56,040
basically you don't need to memorize

250
00:09:54,360 --> 00:09:57,920
even though you will see them repeatedly

251
00:09:56,040 --> 00:10:00,000
saying this is a u type this is an IT

252
00:09:57,920 --> 00:10:01,600
type this is a b type you really don't

253
00:10:00,000 --> 00:10:05,200
care like you just need to be able to

254
00:10:01,600 --> 00:10:08,959
read these things right here so moving

255
00:10:05,200 --> 00:10:11,200
on we have now some of our ads and ands

256
00:10:08,959 --> 00:10:13,320
and exors and some more shifts so you

257
00:10:11,200 --> 00:10:15,600
can see this is a mly collection of

258
00:10:13,320 --> 00:10:18,040
multiple different things and until we

259
00:10:15,600 --> 00:10:20,680
actually know how to interpret these fun

260
00:10:18,040 --> 00:10:23,240
three and the op code and other stuff uh

261
00:10:20,680 --> 00:10:24,959
we won't actually know like how what you

262
00:10:23,240 --> 00:10:26,920
know what differentiates an ad from

263
00:10:24,959 --> 00:10:29,360
assault from assault you like they are

264
00:10:26,920 --> 00:10:31,680
all on the same line and why are they on

265
00:10:29,360 --> 00:10:33,360
the same line once we see the actual

266
00:10:31,680 --> 00:10:35,959
instruction encoding that will perhaps

267
00:10:33,360 --> 00:10:38,279
make a little more sense so again just

268
00:10:35,959 --> 00:10:40,519
uh some instructions up here then we've

269
00:10:38,279 --> 00:10:42,839
got the noop instruction given its own

270
00:10:40,519 --> 00:10:45,320
special place but told that this is

271
00:10:42,839 --> 00:10:47,120
actually an ADDI control transfer

272
00:10:45,320 --> 00:10:50,800
meaning jumps and then subsequently

273
00:10:47,120 --> 00:10:54,440
branches we will have uh the JW the jump

274
00:10:50,800 --> 00:10:56,040
and link and that will be described by

275
00:10:54,440 --> 00:10:58,320
this and we'll have to learn how to read

276
00:10:56,040 --> 00:11:00,279
that and then we've got the jawar the

277
00:10:58,320 --> 00:11:03,680
jump and link r register described by

278
00:11:00,279 --> 00:11:05,800
this so similar but not exactly the same

279
00:11:03,680 --> 00:11:08,000
we've got a much bigger immediate here

280
00:11:05,800 --> 00:11:11,000
right we said joller had only a 12 bit

281
00:11:08,000 --> 00:11:13,720
immediate whereas jel had a 21 bit

282
00:11:11,000 --> 00:11:16,040
immediate so the zeroth bit this was a

283
00:11:13,720 --> 00:11:18,440
thing that was scaled by two and so the

284
00:11:16,040 --> 00:11:22,519
uppermost bit being 20 means that it's

285
00:11:18,440 --> 00:11:24,959
actually index 20 so bit 21 uh and so if

286
00:11:22,519 --> 00:11:26,639
we scale back we can see that here it

287
00:11:24,959 --> 00:11:29,120
doesn't actually use the keyword of

288
00:11:26,639 --> 00:11:31,440
scaled by two it says multiples of two

289
00:11:29,120 --> 00:11:32,800
so it's a little inconsistent sometimes

290
00:11:31,440 --> 00:11:36,839
in the documentation about whether it

291
00:11:32,800 --> 00:11:38,839
says multiples of X or scaled by X but

292
00:11:36,839 --> 00:11:40,959
as we learned elsewhere in the class

293
00:11:38,839 --> 00:11:44,160
just you know when you don't see a

294
00:11:40,959 --> 00:11:45,880
zeroeth bit encoded anywhere in things

295
00:11:44,160 --> 00:11:48,240
then that just means the zeroth bit must

296
00:11:45,880 --> 00:11:50,279
be zero and it's not specified in the

297
00:11:48,240 --> 00:11:52,360
instruction encoding but it's implicitly

298
00:11:50,279 --> 00:11:55,279
zero and therefore you can have zero or

299
00:11:52,360 --> 00:11:57,000
two or four Etc values that always have

300
00:11:55,279 --> 00:12:00,320
the bit Z equal to

301
00:11:57,000 --> 00:12:02,680
zero all right so continuing on past all

302
00:12:00,320 --> 00:12:04,279
of the you know and so again this is why

303
00:12:02,680 --> 00:12:06,079
you need to read the instructions like

304
00:12:04,279 --> 00:12:08,480
there's all sorts of guidance there's

305
00:12:06,079 --> 00:12:10,279
all sorts of best practices and I just

306
00:12:08,480 --> 00:12:13,079
really simplifi things down in this

307
00:12:10,279 --> 00:12:15,399
class uh to avoid complexity for as long

308
00:12:13,079 --> 00:12:17,199
as we can before we ultimately have to

309
00:12:15,399 --> 00:12:19,800
learn how to read and understand the

310
00:12:17,199 --> 00:12:21,959
full complexity via the manual not via

311
00:12:19,800 --> 00:12:24,000
me explaining every single bit to you

312
00:12:21,959 --> 00:12:26,000
then after the jaw and joller we've got

313
00:12:24,000 --> 00:12:27,560
our branches and you can see like these

314
00:12:26,000 --> 00:12:29,199
are all the branches you know we covered

315
00:12:27,560 --> 00:12:31,279
all the assembly instructions there's no

316
00:12:29,199 --> 00:12:34,480
other branch types that are available in

317
00:12:31,279 --> 00:12:37,000
Risk 5 this is it so those are all the

318
00:12:34,480 --> 00:12:39,040
branches scaling down whole bunch of

319
00:12:37,000 --> 00:12:40,920
extra information about you know which

320
00:12:39,040 --> 00:12:42,480
way that conditional branches should

321
00:12:40,920 --> 00:12:44,360
jump should they jump forward versus

322
00:12:42,480 --> 00:12:46,600
backwards all having to do with branch

323
00:12:44,360 --> 00:12:49,120
prediction and stuff like that and their

324
00:12:46,600 --> 00:12:51,560
best practices then we get the load and

325
00:12:49,120 --> 00:12:55,680
store instructions and we know that we

326
00:12:51,560 --> 00:12:58,480
had uh loads for loads words loads bytes

327
00:12:55,680 --> 00:13:01,360
loads halfword and this is again where

328
00:12:58,480 --> 00:13:03,519
some times the shorter version may feel

329
00:13:01,360 --> 00:13:05,519
a little underspecified because it'll

330
00:13:03,519 --> 00:13:07,399
talk about how the load word instruction

331
00:13:05,519 --> 00:13:09,199
works and how the halfword instruction

332
00:13:07,399 --> 00:13:12,079
works but then sometimes you'll have

333
00:13:09,199 --> 00:13:14,920
these turs like lb and lbu are defined

334
00:13:12,079 --> 00:13:16,519
analogously for 8bit values so you need

335
00:13:14,920 --> 00:13:18,360
to make sure that you really understand

336
00:13:16,519 --> 00:13:21,760
like how some of these bigger values uh

337
00:13:18,360 --> 00:13:23,800
bigger value using instructions work so

338
00:13:21,760 --> 00:13:25,800
that you can see how they are defined

339
00:13:23,800 --> 00:13:28,199
analogously well sometimes you may still

340
00:13:25,800 --> 00:13:30,480
have ambiguity there and you may have to

341
00:13:28,199 --> 00:13:32,160
just go off and write an assembly

342
00:13:30,480 --> 00:13:33,160
instruction to confirm that it behaves

343
00:13:32,160 --> 00:13:36,800
the way you

344
00:13:33,160 --> 00:13:40,399
expect all right and continuing

345
00:13:36,800 --> 00:13:43,320
on we will eventually hit our memory

346
00:13:40,399 --> 00:13:44,480
ordering or the fence instruction so I

347
00:13:43,320 --> 00:13:46,800
said that there was a much more

348
00:13:44,480 --> 00:13:49,959
complicated version of fence that has to

349
00:13:46,800 --> 00:13:53,160
do with whether you're dealing with um

350
00:13:49,959 --> 00:13:55,959
device input device output memory reads

351
00:13:53,160 --> 00:13:58,360
and memory rights and the fence uh

352
00:13:55,959 --> 00:14:00,440
basically can actually specify like what

353
00:13:58,360 --> 00:14:03,120
kind of ordering you want the things to

354
00:14:00,440 --> 00:14:05,720
use and so you have to read all of this

355
00:14:03,120 --> 00:14:08,040
to to get the actual full description I

356
00:14:05,720 --> 00:14:10,160
said just use fence and just use fence

357
00:14:08,040 --> 00:14:13,079
will essentially set all of these bits

358
00:14:10,160 --> 00:14:15,240
so that it's um it's uh maintaining the

359
00:14:13,079 --> 00:14:18,279
Ordering of all device input output

360
00:14:15,240 --> 00:14:20,160
reads and writes and so yeah then you

361
00:14:18,279 --> 00:14:21,480
know those are our last few instructions

362
00:14:20,160 --> 00:14:23,680
we got the fence and then we got the

363
00:14:21,480 --> 00:14:25,800
environment call and Brake points and

364
00:14:23,680 --> 00:14:27,920
then that's basically everything now I

365
00:14:25,800 --> 00:14:30,199
want to make a point about like that's

366
00:14:27,920 --> 00:14:32,079
the you know 32-bit stuff there's a

367
00:14:30,199 --> 00:14:33,680
little bit about hints and I don't care

368
00:14:32,079 --> 00:14:35,160
about the hints here so I'm just going

369
00:14:33,680 --> 00:14:37,079
to skip that you can read that if you're

370
00:14:35,160 --> 00:14:39,040
interested I just want to make a point

371
00:14:37,079 --> 00:14:42,880
that that was all the 32-bit stuff

372
00:14:39,040 --> 00:14:45,160
64-bit stuff essentially is all four

373
00:14:42,880 --> 00:14:48,920
pages like four pages is all it takes to

374
00:14:45,160 --> 00:14:51,680
describe the super set of 64-bit stuff

375
00:14:48,920 --> 00:14:54,079
and so it's only those W suffix things

376
00:14:51,680 --> 00:14:56,440
right we only had 15 new assembly

377
00:14:54,079 --> 00:15:00,880
instructions the W suffix things like

378
00:14:56,440 --> 00:15:02,880
add IW the sh the silly W but then also

379
00:15:00,880 --> 00:15:05,600
the silly which we talked about earlier

380
00:15:02,880 --> 00:15:07,440
this has six bits rather than five bits

381
00:15:05,600 --> 00:15:09,720
for the shift amount and that's why it's

382
00:15:07,440 --> 00:15:11,920
a new assembly instruction but yeah it's

383
00:15:09,720 --> 00:15:14,120
like pretty short sometimes they include

384
00:15:11,920 --> 00:15:16,279
some stuff like Lou and we PCS but then

385
00:15:14,120 --> 00:15:19,040
it says this uses the same op code as

386
00:15:16,279 --> 00:15:20,839
the RV32I so it's not actually

387
00:15:19,040 --> 00:15:22,639
different and I'm not really sure why

388
00:15:20,839 --> 00:15:25,399
they included it when they didn't

389
00:15:22,639 --> 00:15:28,000
include all the other stuff right then

390
00:15:25,399 --> 00:15:31,160
other operations like there's some

391
00:15:28,000 --> 00:15:32,800
things like Sil and surl and sraw but

392
00:15:31,160 --> 00:15:34,480
those are ultimately just actually going

393
00:15:32,800 --> 00:15:36,279
to be encoded exactly the same as the

394
00:15:34,480 --> 00:15:38,000
32-bit version didn't really need to be

395
00:15:36,279 --> 00:15:40,920
included here so then we've got

396
00:15:38,000 --> 00:15:42,600
instructions like Sil and surl and SRA

397
00:15:40,920 --> 00:15:45,880
and the only reason they're included

398
00:15:42,600 --> 00:15:48,160
here is just to confirm that in RV64I

399
00:15:45,880 --> 00:15:49,639
it's only going to use the lower six

400
00:15:48,160 --> 00:15:52,560
bits because now we're dealing with

401
00:15:49,639 --> 00:15:55,360
64-bit registers of the source

402
00:15:52,560 --> 00:15:57,399
2 so yeah then we've got our load and

403
00:15:55,360 --> 00:15:59,800
store which in the context of 64-bit are

404
00:15:57,399 --> 00:16:01,639
going to be load and store word and then

405
00:15:59,800 --> 00:16:04,399
we're down to hints so that's it

406
00:16:01,639 --> 00:16:06,319
basically four pages for the only stuff

407
00:16:04,399 --> 00:16:08,399
you need to know for 64-bit so that

408
00:16:06,319 --> 00:16:11,120
certainly is much nicer than something

409
00:16:08,399 --> 00:16:13,360
like x86 all right let's go back and

410
00:16:11,120 --> 00:16:14,800
look at some specific examples that I

411
00:16:13,360 --> 00:16:17,800
plucked out that I think are interesting

412
00:16:14,800 --> 00:16:17,800
from the manual

