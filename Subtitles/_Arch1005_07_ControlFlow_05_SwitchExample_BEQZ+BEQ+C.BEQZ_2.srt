1
00:00:00,359 --> 00:00:05,719
well before we mark up our collection

2
00:00:02,320 --> 00:00:05,719
spot a chaos magic

3
00:00:10,920 --> 00:00:16,960
Governor looks like we've got a

4
00:00:13,599 --> 00:00:19,039
compressed branch of equal to zero which

5
00:00:16,960 --> 00:00:21,119
I thought about actually including a new

6
00:00:19,039 --> 00:00:22,920
notation of it's not going from yellow

7
00:00:21,119 --> 00:00:24,519
to Orange for compressed it's going from

8
00:00:22,920 --> 00:00:27,640
green to Orange for compressed cuz it's

9
00:00:24,519 --> 00:00:30,119
a pseudo instruction but it looked like

10
00:00:27,640 --> 00:00:33,000
terrible so decided not to do that and

11
00:00:30,119 --> 00:00:35,200
then we have CJ the compressed jump

12
00:00:33,000 --> 00:00:37,559
which I did not notice when I first made

13
00:00:35,200 --> 00:00:40,120
these slides and then I had to go spend

14
00:00:37,559 --> 00:00:44,160
hours rearranging and reordering things

15
00:00:40,120 --> 00:00:47,360
fun so we picked up a beq which pops up

16
00:00:44,160 --> 00:00:51,640
down here along with its parasitic bqz

17
00:00:47,360 --> 00:00:53,680
and compressed bqz and CJ the compressed

18
00:00:51,640 --> 00:00:58,320
jump that I just happen to have just

19
00:00:53,680 --> 00:00:58,320
enough space for in this diagram

