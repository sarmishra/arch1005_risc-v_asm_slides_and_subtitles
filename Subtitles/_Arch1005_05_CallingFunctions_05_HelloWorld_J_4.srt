1
00:00:00,359 --> 00:00:04,160
but there's one more variant of hello

2
00:00:02,360 --> 00:00:08,000
world that I'd like you to see so as

3
00:00:04,160 --> 00:00:09,639
morphia says tank load the Jump program

4
00:00:08,000 --> 00:00:12,840
Tank's going to load hello world

5
00:00:09,639 --> 00:00:15,679
optimization level two and that gives us

6
00:00:12,840 --> 00:00:18,640
this this is Hello World optimization

7
00:00:15,679 --> 00:00:21,720
level two three assembly instructions

8
00:00:18,640 --> 00:00:23,400
that's it just three and of those three

9
00:00:21,720 --> 00:00:26,240
there's some magic constant here and if

10
00:00:23,400 --> 00:00:28,960
we look at that that is the address of

11
00:00:26,240 --> 00:00:30,840
the string so examine as string that is

12
00:00:28,960 --> 00:00:33,040
the string hello world so we once again

13
00:00:30,840 --> 00:00:35,920
have the wepc getting the current

14
00:00:33,040 --> 00:00:39,280
program counter adding 208 to it getting

15
00:00:35,920 --> 00:00:42,280
a string pointer into argument zero

16
00:00:39,280 --> 00:00:45,079
before it calls to puts and then it

17
00:00:42,280 --> 00:00:49,000
calls to puts but it's doing a j not a

18
00:00:45,079 --> 00:00:51,760
jaw well friendly friend Cipher says J

19
00:00:49,000 --> 00:00:53,760
stands for jump but yeah really this

20
00:00:51,760 --> 00:00:56,320
code doesn't necessarily make a whole

21
00:00:53,760 --> 00:00:58,719
lot of sense to start with right we call

22
00:00:56,320 --> 00:01:01,879
hello world's main and this is all there

23
00:00:58,719 --> 00:01:04,680
is it's just passing an argument to puts

24
00:01:01,879 --> 00:01:06,400
and then it's done how can that be a sub

25
00:01:04,680 --> 00:01:09,040
routine directly returning to the

26
00:01:06,400 --> 00:01:14,360
function that called me is that even

27
00:01:09,040 --> 00:01:18,360
possible free your mind

28
00:01:14,360 --> 00:01:20,400
jump puts that's right jump over to puts

29
00:01:18,360 --> 00:01:22,560
but of course J is pretending to be

30
00:01:20,400 --> 00:01:26,320
something that it's not it's actually

31
00:01:22,560 --> 00:01:28,799
jaw J is just jaw with the destination

32
00:01:26,320 --> 00:01:33,240
linkage register set to x0 so there is

33
00:01:28,799 --> 00:01:35,720
no linkage so J is JW x0 with a 21 bit

34
00:01:33,240 --> 00:01:39,759
immediate like a normal Jael two effects

35
00:01:35,720 --> 00:01:44,280
as usual PC plus sign extended immediate

36
00:01:39,759 --> 00:01:47,560
equals PC so jump to that new pc but x0

37
00:01:44,280 --> 00:01:49,840
as the destination register means that

38
00:01:47,560 --> 00:01:51,880
no linkage you're not going to store the

39
00:01:49,840 --> 00:01:55,000
address after the jump you're just never

40
00:01:51,880 --> 00:01:57,680
coming back so no link to the past can't

41
00:01:55,000 --> 00:01:59,240
get back there if necessary later jump

42
00:01:57,680 --> 00:02:01,240
is just a pseudo instruction but this is

43
00:01:59,240 --> 00:02:04,280
the good kind of pseudo instruction it's

44
00:02:01,240 --> 00:02:07,159
not evil like those other V

45
00:02:04,280 --> 00:02:09,000
reptiles but there is a lounge of pseudo

46
00:02:07,159 --> 00:02:10,800
instructions that we talked about before

47
00:02:09,000 --> 00:02:14,959
and this was the one that I was hiding

48
00:02:10,800 --> 00:02:19,120
from you J J is just a jump it is a jaw

49
00:02:14,959 --> 00:02:21,440
with x0 and an offset so we had our jaw

50
00:02:19,120 --> 00:02:24,720
and J is the pseudo instruction that

51
00:02:21,440 --> 00:02:24,720
goes to jaw

