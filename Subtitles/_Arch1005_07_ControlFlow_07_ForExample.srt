1
00:00:00,080 --> 00:00:05,879
yet another common control flow type is

2
00:00:02,360 --> 00:00:08,719
the for loop so input from command line

3
00:00:05,879 --> 00:00:10,440
into the I for I we're not going to

4
00:00:08,719 --> 00:00:14,000
initialize it it's already initialized

5
00:00:10,440 --> 00:00:17,560
here for I while I is greater than zero

6
00:00:14,000 --> 00:00:20,279
I minus minus and print high and the

7
00:00:17,560 --> 00:00:22,439
code that pops up for this looks

8
00:00:20,279 --> 00:00:25,000
extremely similar to something we've

9
00:00:22,439 --> 00:00:27,400
seen before so if I gave you plenty of

10
00:00:25,000 --> 00:00:29,359
time to look this over you would perhaps

11
00:00:27,400 --> 00:00:32,880
see that it looks just like the while

12
00:00:29,359 --> 00:00:35,480
example but not quite so let's flip back

13
00:00:32,880 --> 00:00:38,800
and forth and we see that they're almost

14
00:00:35,480 --> 00:00:41,280
exactly the same but something seems to

15
00:00:38,800 --> 00:00:44,160
be changing right around this area

16
00:00:41,280 --> 00:00:46,559
everything else unchanging but this area

17
00:00:44,160 --> 00:00:49,160
is changing so what is the nature of the

18
00:00:46,559 --> 00:00:50,920
change throw it into a diffing program

19
00:00:49,160 --> 00:00:53,359
take a look for a while and you would

20
00:00:50,920 --> 00:00:55,680
find that it seems that the only

21
00:00:53,359 --> 00:00:58,199
difference is the transposition the

22
00:00:55,680 --> 00:01:00,320
changing of the order of two major

23
00:00:58,199 --> 00:01:03,000
operations look in this oper operation

24
00:01:00,320 --> 00:01:04,960
it looks like it's an add minus one so

25
00:01:03,000 --> 00:01:08,080
that's that decrement that we had in the

26
00:01:04,960 --> 00:01:12,119
while it was while a minus minus and

27
00:01:08,080 --> 00:01:14,200
four iusus and then the call to the put

28
00:01:12,119 --> 00:01:16,520
s which is really our print F so it

29
00:01:14,200 --> 00:01:19,240
seems like the print F order was

30
00:01:16,520 --> 00:01:21,040
reversed between our while and R4 and

31
00:01:19,240 --> 00:01:23,520
the decrement was reversed but otherwise

32
00:01:21,040 --> 00:01:26,759
they were the same so this is why our

33
00:01:23,520 --> 00:01:30,240
initial while code is while a greater

34
00:01:26,759 --> 00:01:33,720
than z a minus minus print but if we

35
00:01:30,240 --> 00:01:37,159
changed that to while a greater than 0

36
00:01:33,720 --> 00:01:39,159
print a minus minus then we would indeed

37
00:01:37,159 --> 00:01:42,280
get the same sort of behavior as a for

38
00:01:39,159 --> 00:01:45,079
loop it's doing while I greater than

39
00:01:42,280 --> 00:01:48,200
zero do the stuff inside the for loop

40
00:01:45,079 --> 00:01:52,320
print and then after the for loop do the

41
00:01:48,200 --> 00:01:54,360
I minus minus okay so you can go and

42
00:01:52,320 --> 00:01:55,920
change the code if you'd like for the

43
00:01:54,360 --> 00:01:58,119
while loop to get it to create the same

44
00:01:55,920 --> 00:02:00,759
thing as a four or you can just go ahead

45
00:01:58,119 --> 00:02:02,680
and walk through the four so again step

46
00:02:00,759 --> 00:02:04,240
through the assembly not so much because

47
00:02:02,680 --> 00:02:06,520
you're learning anything new from the

48
00:02:04,240 --> 00:02:09,520
assembly instructions but rather to

49
00:02:06,520 --> 00:02:11,599
recognize the conditional control flow

50
00:02:09,520 --> 00:02:14,560
that you're seeing it's conditional upon

51
00:02:11,599 --> 00:02:16,519
the fact of checking the is I greater

52
00:02:14,560 --> 00:02:17,879
than zero again this is the kind of

53
00:02:16,519 --> 00:02:19,760
thing that a reverse engineer would want

54
00:02:17,879 --> 00:02:21,480
to know they should be able to recognize

55
00:02:19,760 --> 00:02:24,519
something like oh yeah this looks like

56
00:02:21,480 --> 00:02:24,519
this is a for loop

