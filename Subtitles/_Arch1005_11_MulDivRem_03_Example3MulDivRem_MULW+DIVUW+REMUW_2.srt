1
00:00:00,199 --> 00:00:05,120
so the only takeaway from example three

2
00:00:02,240 --> 00:00:08,160
miv remm is that as usual if we use

3
00:00:05,120 --> 00:00:11,759
integers in 64-bit compiled code we're

4
00:00:08,160 --> 00:00:15,080
going to get W suffixed RV 64 extension

5
00:00:11,759 --> 00:00:20,119
instructions so here it's the RV 64m for

6
00:00:15,080 --> 00:00:21,840
multiply MW div UW and REM UW and Avid

7
00:00:20,119 --> 00:00:23,400
collectors that we are that's why we

8
00:00:21,840 --> 00:00:28,080
wanted to include each of these so we

9
00:00:23,400 --> 00:00:30,519
could get the MW dww and remw and again

10
00:00:28,080 --> 00:00:32,680
we see that there's conspicuous

11
00:00:30,519 --> 00:00:36,960
versions that don't have use in them

12
00:00:32,680 --> 00:00:36,960
probably signed instead of unsigned

