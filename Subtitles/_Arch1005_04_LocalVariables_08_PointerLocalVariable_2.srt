1
00:00:00,160 --> 00:00:05,160
and if you step through main plus 44 the

2
00:00:02,960 --> 00:00:07,879
last store this is what you would see on

3
00:00:05,160 --> 00:00:09,960
the stack so typical uninitialized

4
00:00:07,879 --> 00:00:12,040
alignment space after the saved frame

5
00:00:09,960 --> 00:00:14,360
pointer but then we have a pointer B

6
00:00:12,040 --> 00:00:16,240
pointer A and B so first of all again

7
00:00:14,360 --> 00:00:18,000
there's no assumptions that can be made

8
00:00:16,240 --> 00:00:19,720
about the order that local variables

9
00:00:18,000 --> 00:00:22,279
that are non- structs are going to be

10
00:00:19,720 --> 00:00:25,119
stored on the stack additionally we see

11
00:00:22,279 --> 00:00:27,400
B here is the upper 32 bits not the

12
00:00:25,119 --> 00:00:29,519
lower 32 bits that's actually uh

13
00:00:27,400 --> 00:00:31,759
conducive to what we previously learned

14
00:00:29,519 --> 00:00:34,079
about when we had single local variable

15
00:00:31,759 --> 00:00:35,640
obsolete that would get stored at the

16
00:00:34,079 --> 00:00:37,520
upper bits and furthermore when there

17
00:00:35,640 --> 00:00:39,719
was extra stuff added it would get

18
00:00:37,520 --> 00:00:41,559
pushed down so it's kind of like this a

19
00:00:39,719 --> 00:00:44,079
pushed down the B the B is still at the

20
00:00:41,559 --> 00:00:45,879
upper 32 bits but really the takeaway

21
00:00:44,079 --> 00:00:48,239
here is that there's nothing special

22
00:00:45,879 --> 00:00:50,160
about pointers so we have a and b those

23
00:00:48,239 --> 00:00:51,920
are local variables like we always

24
00:00:50,160 --> 00:00:54,800
understand and then a pointer and B

25
00:00:51,920 --> 00:00:56,640
pointer by getting the address of a well

26
00:00:54,800 --> 00:01:01,440
that's just wherever a is stored on the

27
00:00:56,640 --> 00:01:04,159
stack so that address of a 7 ffffff 8 C8

28
00:01:01,440 --> 00:01:07,119
is going to be stored into the local

29
00:01:04,159 --> 00:01:09,880
variable a pointer so a pointer local

30
00:01:07,119 --> 00:01:12,799
variable stores an address specifically

31
00:01:09,880 --> 00:01:15,280
the address of a b pointer stores an

32
00:01:12,799 --> 00:01:18,920
address the address of B and here we see

33
00:01:15,280 --> 00:01:21,560
it is 8 C4 because it is not the lower

34
00:01:18,920 --> 00:01:23,640
four bytes is the upper four bytes so

35
00:01:21,560 --> 00:01:28,960
you skip past the lower four bytes and

36
00:01:23,640 --> 00:01:28,960
you get to 8 C4 is the address of B

