1
00:00:00,199 --> 00:00:05,640
in our third if example we seem to be

2
00:00:02,919 --> 00:00:08,559
playing that game of using some input

3
00:00:05,640 --> 00:00:11,240
from argv and then compiling as

4
00:00:08,559 --> 00:00:13,000
optimized so seem to be wanting to hide

5
00:00:11,240 --> 00:00:15,280
something from the compiler some sort of

6
00:00:13,000 --> 00:00:18,520
input that we're going to use to do a

7
00:00:15,280 --> 00:00:22,000
less than comparison is a less than

8
00:00:18,520 --> 00:00:23,680
input or is B less than input and if not

9
00:00:22,000 --> 00:00:27,160
if neither is true then we're going to

10
00:00:23,680 --> 00:00:31,279
return beats beats beats so here's our

11
00:00:27,160 --> 00:00:34,399
new assembly instructions BLT and L ltu

12
00:00:31,279 --> 00:00:36,239
why might we be seeing both a unsigned

13
00:00:34,399 --> 00:00:38,640
version apparently and a assigned

14
00:00:36,239 --> 00:00:40,680
version well if we look at A and B we

15
00:00:38,640 --> 00:00:42,760
see that one is a signed long and one is

16
00:00:40,680 --> 00:00:46,160
an unsigned long it's almost like I

17
00:00:42,760 --> 00:00:49,160
meant to do that okay BLT it's not a

18
00:00:46,160 --> 00:00:53,760
delicious sandwich it's a branch if less

19
00:00:49,160 --> 00:00:56,680
than so do the rs1 less than rs2 treat

20
00:00:53,760 --> 00:00:58,840
it as a signed number basically running

21
00:00:56,680 --> 00:01:01,000
theme here with RISC-V everything's

22
00:00:58,840 --> 00:01:04,360
signed unless you throw an unsigned

23
00:01:01,000 --> 00:01:06,960
indicator onto it so compare those and

24
00:01:04,360 --> 00:01:09,640
if they if that condition is true then

25
00:01:06,960 --> 00:01:12,040
go ahead and jump to the offset if not

26
00:01:09,640 --> 00:01:14,159
then go ahead and fall through once

27
00:01:12,040 --> 00:01:15,520
again always with these branch assembly

28
00:01:14,159 --> 00:01:18,479
instructions you're going to see an

29
00:01:15,520 --> 00:01:22,400
absolute address and branch if less than

30
00:01:18,479 --> 00:01:25,640
unsigned comparison same deal is rs1

31
00:01:22,400 --> 00:01:29,320
less than rs2 but treated as an unsigned

32
00:01:25,640 --> 00:01:31,240
comparison if so branch to the offset if

33
00:01:29,320 --> 00:01:34,040
not fall through to the next assembly

34
00:01:31,240 --> 00:01:35,759
instruction as always expect that you're

35
00:01:34,040 --> 00:01:37,439
not going to see an offset you're going

36
00:01:35,759 --> 00:01:40,360
to see an absolute address in the

37
00:01:37,439 --> 00:01:42,119
disassembler cool got some new assembly

38
00:01:40,360 --> 00:01:44,600
instructions go step through this

39
00:01:42,119 --> 00:01:46,079
assembly but this is going you know I

40
00:01:44,600 --> 00:01:47,680
think you should make a stack diagram

41
00:01:46,079 --> 00:01:49,280
for this and also there's something a

42
00:01:47,680 --> 00:01:51,159
little bit weird about this assembly

43
00:01:49,280 --> 00:01:53,280
that I didn't dwell upon so that you

44
00:01:51,159 --> 00:01:56,759
could go run into it yourself we'll

45
00:01:53,280 --> 00:01:56,759
dwell upon it when we come back

