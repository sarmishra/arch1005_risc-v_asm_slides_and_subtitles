1
00:00:00,719 --> 00:00:05,520
continuing our exploration of compound

2
00:00:03,679 --> 00:00:08,840
variable types we're now going to

3
00:00:05,520 --> 00:00:12,719
introduce structs or structures so I

4
00:00:08,840 --> 00:00:16,680
typed f a new mystruct of type mystruct

5
00:00:12,719 --> 00:00:19,359
T which shall contain a single short six

6
00:00:16,680 --> 00:00:22,600
integers all in an array and a single

7
00:00:19,359 --> 00:00:25,400
long long so that structure is declared

8
00:00:22,600 --> 00:00:29,119
my struct foo and then food. a which is

9
00:00:25,400 --> 00:00:31,279
the short is set to babe f. C is set to

10
00:00:29,119 --> 00:00:35,360
Balboa bled blood because it's always

11
00:00:31,279 --> 00:00:37,719
like I say to my wife Babe b BL blood

12
00:00:35,360 --> 00:00:40,719
anyways food. a is Babe which was a

13
00:00:37,719 --> 00:00:42,960
short it is now being assigned to an INT

14
00:00:40,719 --> 00:00:44,960
B of one that's going to lead to some

15
00:00:42,960 --> 00:00:48,440
sign extension because it is a signed

16
00:00:44,960 --> 00:00:50,480
short and a signed int so that 16bit

17
00:00:48,440 --> 00:00:52,879
value is sign extended up to a 32-bit

18
00:00:50,480 --> 00:00:55,960
value then we do math on the 32-bit

19
00:00:52,879 --> 00:00:57,600
Value plus the 64-bit value so again

20
00:00:55,960 --> 00:01:00,800
there's going to be some sign extension

21
00:00:57,600 --> 00:01:04,280
going on and storing it into a smaller

22
00:01:00,800 --> 00:01:07,520
variable the integer so 64-bit Value

23
00:01:04,280 --> 00:01:09,680
plus 32-bit value into a 32-bit storage

24
00:01:07,520 --> 00:01:12,119
and ultimately returning that storage

25
00:01:09,680 --> 00:01:14,840
once again compiled without the stack

26
00:01:12,119 --> 00:01:17,799
protector to simplify the code slightly

27
00:01:14,840 --> 00:01:20,280
so here's what we get and we get a bunch

28
00:01:17,799 --> 00:01:23,159
of new assembly instructions so let's

29
00:01:20,280 --> 00:01:26,560
check them out first we have store

30
00:01:23,159 --> 00:01:28,920
halfword so halfword again 16bit values

31
00:01:26,560 --> 00:01:31,360
that makes sense we had a 16bit value in

32
00:01:28,920 --> 00:01:34,119
this code that short a so to write the

33
00:01:31,360 --> 00:01:37,759
value babe to a we're going to store a

34
00:01:34,119 --> 00:01:40,720
half word so this is going to be a store

35
00:01:37,759 --> 00:01:42,920
which stores from left to right we've

36
00:01:40,720 --> 00:01:44,840
got a calculation of a memory address

37
00:01:42,920 --> 00:01:48,280
and that is the parentheses saying

38
00:01:44,840 --> 00:01:51,360
memory address and it is rs1 address

39
00:01:48,280 --> 00:01:53,719
plus sign extended immediate 12 that is

40
00:01:51,360 --> 00:01:57,039
the memory address where we shall store

41
00:01:53,719 --> 00:01:59,960
the halfword sized lower 16 bits from

42
00:01:57,039 --> 00:02:02,640
rs2 and again I'm a broken record here

43
00:01:59,960 --> 00:02:05,560
thanks to the miracle of copy and paste

44
00:02:02,640 --> 00:02:08,119
unlike most instructions stores are read

45
00:02:05,560 --> 00:02:10,319
from left to right this arrow goes that

46
00:02:08,119 --> 00:02:12,879
way for everything else it goes the

47
00:02:10,319 --> 00:02:14,879
opposite direction and the parentheses

48
00:02:12,879 --> 00:02:17,560
indicate the parentheses specifically in

49
00:02:14,879 --> 00:02:19,319
the disassembled output indicate that

50
00:02:17,560 --> 00:02:21,200
something is being used as a memory

51
00:02:19,319 --> 00:02:22,879
address and there is going to be a

52
00:02:21,200 --> 00:02:24,800
dereferencing of memory for either

53
00:02:22,879 --> 00:02:26,760
reading or writing on the flip side

54
00:02:24,800 --> 00:02:29,200
there is load halfword so we're going to

55
00:02:26,760 --> 00:02:33,200
get something back out from memory so

56
00:02:29,200 --> 00:02:36,400
this is red from right to left so rs1

57
00:02:33,200 --> 00:02:39,239
plus immediate 12 pluck a half word 16

58
00:02:36,400 --> 00:02:41,680
bits out of that and store it into this

59
00:02:39,239 --> 00:02:42,920
register now this is going to sign

60
00:02:41,680 --> 00:02:46,200
extended to

61
00:02:42,920 --> 00:02:48,920
32bits and if we're in 32-bit code then

62
00:02:46,200 --> 00:02:50,800
you're done but if you are in 64-bit

63
00:02:48,920 --> 00:02:53,840
code then it will further sign extended

64
00:02:50,800 --> 00:02:56,720
up to the full 64-bit size of your

65
00:02:53,840 --> 00:02:58,640
64-bit register broken record once again

66
00:02:56,720 --> 00:03:00,760
this instruction is read from right to

67
00:02:58,640 --> 00:03:03,040
left and because there's parentheses we

68
00:03:00,760 --> 00:03:05,879
know that it is using memory then we

69
00:03:03,040 --> 00:03:09,000
picked up another instruction add W for

70
00:03:05,879 --> 00:03:11,760
add word register to word register so

71
00:03:09,000 --> 00:03:14,280
two parameters a source one and a source

72
00:03:11,760 --> 00:03:16,440
two so add these two together and place

73
00:03:14,280 --> 00:03:20,120
the result in the destination but the

74
00:03:16,440 --> 00:03:22,440
key thing here is this is an RV64I only

75
00:03:20,120 --> 00:03:25,879
assembly instruction so it is dealing

76
00:03:22,440 --> 00:03:27,400
with word registers and normally RV64I

77
00:03:25,879 --> 00:03:29,720
is dealing with double word sized

78
00:03:27,400 --> 00:03:31,599
registers they're 64 bits but this is

79
00:03:29,720 --> 00:03:34,080
instruction is saying specifically I'm

80
00:03:31,599 --> 00:03:36,760
in 64-bit mode but I want to deal in

81
00:03:34,080 --> 00:03:40,360
32-bit registers so consequently it's

82
00:03:36,760 --> 00:03:43,760
going to take the bottom 32 bits of rs1

83
00:03:40,360 --> 00:03:47,120
and the bottom 32 bits of rs2 and add

84
00:03:43,760 --> 00:03:49,439
them together yielding a 32-bit result

85
00:03:47,120 --> 00:03:51,280
and then it will sign extend that 64

86
00:03:49,439 --> 00:03:52,720
bits and store the result into the

87
00:03:51,280 --> 00:03:54,519
destination register and this

88
00:03:52,720 --> 00:03:56,480
instruction isn't going to care if there

89
00:03:54,519 --> 00:03:58,519
was some sort of overflow it's just

90
00:03:56,480 --> 00:04:00,840
going to add them together and there's

91
00:03:58,519 --> 00:04:02,760
no indication anywhere that the result

92
00:04:00,840 --> 00:04:07,840
may have overflown and then there's the

93
00:04:02,760 --> 00:04:10,560
Oho conspicuous sex instruction so sexw

94
00:04:07,840 --> 00:04:13,519
specifically is sign extend word in

95
00:04:10,560 --> 00:04:16,919
register to double word register so this

96
00:04:13,519 --> 00:04:19,799
is an RV64I instruction and it is

97
00:04:16,919 --> 00:04:22,360
saying Hey I want to treat rs1 as a

98
00:04:19,799 --> 00:04:25,199
32-bit word register and I want that

99
00:04:22,360 --> 00:04:28,199
sign extended and just placed into the

100
00:04:25,199 --> 00:04:31,280
dword double word register destination

101
00:04:28,199 --> 00:04:33,960
but this is scandalous is it RISC-V

102
00:04:31,280 --> 00:04:36,080
encouraging sexting what about the

103
00:04:33,960 --> 00:04:39,199
children won't somebody please think of

104
00:04:36,080 --> 00:04:41,000
the children well it's just that UNH

105
00:04:39,199 --> 00:04:43,000
wholesome influence of those pseudo

106
00:04:41,000 --> 00:04:45,120
instructions at it again and so as a

107
00:04:43,000 --> 00:04:47,520
reminder I've never actually seen this

108
00:04:45,120 --> 00:04:49,240
vtv show so I have no context for what

109
00:04:47,520 --> 00:04:50,840
this picture is but I can tell you it's

110
00:04:49,240 --> 00:04:53,199
extra inappropriate when we add that

111
00:04:50,840 --> 00:04:55,360
little jiggle so let's go ahead and get

112
00:04:53,199 --> 00:04:57,240
rid of that UNH wholesome pseudo

113
00:04:55,360 --> 00:05:00,160
instruction through the use of chaos

114
00:04:57,240 --> 00:05:03,199
magic rewrite reality and there we we go

115
00:05:00,160 --> 00:05:05,840
nice clean assembly traditional Family

116
00:05:03,199 --> 00:05:09,039
Values no more Sexting in our assembly

117
00:05:05,840 --> 00:05:11,400
but we do pick up some compressed Lou

118
00:05:09,039 --> 00:05:14,520
some compressed adiw which we haven't

119
00:05:11,400 --> 00:05:16,280
seen adiw yet oh wait there it is AD IW

120
00:05:14,520 --> 00:05:18,720
down a couple assembly instructions

121
00:05:16,280 --> 00:05:20,560
later and a compressed ad w we just

122
00:05:18,720 --> 00:05:22,120
literally learned about ad W and now

123
00:05:20,560 --> 00:05:24,240
we're picking up the compressed form

124
00:05:22,120 --> 00:05:26,960
great hope you remember it well anyways

125
00:05:24,240 --> 00:05:30,160
the sexting is not sexting it's a pseudo

126
00:05:26,960 --> 00:05:33,720
instruction which is actually ad I

127
00:05:30,160 --> 00:05:37,000
and this zero for the immediate so what

128
00:05:33,720 --> 00:05:39,600
is ADD IW add IW is ADD immediate to

129
00:05:37,000 --> 00:05:42,080
word it's another one of these RV64I

130
00:05:39,600 --> 00:05:43,840
instructions where the W suffix is

131
00:05:42,080 --> 00:05:47,039
specifically telling us we want to deal

132
00:05:43,840 --> 00:05:48,919
in word-sized registers so we've got a

133
00:05:47,039 --> 00:05:50,639
source we've got an immediate we've got

134
00:05:48,919 --> 00:05:54,680
a destination so this is going to take

135
00:05:50,639 --> 00:05:57,240
the bottom 32bits word size of the rs1

136
00:05:54,680 --> 00:05:59,319
source register and then take the 12-bit

137
00:05:57,240 --> 00:06:01,600
immediate and sign extend it to only 32

138
00:05:59,319 --> 00:06:04,600
bits bits add them together yield a

139
00:06:01,600 --> 00:06:06,280
32-bit result and then sign extend that

140
00:06:04,600 --> 00:06:08,000
ultimately and place it into the

141
00:06:06,280 --> 00:06:10,720
destination register because we're still

142
00:06:08,000 --> 00:06:14,680
in RV 64 mode and it is a 64-bit

143
00:06:10,720 --> 00:06:17,319
register so this again like the ad W is

144
00:06:14,680 --> 00:06:19,400
going to ignore any sort of overflow

145
00:06:17,319 --> 00:06:20,960
that occurs as a result of this addition

146
00:06:19,400 --> 00:06:22,880
but the key thing is that we can see

147
00:06:20,960 --> 00:06:25,919
that if SE which behind the scenes is

148
00:06:22,880 --> 00:06:28,800
really an add IW destination source and

149
00:06:25,919 --> 00:06:31,560
a zero it's really just taking advantage

150
00:06:28,800 --> 00:06:34,039
of the fact that the ad IW is performing

151
00:06:31,560 --> 00:06:35,160
this sign extension after the addition

152
00:06:34,039 --> 00:06:36,880
and then it's saying you know what I'm

153
00:06:35,160 --> 00:06:40,120
going to add zero I'm going to just

154
00:06:36,880 --> 00:06:43,880
treat this as a word register 32bits add

155
00:06:40,120 --> 00:06:46,639
zero and then the adiw will do a 32 to

156
00:06:43,880 --> 00:06:49,120
64-bit sign extension for me so sexting

157
00:06:46,639 --> 00:06:51,479
is really just at IW when we got rid of

158
00:06:49,120 --> 00:06:54,720
the instruction aliases we did also see

159
00:06:51,479 --> 00:06:57,039
a compressed at IW and this is the same

160
00:06:54,720 --> 00:06:59,599
ultimate result but it is of course

161
00:06:57,039 --> 00:07:01,599
compressed it has less bits and so while

162
00:06:59,599 --> 00:07:03,720
it'll be written like destination

163
00:07:01,599 --> 00:07:06,199
register and an immediate that immediate

164
00:07:03,720 --> 00:07:08,720
is actually only a six-bit immediate and

165
00:07:06,199 --> 00:07:10,919
the destination is both a source and a

166
00:07:08,720 --> 00:07:15,160
destination so basically you're going to

167
00:07:10,919 --> 00:07:17,639
do Rd plus sign extended immediate six

168
00:07:15,160 --> 00:07:20,360
and store the result into Rd now there

169
00:07:17,639 --> 00:07:22,680
is a caveat here that Rd may not be the

170
00:07:20,360 --> 00:07:24,680
zero register because that's reserved

171
00:07:22,680 --> 00:07:27,319
and then moving back to before we

172
00:07:24,680 --> 00:07:28,800
learned about sex we had the ad W

173
00:07:27,319 --> 00:07:30,720
instruction we just learned about it

174
00:07:28,800 --> 00:07:33,879
this is now the compressed add word

175
00:07:30,720 --> 00:07:36,360
register to word register so add IW

176
00:07:33,879 --> 00:07:38,360
versus add W they're all just additions

177
00:07:36,360 --> 00:07:40,199
on word registers one of them is

178
00:07:38,360 --> 00:07:42,000
register to register ad and one of them

179
00:07:40,199 --> 00:07:44,479
is an immediate to register ad this is

180
00:07:42,000 --> 00:07:46,879
the register to register so it is giving

181
00:07:44,479 --> 00:07:49,120
us a destination in a source they are

182
00:07:46,879 --> 00:07:51,960
destination prime source prime that

183
00:07:49,120 --> 00:07:55,440
expands to the uncompressed form of

184
00:07:51,960 --> 00:07:57,759
destination plus source two because this

185
00:07:55,440 --> 00:07:59,879
is also implicitly source one source one

186
00:07:57,759 --> 00:08:01,759
source two add them together place them

187
00:07:59,879 --> 00:08:04,360
into the destination register but this

188
00:08:01,759 --> 00:08:07,960
is an add W so treat those things like

189
00:08:04,360 --> 00:08:10,639
they are 32-bit values so r d prime is

190
00:08:07,960 --> 00:08:13,240
the bottom 32 of whatever the initial Rd

191
00:08:10,639 --> 00:08:16,159
prime value is plus the bottom 32 bits

192
00:08:13,240 --> 00:08:19,120
of rs2 prime get a 32bit results sign

193
00:08:16,159 --> 00:08:21,639
extended to 64 bits a quick reminder on

194
00:08:19,120 --> 00:08:24,800
Lou from before LUI had a destination

195
00:08:21,639 --> 00:08:27,199
register and a upper immediate 20 bits

196
00:08:24,800 --> 00:08:29,319
and it set bits 31 through 12 so that we

197
00:08:27,199 --> 00:08:32,560
could ultimately create things like 32

198
00:08:29,319 --> 00:08:34,519
bit constants well there is a compressed

199
00:08:32,560 --> 00:08:37,599
LUI that popped up in this code and

200
00:08:34,519 --> 00:08:40,440
the encoding is like this got a nonzero

201
00:08:37,599 --> 00:08:42,080
upper immediate six and so it gets a

202
00:08:40,440 --> 00:08:43,919
little more complicated because it's not

203
00:08:42,080 --> 00:08:47,480
just setting the upper 20 bits it's

204
00:08:43,919 --> 00:08:49,640
setting bits 17 through 12 with our six

205
00:08:47,480 --> 00:08:52,279
bits there and then it takes and it s

206
00:08:49,640 --> 00:08:55,720
extends bit 17 to the rest of the upper

207
00:08:52,279 --> 00:08:58,360
bits and it zeros out the bottom 12 bits

208
00:08:55,720 --> 00:09:00,240
just like the Loui did before so I think

209
00:08:58,360 --> 00:09:02,040
um well let me just say quick that like

210
00:09:00,240 --> 00:09:04,839
the there is a further caveat that the

211
00:09:02,040 --> 00:09:07,279
destination register cannot be x0 or x2

212
00:09:04,839 --> 00:09:09,399
but I think this human readable text

213
00:09:07,279 --> 00:09:12,240
makes it a little clear compressed LUI

214
00:09:09,399 --> 00:09:15,160
loads the nonzero 6-bit immediate field

215
00:09:12,240 --> 00:09:18,440
into bits 17 to 12 of the destination

216
00:09:15,160 --> 00:09:21,760
register clears the bottom 12 bits and

217
00:09:18,440 --> 00:09:23,839
sign extends bit 17 into all higher bits

218
00:09:21,760 --> 00:09:25,920
of the destination all higher bits of

219
00:09:23,839 --> 00:09:28,640
the destination being a nice generic way

220
00:09:25,920 --> 00:09:30,480
to say that although this is an rv32

221
00:09:28,640 --> 00:09:32,920
compressed instruction if it was

222
00:09:30,480 --> 00:09:35,320
executed as it is here in the context of

223
00:09:32,920 --> 00:09:37,519
64-bit code all higher bits would mean

224
00:09:35,320 --> 00:09:40,680
it will sign extend that bit 17 all the

225
00:09:37,519 --> 00:09:43,000
way up to bit 63 the maximum size for

226
00:09:40,680 --> 00:09:44,360
the 64-bit destination register and so

227
00:09:43,000 --> 00:09:46,160
with that we've learned way more

228
00:09:44,360 --> 00:09:48,000
assembly instructions that we want to so

229
00:09:46,160 --> 00:09:51,079
it's time for you once again to go off

230
00:09:48,000 --> 00:09:54,440
and draw a stack diagram figure out how

231
00:09:51,079 --> 00:09:56,320
that my struct T is stored on the stack

232
00:09:54,440 --> 00:09:58,480
what's stored where what order is it

233
00:09:56,320 --> 00:10:01,640
reordered by the compiler let's figure

234
00:09:58,480 --> 00:10:04,079
it out by reading the code so go ahead

235
00:10:01,640 --> 00:10:07,200
and step through the code you can see it

236
00:10:04,079 --> 00:10:08,760
with your instruction aliases the sex or

237
00:10:07,200 --> 00:10:11,480
you can see it without the instruction

238
00:10:08,760 --> 00:10:15,279
alias is the ADDI whatever is more

239
00:10:11,480 --> 00:10:15,279
convenient for you

