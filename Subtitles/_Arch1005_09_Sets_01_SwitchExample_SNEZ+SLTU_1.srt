1
00:00:00,199 --> 00:00:05,359
so I want to give you a reminder and go

2
00:00:02,360 --> 00:00:07,319
back to switch example. C when we looked

3
00:00:05,359 --> 00:00:09,639
at that code before we saw some

4
00:00:07,319 --> 00:00:12,639
utilization of branch if equal to zero

5
00:00:09,639 --> 00:00:14,559
and branch if equal now I want to take

6
00:00:12,639 --> 00:00:18,119
it and instead of compiling at

7
00:00:14,559 --> 00:00:20,279
optimization level o0 instead let's just

8
00:00:18,119 --> 00:00:22,880
have the exact same code compiled at

9
00:00:20,279 --> 00:00:24,720
optimization level two if we do that we

10
00:00:22,880 --> 00:00:29,080
get the following code with a new

11
00:00:24,720 --> 00:00:32,599
assembly instruction SNES so SNES is the

12
00:00:29,080 --> 00:00:35,600
set if not equal to zero so it takes a

13
00:00:32,599 --> 00:00:38,559
destination and a source and if the

14
00:00:35,600 --> 00:00:41,239
source is not equal to zero then it sets

15
00:00:38,559 --> 00:00:44,559
the destination to one otherwise it sets

16
00:00:41,239 --> 00:00:47,920
the destination to zero so if source not

17
00:00:44,559 --> 00:00:50,160
equal to zero then set it to one else

18
00:00:47,920 --> 00:00:51,840
set it to zero but here's the thing tell

19
00:00:50,160 --> 00:00:55,000
me if you've heard this one before

20
00:00:51,840 --> 00:00:58,280
there's three types of lies lies damn

21
00:00:55,000 --> 00:00:59,760
lies and SNES because SNES is not

22
00:00:58,280 --> 00:01:02,480
actually a new assembly instruction it's

23
00:00:59,760 --> 00:01:06,439
a pseudo assembly instruction and it is

24
00:01:02,480 --> 00:01:10,920
actually a salt U so let's get rid of

25
00:01:06,439 --> 00:01:13,400
that odious snz and see the salt U new

26
00:01:10,920 --> 00:01:15,360
assembly instructions salt U and I'm

27
00:01:13,400 --> 00:01:17,280
going to say to pronounce these next

28
00:01:15,360 --> 00:01:20,280
instructions as salt and there's going

29
00:01:17,280 --> 00:01:24,520
to be four variants of them so salt you

30
00:01:20,280 --> 00:01:26,520
is set if less than unsigned comparison

31
00:01:24,520 --> 00:01:28,640
so set of less than is going to take two

32
00:01:26,520 --> 00:01:31,840
parameters now a source one and a source

33
00:01:28,640 --> 00:01:34,159
two and if source one is less than

34
00:01:31,840 --> 00:01:36,079
source 2 then it's going to set the

35
00:01:34,159 --> 00:01:38,280
destination to one else it's going to

36
00:01:36,079 --> 00:01:40,720
set it to zero and the key thing about

37
00:01:38,280 --> 00:01:44,200
salt U is that it is an unsigned

38
00:01:40,720 --> 00:01:45,920
comparison so this less than is unsigned

39
00:01:44,200 --> 00:01:47,560
they will not be treated as signed

40
00:01:45,920 --> 00:01:50,840
number so they can't be treated as

41
00:01:47,560 --> 00:01:54,759
negative so if we said snz is actually

42
00:01:50,840 --> 00:01:56,840
just salt you with a x0 with a zero

43
00:01:54,759 --> 00:01:59,520
hard-coded zero in the source one

44
00:01:56,840 --> 00:02:02,560
register then what it's doing is it's if

45
00:01:59,520 --> 00:02:04,920
Z Z is less than your number and your

46
00:02:02,560 --> 00:02:07,439
number is unsigned so it can't possibly

47
00:02:04,920 --> 00:02:10,679
be treated as a negative number well

48
00:02:07,439 --> 00:02:13,280
then basically everything except zero is

49
00:02:10,679 --> 00:02:16,560
going to set one because nothing else is

50
00:02:13,280 --> 00:02:18,319
less than zero but Z is equal to zero so

51
00:02:16,560 --> 00:02:20,760
that won't be true and instead it'll go

52
00:02:18,319 --> 00:02:23,879
to the if and it'll set the destination

53
00:02:20,760 --> 00:02:27,360
to zero so snz set if not zero which

54
00:02:23,879 --> 00:02:29,879
means everything's not zero except zero

55
00:02:27,360 --> 00:02:31,200
okay so go ahead and stop walk through

56
00:02:29,879 --> 00:02:33,760
the assembly and check your

57
00:02:31,200 --> 00:02:33,760
understanding

