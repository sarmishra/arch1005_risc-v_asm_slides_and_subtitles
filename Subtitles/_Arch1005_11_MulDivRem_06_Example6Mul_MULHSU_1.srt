1
00:00:00,160 --> 00:00:05,319
and in our final example 6 M.C we are

2
00:00:03,480 --> 00:00:07,720
going to pick up our final assembly

3
00:00:05,319 --> 00:00:10,480
instruction so in this code we've got

4
00:00:07,720 --> 00:00:13,200
a2l again being used to pull in a signed

5
00:00:10,480 --> 00:00:15,440
128bit value but of course a2l is only

6
00:00:13,200 --> 00:00:18,240
going to have a 32-bit value which will

7
00:00:15,440 --> 00:00:20,680
subsequently be sign extended up to 128

8
00:00:18,240 --> 00:00:22,439
and then we have a2l being used to pull

9
00:00:20,680 --> 00:00:25,640
in a second value but that value is

10
00:00:22,439 --> 00:00:30,000
being assigned to a b64 which is an

11
00:00:25,640 --> 00:00:33,520
unsigned long long then b64 * the 128

12
00:00:30,000 --> 00:00:36,559
and 128 times itself so here's our

13
00:00:33,520 --> 00:00:41,039
assembly New assembly instruction mul

14
00:00:36,559 --> 00:00:44,520
Hsu so mul Hsu is the multiply keep the

15
00:00:41,039 --> 00:00:47,840
high xll and it is signed times unsigned

16
00:00:44,520 --> 00:00:50,399
for that upper bits so treat the rs1

17
00:00:47,840 --> 00:00:53,000
like it is signed treat the rs2 like it

18
00:00:50,399 --> 00:00:56,359
is unsigned multiply them together and

19
00:00:53,000 --> 00:00:59,559
keep only the high XLEN bits so that's

20
00:00:56,359 --> 00:01:02,039
again what that says and M Hsu is meant

21
00:00:59,559 --> 00:01:03,480
to be used when rs1 is signed and rs2 is

22
00:01:02,039 --> 00:01:06,000
unsigned unlike the other ones we've

23
00:01:03,480 --> 00:01:09,759
seen thus far m h everything is

24
00:01:06,000 --> 00:01:12,840
implicitly signed m h u unsigned times

25
00:01:09,759 --> 00:01:14,759
unsigned and here M Hsu is the signed

26
00:01:12,840 --> 00:01:17,600
times unsigned and of course you don't

27
00:01:14,759 --> 00:01:19,360
need to mul u s because the compiler can

28
00:01:17,600 --> 00:01:21,280
just flip around the arguments if it

29
00:01:19,360 --> 00:01:24,000
needs to put the signed on the

30
00:01:21,280 --> 00:01:26,479
appropriate side so now a little more

31
00:01:24,000 --> 00:01:29,280
creative example than I used previously

32
00:01:26,479 --> 00:01:32,799
let's assume that we have t0 in a mle

33
00:01:29,280 --> 00:01:35,200
Hsu of -2 so we said the first argument

34
00:01:32,799 --> 00:01:36,720
rs1 is supposed to be treated as signed

35
00:01:35,200 --> 00:01:39,560
so that will not be the big positive

36
00:01:36,720 --> 00:01:42,680
value that will be the -2 we're going to

37
00:01:39,560 --> 00:01:46,280
multiply that by hex2 which is decimal

38
00:01:42,680 --> 00:01:49,320
32 and for that the upper bits will all

39
00:01:46,280 --> 00:01:52,280
be one or sorry all FS are all binary

40
00:01:49,320 --> 00:01:54,240
ones and then the lower bits will be

41
00:01:52,280 --> 00:01:59,960
calculated by a normal mole and we've

42
00:01:54,240 --> 00:02:04,439
got -2 * 32 and that should be -64 or

43
00:01:59,960 --> 00:02:07,280
heximal FS c0 so negative hex 40 so

44
00:02:04,439 --> 00:02:11,360
upper bits are all FS because the

45
00:02:07,280 --> 00:02:14,560
overall 128bit value should be -64 and

46
00:02:11,360 --> 00:02:18,680
that should just be z C and all FS all

47
00:02:14,560 --> 00:02:20,920
the way up to 128 bits okay well let's

48
00:02:18,680 --> 00:02:23,040
go collect that star and get the last

49
00:02:20,920 --> 00:02:25,280
assembly instruction once again there's

50
00:02:23,040 --> 00:02:27,160
a few multiplies used in this so I

51
00:02:25,280 --> 00:02:28,959
recommend you put comments on every

52
00:02:27,160 --> 00:02:31,400
single line of the code so that you are

53
00:02:28,959 --> 00:02:34,000
super sure what exactly is going on with

54
00:02:31,400 --> 00:02:34,000
the code

