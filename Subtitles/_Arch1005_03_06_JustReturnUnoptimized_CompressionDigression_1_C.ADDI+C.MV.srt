1
00:00:00,760 --> 00:00:03,879
all right so now it's time to come back

2
00:00:02,280 --> 00:00:05,720
to those compressed assembly

3
00:00:03,879 --> 00:00:08,120
instructions that I said we would get

4
00:00:05,720 --> 00:00:10,840
back to later so as a reminder when we

5
00:00:08,120 --> 00:00:14,320
compile just return unoptimized with the

6
00:00:10,840 --> 00:00:16,279
o0 optimization level zero we see that

7
00:00:14,320 --> 00:00:18,359
there are assembly instructions which

8
00:00:16,279 --> 00:00:20,880
are two bytes from each other the SD is

9
00:00:18,359 --> 00:00:22,920
two bytes after the ADI the next ADI is

10
00:00:20,880 --> 00:00:25,080
two bytes after the SD and so forth and

11
00:00:22,920 --> 00:00:26,960
we said that two by assembly instruction

12
00:00:25,080 --> 00:00:28,679
differences is a good indicator that

13
00:00:26,960 --> 00:00:31,679
there's probably compressed instructions

14
00:00:28,679 --> 00:00:32,680
involved so now let's go ahead and do a

15
00:00:31,679 --> 00:00:36,320
deep

16
00:00:32,680 --> 00:00:38,960
dive so invoking Scarlet witches chaos

17
00:00:36,320 --> 00:00:41,239
magic let's go ahead and rewrite reality

18
00:00:38,960 --> 00:00:43,039
and show you that this is significantly

19
00:00:41,239 --> 00:00:46,440
more complicated and stuff we haven't

20
00:00:43,039 --> 00:00:49,120
seen before so first we have a C ADDI

21
00:00:46,440 --> 00:00:51,480
so compressed ADDI now I'm indicating

22
00:00:49,120 --> 00:00:53,520
that just with a compressed star because

23
00:00:51,480 --> 00:00:55,600
we have seen something called ADDI

24
00:00:53,520 --> 00:00:57,359
before so that's not new it's basically

25
00:00:55,600 --> 00:00:59,680
should just be a normal ad with

26
00:00:57,359 --> 00:01:01,039
immediate and it should you know we see

27
00:00:59,680 --> 00:01:03,160
there's there's two arguments instead of

28
00:01:01,039 --> 00:01:04,879
three so we'll still go into exactly how

29
00:01:03,160 --> 00:01:06,960
it's specified but it's probably going

30
00:01:04,879 --> 00:01:09,640
to be just a simpler form of the Addam

31
00:01:06,960 --> 00:01:11,439
median instruction next however we see

32
00:01:09,640 --> 00:01:14,439
the c.

33
00:01:11,439 --> 00:01:16,880
sdsp so that is compressed but I'm also

34
00:01:14,439 --> 00:01:18,759
going to put the yellow star here for

35
00:01:16,880 --> 00:01:20,680
something we haven't seen before we

36
00:01:18,759 --> 00:01:23,439
haven't seen any assembly instruction

37
00:01:20,680 --> 00:01:25,400
called sdsp so we'll just treat that as

38
00:01:23,439 --> 00:01:28,880
a new assembly instruction until we find

39
00:01:25,400 --> 00:01:30,920
out otherwise similarly we've got ci4

40
00:01:28,880 --> 00:01:34,799
SPN and we definitely haven't seen

41
00:01:30,920 --> 00:01:36,920
anything called at I4 SPN then we have C

42
00:01:34,799 --> 00:01:39,000
move and that seems to be something

43
00:01:36,920 --> 00:01:42,159
we've seen before just a move and two

44
00:01:39,000 --> 00:01:43,680
arguments a5 into a0 seems like it's

45
00:01:42,159 --> 00:01:45,600
going to behave just like a normal

46
00:01:43,680 --> 00:01:47,759
uncompressed move so we'll just call

47
00:01:45,600 --> 00:01:50,360
that something we've seen before and

48
00:01:47,759 --> 00:01:53,079
finally we've got the c.

49
00:01:50,360 --> 00:01:54,560
ldsp and that is going to be compressed

50
00:01:53,079 --> 00:01:56,799
but also new because we haven't seen

51
00:01:54,560 --> 00:01:58,920
anything called ldsp so I'm going to go

52
00:01:56,799 --> 00:02:01,399
ahead and check off this C move I'm not

53
00:01:58,920 --> 00:02:02,920
going to say that you need to see a

54
00:02:01,399 --> 00:02:04,439
slide specifically telling you how that

55
00:02:02,920 --> 00:02:07,000
works just treat it like a normal

56
00:02:04,439 --> 00:02:08,840
uncompressed move just taking this

57
00:02:07,000 --> 00:02:10,679
argument whatever the value is in this

58
00:02:08,840 --> 00:02:12,879
register and placing it into that

59
00:02:10,679 --> 00:02:15,599
register but let's go ahead and look at

60
00:02:12,879 --> 00:02:17,760
compressed ADDI as a reminder here's

61
00:02:15,599 --> 00:02:20,440
what the ADDI looked like it had three

62
00:02:17,760 --> 00:02:22,959
arguments not two and it had a source an

63
00:02:20,440 --> 00:02:25,680
immediate 12 which is a s extended

64
00:02:22,959 --> 00:02:27,840
immediate 12 and a destination register

65
00:02:25,680 --> 00:02:30,319
it just added this to that and stored it

66
00:02:27,840 --> 00:02:32,599
there so what is the compressed app I

67
00:02:30,319 --> 00:02:35,080
well it takes two arguments a

68
00:02:32,599 --> 00:02:38,480
destination register and a nonzero

69
00:02:35,080 --> 00:02:41,360
immediate 6 so a nonzero sign extended

70
00:02:38,480 --> 00:02:43,560
6bit immediate so what is the math here

71
00:02:41,360 --> 00:02:46,040
well it's an ADDI so we've got the

72
00:02:43,560 --> 00:02:48,280
destination is going to act as both a

73
00:02:46,040 --> 00:02:52,000
source and a destination so the

74
00:02:48,280 --> 00:02:54,959
destination register plus the nonzero

75
00:02:52,000 --> 00:02:57,040
sign extended six bit imediate and so

76
00:02:54,959 --> 00:02:59,360
this plus that and store it back into

77
00:02:57,040 --> 00:03:01,360
the destination and also just one minor

78
00:02:59,360 --> 00:03:03,040
caveat that Rd can't be the zero

79
00:03:01,360 --> 00:03:05,280
register of course that would do a whole

80
00:03:03,040 --> 00:03:07,480
bunch of nothing if it was so it is

81
00:03:05,280 --> 00:03:09,840
worth saying that in the documentation

82
00:03:07,480 --> 00:03:13,319
it says the code points with Rd equal to

83
00:03:09,840 --> 00:03:14,799
x0 encodes the cop instruction so yeah

84
00:03:13,319 --> 00:03:16,280
that would do a whole lot of nothing so

85
00:03:14,799 --> 00:03:18,200
much nothing that they decided to

86
00:03:16,280 --> 00:03:21,879
designate it the compressed noop

87
00:03:18,200 --> 00:03:24,319
instruction so if normal noop is ADD ey

88
00:03:21,879 --> 00:03:26,599
compressed noop is actually compressed

89
00:03:24,319 --> 00:03:28,760
ADDI okay and that's pretty much it for

90
00:03:26,599 --> 00:03:31,680
compressed ADDI let's go ahead and

91
00:03:28,760 --> 00:03:31,680
check that off

