1
00:00:00,320 --> 00:00:03,879
before we start learning about our new

2
00:00:02,200 --> 00:00:05,960
assembly instructions just a quick

3
00:00:03,879 --> 00:00:08,240
reminder about multiplication in General

4
00:00:05,960 --> 00:00:11,160
on computers given that we have limited

5
00:00:08,240 --> 00:00:13,559
and fixed register sizes in general if

6
00:00:11,160 --> 00:00:15,519
you multiply two registers together very

7
00:00:13,559 --> 00:00:18,080
often the results will not fit into the

8
00:00:15,519 --> 00:00:20,039
same size register and generically

9
00:00:18,080 --> 00:00:22,439
speaking if you multiply two x bit

10
00:00:20,039 --> 00:00:24,640
registers together you will need at most

11
00:00:22,439 --> 00:00:27,039
two to the X bits worth of space to

12
00:00:24,640 --> 00:00:28,760
store all the possible values giving you

13
00:00:27,039 --> 00:00:31,119
a concrete example let's pretend we had

14
00:00:28,760 --> 00:00:34,440
one byte registers the values are 0 to

15
00:00:31,119 --> 00:00:37,800
FF but even the small values of hex 10 *

16
00:00:34,440 --> 00:00:40,320
hex 10 yields hex 100 which does not fit

17
00:00:37,800 --> 00:00:44,480
in a one byte register and the maximum

18
00:00:40,320 --> 00:00:46,239
values of all FS time all FS yields fe01

19
00:00:44,480 --> 00:00:48,160
which again does not fit in a 1 byte

20
00:00:46,239 --> 00:00:50,199
register you would either need to have a

21
00:00:48,160 --> 00:00:51,960
two byte register available or you would

22
00:00:50,199 --> 00:00:54,800
have to store the upper half into a one

23
00:00:51,960 --> 00:00:57,079
byte register and the lower half same

24
00:00:54,800 --> 00:00:58,519
thing with four bytes and eight bytes

25
00:00:57,079 --> 00:01:01,079
and everything else I'm just using four

26
00:00:58,519 --> 00:01:04,879
bytes here because it's a little smaller

27
00:01:01,079 --> 00:01:07,119
10,000 * 10,000 yields 1 and8 zeros but

28
00:01:04,879 --> 00:01:10,600
those eight zeros are like the least

29
00:01:07,119 --> 00:01:13,439
significant um four bytes and the one is

30
00:01:10,600 --> 00:01:15,119
now overflowed into an upper 4 bytes

31
00:01:13,439 --> 00:01:17,280
that would be necessary to store this

32
00:01:15,119 --> 00:01:20,560
result and so the same thing all FS

33
00:01:17,280 --> 00:01:23,439
times all FS would give us f f

34
00:01:20,560 --> 00:01:26,040
ff00 e which obviously doesn't fit in a

35
00:01:23,439 --> 00:01:28,840
4 by register you would need either one

36
00:01:26,040 --> 00:01:31,360
8 by register or you would have to store

37
00:01:28,840 --> 00:01:33,119
the upper 4 bytes and the lower 4 bytes

38
00:01:31,360 --> 00:01:34,640
so the same principle applies to

39
00:01:33,119 --> 00:01:37,159
computers no matter what size of

40
00:01:34,640 --> 00:01:40,159
registers you're using here in our code

41
00:01:37,159 --> 00:01:42,520
it is 64bit and therefore we have 8 byte

42
00:01:40,159 --> 00:01:45,479
registers but the same thing if you had

43
00:01:42,520 --> 00:01:47,479
RV 128 and they had 16 byte registers

44
00:01:45,479 --> 00:01:49,200
you always have this potential that if

45
00:01:47,479 --> 00:01:51,799
you're trying to multiply native

46
00:01:49,200 --> 00:01:53,920
register size like XLEN time x length

47
00:01:51,799 --> 00:01:57,119
you're going to need 2 * X length worth

48
00:01:53,920 --> 00:01:59,039
of space so also one property about

49
00:01:57,119 --> 00:02:01,240
binary multiplication that I want to

50
00:01:59,039 --> 00:02:03,520
inform you about and here I've uh

51
00:02:01,240 --> 00:02:05,560
borrowed a slide from this arm assembly

52
00:02:03,520 --> 00:02:07,840
for embedded applications textbook

53
00:02:05,560 --> 00:02:10,000
website and there is a mathematical

54
00:02:07,840 --> 00:02:13,120
property that whenever you're doing some

55
00:02:10,000 --> 00:02:16,000
xll here we'll say is four bits some xll

56
00:02:13,120 --> 00:02:18,800
time xll thing the result will be 2 * X

57
00:02:16,000 --> 00:02:21,440
Ling but the bottom Xing will always be

58
00:02:18,800 --> 00:02:24,200
the same regardless of whether the input

59
00:02:21,440 --> 00:02:26,680
parameters are interpreted as unsigned

60
00:02:24,200 --> 00:02:28,840
or whether they're interpreted as signed

61
00:02:26,680 --> 00:02:31,760
so you can go ahead and go see that book

62
00:02:28,840 --> 00:02:33,680
and the website to and go to the uh

63
00:02:31,760 --> 00:02:35,920
integer arithmetic which is where this

64
00:02:33,680 --> 00:02:37,879
slide is from and he'll show you the

65
00:02:35,920 --> 00:02:39,920
mathematical reason why that's true but

66
00:02:37,879 --> 00:02:42,480
I'm just going to handwave and say just

67
00:02:39,920 --> 00:02:45,000
assume that if you multiply two XLEN

68
00:02:42,480 --> 00:02:46,159
registers together the bottom bits will

69
00:02:45,000 --> 00:02:48,280
always be the same for the

70
00:02:46,159 --> 00:02:50,319
multiplication regardless of whether you

71
00:02:48,280 --> 00:02:52,760
interpret the values in the registers as

72
00:02:50,319 --> 00:02:55,239
signed or unsigned so RISC architectures

73
00:02:52,760 --> 00:02:56,800
like Risk 5 or arm will take advantage

74
00:02:55,239 --> 00:02:58,640
of this property and they will have

75
00:02:56,800 --> 00:03:01,319
multiply instructions that will give you

76
00:02:58,640 --> 00:03:02,959
only the lower half and then separate

77
00:03:01,319 --> 00:03:05,280
multiply instructions that will give you

78
00:03:02,959 --> 00:03:07,360
the upper half depending on whether it's

79
00:03:05,280 --> 00:03:09,200
interpreted as unsigned multiplication

80
00:03:07,360 --> 00:03:11,280
or signed multiplication and we're going

81
00:03:09,200 --> 00:03:15,720
to see the same pattern play out here in

82
00:03:11,280 --> 00:03:18,720
Risk 5 so example one moldiv REM so we

83
00:03:15,720 --> 00:03:21,040
have our u64 defined as unsigned long

84
00:03:18,720 --> 00:03:22,760
long so we have an A which is unsigned

85
00:03:21,040 --> 00:03:26,120
long long and we initialize it to

86
00:03:22,760 --> 00:03:29,360
defected we multiply times detectable we

87
00:03:26,120 --> 00:03:32,239
divide by to bad so s and we modulus by

88
00:03:29,360 --> 00:03:34,200
laog or 101 if we compile this at

89
00:03:32,239 --> 00:03:35,879
optimization level zero we get the

90
00:03:34,200 --> 00:03:39,239
following assembly code with three new

91
00:03:35,879 --> 00:03:40,680
assembly instructions M Dev VI and remu

92
00:03:39,239 --> 00:03:43,280
seemingly associated with our

93
00:03:40,680 --> 00:03:47,280
multiplication division and remainer or

94
00:03:43,280 --> 00:03:50,439
modulus operation so first mul mul is a

95
00:03:47,280 --> 00:03:52,480
multiply that keeps only the lower XLEN

96
00:03:50,439 --> 00:03:55,280
bits and this is because as I said

97
00:03:52,480 --> 00:03:57,680
before when you have xll * xll

98
00:03:55,280 --> 00:03:59,319
multiplication the lower xll bits will

99
00:03:57,680 --> 00:04:01,720
always be the same regardless of whether

100
00:03:59,319 --> 00:04:04,879
you have signed or unsigned operants so

101
00:04:01,720 --> 00:04:08,159
just rs1 the source one times rs2 the

102
00:04:04,879 --> 00:04:10,799
source 2 keep only the lower XLEN of the

103
00:04:08,159 --> 00:04:12,519
product and store it into destination

104
00:04:10,799 --> 00:04:14,120
and that's exactly what that's saying

105
00:04:12,519 --> 00:04:16,359
looking at a concrete example of that

106
00:04:14,120 --> 00:04:19,840
let's say we had multiply T2 is our

107
00:04:16,359 --> 00:04:21,000
destination t0 and T1 is our source t 0

108
00:04:19,840 --> 00:04:25,600
set to

109
00:04:21,000 --> 00:04:28,680
2times T1 set to 8 and so 8 * 2 is 16

110
00:04:25,600 --> 00:04:31,160
and that is hex 16 that's expected now a

111
00:04:28,680 --> 00:04:35,320
little more complicated we have t0 of

112
00:04:31,160 --> 00:04:37,759
two T1 of all eights and if we multiply

113
00:04:35,320 --> 00:04:40,160
these together we see that the result is

114
00:04:37,759 --> 00:04:42,199
somehow smaller than the inputs well

115
00:04:40,160 --> 00:04:45,520
it's not smaller it's just that this

116
00:04:42,199 --> 00:04:48,039
overflowed the 64-bit available space

117
00:04:45,520 --> 00:04:51,160
and this is now only keeping the lower

118
00:04:48,039 --> 00:04:53,280
XLEN bits the lower 64 bits of the

119
00:04:51,160 --> 00:04:55,000
product if we wanted to see the upper

120
00:04:53,280 --> 00:04:56,560
XLEN bits we will have to use a

121
00:04:55,000 --> 00:04:58,759
different multiply instruction to keep

122
00:04:56,560 --> 00:05:00,199
the upper or high bits which we'll learn

123
00:04:58,759 --> 00:05:03,600
about later

124
00:05:00,199 --> 00:05:05,800
okay then we have div VI divide unsigned

125
00:05:03,600 --> 00:05:08,080
so it's going to treat the upper ends as

126
00:05:05,800 --> 00:05:10,120
unsigned source one and source two it's

127
00:05:08,080 --> 00:05:12,320
going to divide and it's going to round

128
00:05:10,120 --> 00:05:15,520
toward zero so remember that when you're

129
00:05:12,320 --> 00:05:17,320
doing integer division in C you can't

130
00:05:15,520 --> 00:05:19,240
get a floating point result you can't

131
00:05:17,320 --> 00:05:21,039
get something with the decimal places

132
00:05:19,240 --> 00:05:24,479
and stuff like that so it's basically

133
00:05:21,039 --> 00:05:28,039
going to say how many times does rs2 go

134
00:05:24,479 --> 00:05:29,520
into rs1 evenly and basically then have

135
00:05:28,039 --> 00:05:31,880
the remainder stored elsewhere so it's

136
00:05:29,520 --> 00:05:33,800
it's only sort of the the division that

137
00:05:31,880 --> 00:05:38,160
goes an even number of times into the

138
00:05:33,800 --> 00:05:41,680
source one so concretely if we had five

139
00:05:38,160 --> 00:05:45,199
and we divided it by six well six goes

140
00:05:41,680 --> 00:05:47,319
into five Z times and so the net result

141
00:05:45,199 --> 00:05:50,000
of that is zero if on the other hand we

142
00:05:47,319 --> 00:05:52,919
had some very large unsigned number like

143
00:05:50,000 --> 00:05:56,080
FS ending in seven and we divided that

144
00:05:52,919 --> 00:05:58,440
by three we would get this all fives

145
00:05:56,080 --> 00:05:59,720
ending in two but we can perhaps have an

146
00:05:58,440 --> 00:06:02,479
intuition just by trying trying to

147
00:05:59,720 --> 00:06:07,080
multiply these back together that all of

148
00:06:02,479 --> 00:06:09,319
this this fives ending in two * 3 well 3

149
00:06:07,080 --> 00:06:11,599
* 2 is going to be six so this is

150
00:06:09,319 --> 00:06:14,160
probably going to be all FS ending in

151
00:06:11,599 --> 00:06:15,720
six so there's probably a one remainder

152
00:06:14,160 --> 00:06:19,080
here and we'll see that when we look at

153
00:06:15,720 --> 00:06:22,039
the remainder operation next remu

154
00:06:19,080 --> 00:06:24,520
remainder unsigned same thing unsigned

155
00:06:22,039 --> 00:06:27,680
operand the percentage or modulus

156
00:06:24,520 --> 00:06:29,759
operator in C is going to give you the

157
00:06:27,680 --> 00:06:32,080
remainder and the remainder is kind of

158
00:06:29,759 --> 00:06:34,120
like if you did the source one divided

159
00:06:32,080 --> 00:06:35,599
by source 2 but keeping in mind that

160
00:06:34,120 --> 00:06:37,319
that's not going to be floating point so

161
00:06:35,599 --> 00:06:39,560
it's just the number of times that it

162
00:06:37,319 --> 00:06:41,919
goes in evenly and then multiply that

163
00:06:39,560 --> 00:06:44,720
back times source 2 if there's any sort

164
00:06:41,919 --> 00:06:47,479
of remainder this result will be less

165
00:06:44,720 --> 00:06:49,440
than this result and so rs1 minus this

166
00:06:47,479 --> 00:06:51,919
result will give you whatever the

167
00:06:49,440 --> 00:06:56,080
leftover or remainder is after that

168
00:06:51,919 --> 00:07:00,000
division so concretely we had 5 / 6

169
00:06:56,080 --> 00:07:02,360
before but if we instead do 5 modul 6

170
00:07:00,000 --> 00:07:06,479
the result is going to be five because

171
00:07:02,360 --> 00:07:08,800
this rs1 / rs2 gave us a zero before

172
00:07:06,479 --> 00:07:11,800
that cancels out this entire term so

173
00:07:08,800 --> 00:07:15,080
it's rs1 which is 5 minus 0 and the

174
00:07:11,800 --> 00:07:18,039
remainder is five also again we had all

175
00:07:15,080 --> 00:07:20,400
FS ending in seven and before the

176
00:07:18,039 --> 00:07:23,560
results of that division so all FS

177
00:07:20,400 --> 00:07:26,080
ending in seven divided by three gave us

178
00:07:23,560 --> 00:07:28,599
all fives ending in two but basically I

179
00:07:26,080 --> 00:07:30,560
said all fives ending in two times the

180
00:07:28,599 --> 00:07:33,800
three would probably give us all FS

181
00:07:30,560 --> 00:07:36,120
ending in six so this would be rs1 of

182
00:07:33,800 --> 00:07:38,599
all FS ending in seven minus all FS

183
00:07:36,120 --> 00:07:41,080
ending in six and that gives us a

184
00:07:38,599 --> 00:07:43,440
remainder of one okay so now it's time

185
00:07:41,080 --> 00:07:45,479
for you to go look at the source code

186
00:07:43,440 --> 00:07:47,440
look at the assembly step through it in

187
00:07:45,479 --> 00:07:48,720
a debugger and make sure you understand

188
00:07:47,440 --> 00:07:50,759
what's going on with these new

189
00:07:48,720 --> 00:07:55,039
instructions we learned multiply keep

190
00:07:50,759 --> 00:07:58,520
the bottom X bits divide as unsigned and

191
00:07:55,039 --> 00:07:58,520
remainder as unsigned

