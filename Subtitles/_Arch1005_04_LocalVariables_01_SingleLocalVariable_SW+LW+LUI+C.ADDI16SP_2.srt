1
00:00:00,880 --> 00:00:05,040
okay so if everything went correctly you

2
00:00:02,919 --> 00:00:06,759
should check your stack diagram and it

3
00:00:05,040 --> 00:00:08,800
should look something like this go ahead

4
00:00:06,759 --> 00:00:10,200
and pause if you'd like and take a look

5
00:00:08,800 --> 00:00:11,519
at this and compare it against your

6
00:00:10,200 --> 00:00:13,599
stack

7
00:00:11,519 --> 00:00:15,280
diagram now this diagram may look

8
00:00:13,599 --> 00:00:17,039
slightly different from yours because I

9
00:00:15,280 --> 00:00:18,640
didn't walk all the way down through the

10
00:00:17,039 --> 00:00:20,160
code because that would have meant the

11
00:00:18,640 --> 00:00:22,480
stack pointer got moved back up and the

12
00:00:20,160 --> 00:00:24,760
frame pointer moved back up so this is

13
00:00:22,480 --> 00:00:29,480
the state of the system if I step

14
00:00:24,760 --> 00:00:31,759
through this line 5555 Etc 636 which is

15
00:00:29,480 --> 00:00:33,680
main plus 14 so that's essentially we're

16
00:00:31,759 --> 00:00:35,480
stepping through the last store to the

17
00:00:33,680 --> 00:00:37,680
stack so the last time anything has

18
00:00:35,480 --> 00:00:39,399
changed on the stack and then after that

19
00:00:37,680 --> 00:00:42,559
then it's just going to be you know tear

20
00:00:39,399 --> 00:00:44,800
down of the actual main function so we

21
00:00:42,559 --> 00:00:47,120
have this state that I already said

22
00:00:44,800 --> 00:00:50,160
before but what got filled in was this

23
00:00:47,120 --> 00:00:52,120
saved frame pointer and so that was the

24
00:00:50,160 --> 00:00:55,600
value of frame pointer which used to

25
00:00:52,120 --> 00:00:58,399
point up here this 7 fffff

26
00:00:55,600 --> 00:00:59,920
a88 that got stored onto the stack at

27
00:00:58,399 --> 00:01:04,080
some point and that would be at this

28
00:00:59,920 --> 00:01:07,080
specific location fs and then 8 f8 then

29
00:01:04,080 --> 00:01:08,920
you have at 8 F0 uninitialized that is

30
00:01:07,080 --> 00:01:11,600
never touched by anyone that is never

31
00:01:08,920 --> 00:01:13,320
modified and so I'm choosing to put a

32
00:01:11,600 --> 00:01:15,799
Convention of putting it in Gray and

33
00:01:13,320 --> 00:01:20,920
just leaving it as uninitialized then at

34
00:01:15,799 --> 00:01:23,640
this address we have S fs and 8 E8 that

35
00:01:20,920 --> 00:01:26,439
is the address of the lower 32 bits and

36
00:01:23,640 --> 00:01:29,000
those lower 32 bits were never actually

37
00:01:26,439 --> 00:01:30,960
touched and so that is uninitialized so

38
00:01:29,000 --> 00:01:32,640
here I'm showing green Fading Into gray

39
00:01:30,960 --> 00:01:35,680
because the bottom 32 bits are

40
00:01:32,640 --> 00:01:37,640
uninitialized but the upper 32 bits are

41
00:01:35,680 --> 00:01:44,159
initialized so specifically that would

42
00:01:37,640 --> 00:01:46,840
be 8 e C so 8 + 4 is C and so at 8 EC

43
00:01:44,159 --> 00:01:49,840
that is where this scalable this integer

44
00:01:46,840 --> 00:01:52,520
I this constant that we created a local

45
00:01:49,840 --> 00:01:56,360
variable for that I is stored on the

46
00:01:52,520 --> 00:01:58,280
stack at 7fs 8 EC so that's where you

47
00:01:56,360 --> 00:02:01,079
should see scalable and then down at the

48
00:01:58,280 --> 00:02:03,280
stack pointer again there's 64 bits of

49
00:02:01,079 --> 00:02:05,079
uninitialized space basically there

50
00:02:03,280 --> 00:02:07,320
seems to be some sort of over allocation

51
00:02:05,079 --> 00:02:09,039
it's not using all that space But it

52
00:02:07,320 --> 00:02:11,120
allocates that space nonetheless and

53
00:02:09,039 --> 00:02:13,800
then just never touches it okay so what

54
00:02:11,120 --> 00:02:17,480
are our takeaways from this single local

55
00:02:13,800 --> 00:02:19,599
variable o0 example well the first thing

56
00:02:17,480 --> 00:02:21,360
is local variables lead to an allocation

57
00:02:19,599 --> 00:02:22,800
of space on the stack and it's

58
00:02:21,360 --> 00:02:24,760
specifically within the function where

59
00:02:22,800 --> 00:02:27,879
the variable is scoped to so we have

60
00:02:24,760 --> 00:02:30,599
main I is scoped to be a local variable

61
00:02:27,879 --> 00:02:32,640
in main and consequently there is space

62
00:02:30,599 --> 00:02:34,120
allocated on the stack we saw before

63
00:02:32,640 --> 00:02:36,319
that there was space allocated on the

64
00:02:34,120 --> 00:02:37,840
stack for frame pointers when we had no

65
00:02:36,319 --> 00:02:40,519
variables whatsoever and we just

66
00:02:37,840 --> 00:02:42,519
returned a hard-coded constant and here

67
00:02:40,519 --> 00:02:45,040
we are seeing the allocation of space

68
00:02:42,519 --> 00:02:48,440
for both frame pointers and the local

69
00:02:45,040 --> 00:02:51,319
variable so stack pointer minus 32 so

70
00:02:48,440 --> 00:02:53,319
that is allocating space on the stack

71
00:02:51,319 --> 00:02:56,280
another takeaway is that it is over

72
00:02:53,319 --> 00:02:58,560
allocating space because we have minus

73
00:02:56,280 --> 00:03:01,599
32 that's the allocation of space which

74
00:02:58,560 --> 00:03:04,799
is a minus xx2 20 but there's hex 20

75
00:03:01,599 --> 00:03:08,680
reserved and only eight for a saved

76
00:03:04,799 --> 00:03:12,440
frame pointer and four for an integer I

77
00:03:08,680 --> 00:03:15,440
there's only hex C worth of data but

78
00:03:12,440 --> 00:03:17,280
there's hex 20 worth of reserved space

79
00:03:15,440 --> 00:03:19,000
so something is going on there we don't

80
00:03:17,280 --> 00:03:21,599
know exactly what but these are our

81
00:03:19,000 --> 00:03:24,280
stores one store of a dword that's the

82
00:03:21,599 --> 00:03:27,440
uh frame pointer and one store of a word

83
00:03:24,280 --> 00:03:29,879
that's int again words 32bit values in I

84
00:03:27,440 --> 00:03:32,360
32-bit value so we're already running a

85
00:03:29,879 --> 00:03:34,120
mystery listy that's keeping track of

86
00:03:32,360 --> 00:03:36,120
the fact that we don't know why GCC is

87
00:03:34,120 --> 00:03:37,720
over allocating space just for frame

88
00:03:36,120 --> 00:03:40,519
pointers and now we're going to add a

89
00:03:37,720 --> 00:03:42,000
mystery to that which is why is GCC over

90
00:03:40,519 --> 00:03:43,799
allocating space for a single local

91
00:03:42,000 --> 00:03:45,519
variable if we say that the frame

92
00:03:43,799 --> 00:03:48,640
pointer over allocation is that it

93
00:03:45,519 --> 00:03:51,599
allocates hex 16 for only hex 8 worth of

94
00:03:48,640 --> 00:03:55,799
frame pointers well then here it again

95
00:03:51,599 --> 00:03:58,040
allocated hex 16 for only four bytes of

96
00:03:55,799 --> 00:04:00,000
local variables so again some sort of

97
00:03:58,040 --> 00:04:01,840
allocation going on don't know why but

98
00:04:00,000 --> 00:04:03,760
we'll come back to it later another

99
00:04:01,840 --> 00:04:06,159
little takeaway that we can see from

100
00:04:03,760 --> 00:04:09,159
this is that it seems like GCC is

101
00:04:06,159 --> 00:04:11,280
referencing the local variables via a

102
00:04:09,159 --> 00:04:14,239
frame pointer relative address and what

103
00:04:11,280 --> 00:04:15,760
do I mean by that well we have this inti

104
00:04:14,239 --> 00:04:17,799
and we can see because this is

105
00:04:15,760 --> 00:04:21,720
unoptimized code that there is a very

106
00:04:17,799 --> 00:04:24,320
inefficient storing of a word to frame

107
00:04:21,720 --> 00:04:26,960
pointer minus 20 and then an immediate

108
00:04:24,320 --> 00:04:29,400
reading of that same frame pointer minus

109
00:04:26,960 --> 00:04:31,720
20 back into the exact same variable and

110
00:04:29,400 --> 00:04:33,639
that variable didn't change between you

111
00:04:31,720 --> 00:04:35,320
know at any point here so that was

112
00:04:33,639 --> 00:04:36,919
completely pointless that value was

113
00:04:35,320 --> 00:04:38,639
already in that variable but again

114
00:04:36,919 --> 00:04:42,039
that's what you get when you have

115
00:04:38,639 --> 00:04:45,280
explicitly unoptimized code so yes this

116
00:04:42,039 --> 00:04:46,880
seems to be the references to I storing

117
00:04:45,280 --> 00:04:49,199
the value into I and then pulling the

118
00:04:46,880 --> 00:04:52,120
value back out so that it can return it

119
00:04:49,199 --> 00:04:54,360
and those references to I are frame

120
00:04:52,120 --> 00:04:55,800
pointer relative different compilers can

121
00:04:54,360 --> 00:04:57,919
do this different ways they could have

122
00:04:55,800 --> 00:05:00,240
chosen to make it stack pointer relative

123
00:04:57,919 --> 00:05:02,120
instead of frame pointer relative but

124
00:05:00,240 --> 00:05:04,520
this is just a little inference about

125
00:05:02,120 --> 00:05:07,199
how GCC happens to be doing things here

126
00:05:04,520 --> 00:05:09,759
and then a final takeaway is that uh the

127
00:05:07,199 --> 00:05:12,240
frame pointer which is s0 doesn't point

128
00:05:09,759 --> 00:05:14,120
directly at the saved frame pointer so

129
00:05:12,240 --> 00:05:17,280
here was the storage of the frame

130
00:05:14,120 --> 00:05:20,280
pointer to stack plus 24 but then it

131
00:05:17,280 --> 00:05:22,880
takes stack plus 32 when it's setting a

132
00:05:20,280 --> 00:05:25,400
new frame pointer value and we saw that

133
00:05:22,880 --> 00:05:27,639
before up here if we looked at our

134
00:05:25,400 --> 00:05:29,880
diagram we saw that the save frame

135
00:05:27,639 --> 00:05:31,560
pointer is right here but the new frame

136
00:05:29,880 --> 00:05:34,759
pointer values right here right so that

137
00:05:31,560 --> 00:05:37,680
was storing of Stack plus 24 that was

138
00:05:34,759 --> 00:05:39,520
the storage but stack plus 32 is the new

139
00:05:37,680 --> 00:05:41,840
setting of the new frame pointer value

140
00:05:39,520 --> 00:05:44,000
so they seem to be off by8 some other

141
00:05:41,840 --> 00:05:46,840
architectures like x86 when you see you

142
00:05:44,000 --> 00:05:49,479
know 32-bit use of frame pointers always

143
00:05:46,840 --> 00:05:51,360
the frame pointer register is pointing

144
00:05:49,479 --> 00:05:52,919
directly at the saved frame pointer but

145
00:05:51,360 --> 00:05:54,319
that doesn't seem to be the case here

146
00:05:52,919 --> 00:05:56,919
with this

147
00:05:54,319 --> 00:05:58,880
code okay well let's go ahead and throw

148
00:05:56,919 --> 00:06:00,000
some chaos magic at this code and see

149
00:05:58,880 --> 00:06:01,479
what happens

150
00:06:00,000 --> 00:06:04,000
so here is the code that we've been

151
00:06:01,479 --> 00:06:07,000
looking at thus far and if we blow it

152
00:06:04,000 --> 00:06:09,160
away we see some of our old friends but

153
00:06:07,000 --> 00:06:11,560
we see a new friend as well the

154
00:06:09,160 --> 00:06:13,919
compressed ADI

155
00:06:11,560 --> 00:06:15,960
16sp so that is both a compressed

156
00:06:13,919 --> 00:06:17,560
instruction and some neonic that we

157
00:06:15,960 --> 00:06:20,960
haven't seen before we haven't seen an

158
00:06:17,560 --> 00:06:22,479
uncompressed ad I6 SP again just as a

159
00:06:20,960 --> 00:06:24,240
reminder part of the reason why I had

160
00:06:22,479 --> 00:06:25,720
that compression degression before is

161
00:06:24,240 --> 00:06:27,800
because I said you're going to keep

162
00:06:25,720 --> 00:06:30,039
seeing things compressed ADDI at the

163
00:06:27,800 --> 00:06:32,560
beginning compressed sdsp

164
00:06:30,039 --> 00:06:34,080
compressed at for SPN so you will just

165
00:06:32,560 --> 00:06:36,840
keep seeing those things over and over

166
00:06:34,080 --> 00:06:39,319
as we get rid of the aliases on the code

167
00:06:36,840 --> 00:06:41,120
because that's just what GCC happens to

168
00:06:39,319 --> 00:06:43,000
want to use for this unoptimized

169
00:06:41,120 --> 00:06:44,520
assembly so let's go ahead and drill

170
00:06:43,000 --> 00:06:48,840
down and figure out what

171
00:06:44,520 --> 00:06:51,199
C6 SP is referencing back to this handy

172
00:06:48,840 --> 00:06:55,120
diagram from the book once again we go

173
00:06:51,199 --> 00:06:57,680
ahead and we line it up and we see c.

174
00:06:55,120 --> 00:07:02,440
add I6 that's what we're looking for c.

175
00:06:57,680 --> 00:07:05,000
add I but no 16 moving down c. add I

176
00:07:02,440 --> 00:07:07,160
16sp Bingo perfect that's what we're

177
00:07:05,000 --> 00:07:10,960
looking for so what does it stand for

178
00:07:07,160 --> 00:07:13,039
compressed add immediate * 16 to stack

179
00:07:10,960 --> 00:07:14,919
pointer so we saw this one before

180
00:07:13,039 --> 00:07:17,240
compressed add immediate time 4 to stack

181
00:07:14,919 --> 00:07:19,479
pointer non-destructive so the

182
00:07:17,240 --> 00:07:21,680
differences are clearly a Time 16

183
00:07:19,479 --> 00:07:23,800
instead of a Time 4 and we talked about

184
00:07:21,680 --> 00:07:25,240
the scale by four last time so we might

185
00:07:23,800 --> 00:07:27,599
infer that there's going to be a scale

186
00:07:25,240 --> 00:07:29,680
by 16 going on this time and we talked

187
00:07:27,599 --> 00:07:31,000
about non-destructive last time so it's

188
00:07:29,680 --> 00:07:32,120
not actually going to change the stack

189
00:07:31,000 --> 00:07:34,080
pointer but we don't have

190
00:07:32,120 --> 00:07:35,560
non-destructive this time so that

191
00:07:34,080 --> 00:07:37,800
probably means it's going to change the

192
00:07:35,560 --> 00:07:41,520
stack pointer all right what does this

193
00:07:37,800 --> 00:07:44,240
expand out to well compressed ADI 16sp

194
00:07:41,520 --> 00:07:46,759
turns into ADI stack pointer stack

195
00:07:44,240 --> 00:07:49,120
pointer and 32 in this case whatever the

196
00:07:46,759 --> 00:07:52,159
constant is whatever the immediate is

197
00:07:49,120 --> 00:07:55,639
okay so we could write that like this

198
00:07:52,159 --> 00:07:59,400
the compressed add immediate time 16 to

199
00:07:55,639 --> 00:08:02,159
stack pointer this is CI SP and we're

200
00:07:59,400 --> 00:08:04,039
going to say a nonzero immediate 10bit

201
00:08:02,159 --> 00:08:06,680
value and it looks like it's signed we

202
00:08:04,039 --> 00:08:10,159
don't have a u there for unsigned so

203
00:08:06,680 --> 00:08:14,199
that expands to this and so we know how

204
00:08:10,159 --> 00:08:17,680
ADI behaves so we would do sp plus this

205
00:08:14,199 --> 00:08:19,759
sin extended 10bit value and it has to

206
00:08:17,680 --> 00:08:22,120
be non zero that's just a little caveat

207
00:08:19,759 --> 00:08:25,800
there so SP plus sin extended 10bit

208
00:08:22,120 --> 00:08:28,800
Value Store the result back into SP okay

209
00:08:25,800 --> 00:08:31,479
so we said NZ immediate 10 is a non zero

210
00:08:28,800 --> 00:08:33,479
10 bit signed immediate and because of

211
00:08:31,479 --> 00:08:36,320
this whole name we know that it is

212
00:08:33,479 --> 00:08:39,399
scaled by 16 scaled by 16 means that

213
00:08:36,320 --> 00:08:41,760
bits three 2 1 and Zer are all

214
00:08:39,399 --> 00:08:45,080
implicitly zero so that means you can

215
00:08:41,760 --> 00:08:47,480
only have multiples of 16 0 16 32 and so

216
00:08:45,080 --> 00:08:49,920
forth so there's really only six bits

217
00:08:47,480 --> 00:08:54,120
encoded in the instruction behind the

218
00:08:49,920 --> 00:08:56,040
scenes so we talked with the ci4 SPN

219
00:08:54,120 --> 00:08:58,600
that the N meant non-destructive but

220
00:08:56,040 --> 00:09:00,360
here the entire point of it is to change

221
00:08:58,600 --> 00:09:02,720
the stack point pointer so it's always

222
00:09:00,360 --> 00:09:04,519
going to be changing the stack pointer

223
00:09:02,720 --> 00:09:07,760
that is why it has the name stack

224
00:09:04,519 --> 00:09:10,200
pointer directly in the name of the

225
00:09:07,760 --> 00:09:11,360
instruction so looking back at our code

226
00:09:10,200 --> 00:09:13,240
we've got a little bit of Monkey

227
00:09:11,360 --> 00:09:17,440
Business going on here given the side

228
00:09:13,240 --> 00:09:20,120
eye to this ADDI 16sp because basically

229
00:09:17,440 --> 00:09:24,200
at the beginning we had this compressed

230
00:09:20,120 --> 00:09:27,640
ADI SP 32 so it was subtracting 32 and

231
00:09:24,200 --> 00:09:29,720
here at the end it is adding 32 but the

232
00:09:27,640 --> 00:09:32,920
question is why did it not just use

233
00:09:29,720 --> 00:09:34,760
another C add I because C add I can have

234
00:09:32,920 --> 00:09:37,680
signed values so why couldn't it just

235
00:09:34,760 --> 00:09:40,480
use a positive 32 and of course you know

236
00:09:37,680 --> 00:09:42,279
this also makes Thanos mad because there

237
00:09:40,480 --> 00:09:45,800
could have been perfectly balanced code

238
00:09:42,279 --> 00:09:48,920
here with a CI of 32 and a C addi of

239
00:09:45,800 --> 00:09:51,680
positive 32 so we don't like lack of

240
00:09:48,920 --> 00:09:53,800
balance do we no we don't so what's the

241
00:09:51,680 --> 00:09:56,360
reason for this well I don't strictly

242
00:09:53,800 --> 00:09:58,200
know but I am reasonably sure that this

243
00:09:56,360 --> 00:10:01,040
is fundamentally down to it being

244
00:09:58,200 --> 00:10:03,800
heuristics due to to unoptimized code

245
00:10:01,040 --> 00:10:05,560
the reality is if that we know the size

246
00:10:03,800 --> 00:10:08,640
of the stack pointer is changing by

247
00:10:05,560 --> 00:10:10,320
multiples of 16 compressed ADI 16 is

248
00:10:08,640 --> 00:10:13,120
actually a much better instruction to

249
00:10:10,320 --> 00:10:15,959
use because it can actually do a larger

250
00:10:13,120 --> 00:10:18,160
range due to that scaling by 16 so it

251
00:10:15,959 --> 00:10:19,720
can handle a much larger range of

252
00:10:18,160 --> 00:10:22,480
subtraction from the stack and addition

253
00:10:19,720 --> 00:10:24,760
to the stack and indeed we will actually

254
00:10:22,480 --> 00:10:26,600
see that later in the class we will see

255
00:10:24,760 --> 00:10:31,279
that they go ahead and get rid of this

256
00:10:26,600 --> 00:10:33,120
CI and start using CI I6 SP both at the

257
00:10:31,279 --> 00:10:34,880
function prologue and the function

258
00:10:33,120 --> 00:10:36,399
epilog and of course that's going to

259
00:10:34,880 --> 00:10:37,959
make Thanos happy because it'll be

260
00:10:36,399 --> 00:10:40,079
perfectly balanced like all things

261
00:10:37,959 --> 00:10:42,399
should be additionally one other little

262
00:10:40,079 --> 00:10:43,880
bit here if you looked closely you may

263
00:10:42,399 --> 00:10:47,160
have seen that when it was loading that

264
00:10:43,880 --> 00:10:51,519
immediate scalable here it had the upper

265
00:10:47,160 --> 00:10:54,560
20 bits in the LUI was scale b instead

266
00:10:51,519 --> 00:10:56,360
of scale a and there's the question of

267
00:10:54,560 --> 00:10:59,040
why is it not just using the upper 20

268
00:10:56,360 --> 00:11:01,600
literal bits of scalable and the bottom

269
00:10:59,040 --> 00:11:05,000
12 bits in the ADDI right why don't you

270
00:11:01,600 --> 00:11:08,320
use scale a well the answer is math the

271
00:11:05,000 --> 00:11:10,800
math is the reason because ADI is using

272
00:11:08,320 --> 00:11:12,959
a signed immediate as is obvious here by

273
00:11:10,800 --> 00:11:14,680
the fact that the disassembler is

274
00:11:12,959 --> 00:11:17,120
showing it in decimal and putting a

275
00:11:14,680 --> 00:11:21,200
negative there so the

276
00:11:17,120 --> 00:11:23,600
b1e BL e is s extended so it's 12 bits

277
00:11:21,200 --> 00:11:26,040
and that would be extended to be all fs

278
00:11:23,600 --> 00:11:27,680
and then b1e and if we take the two's

279
00:11:26,040 --> 00:11:31,480
complement of that we could see that

280
00:11:27,680 --> 00:11:33,000
that is 4 E2 which is 1250 which is

281
00:11:31,480 --> 00:11:35,399
exactly what they're showing here but

282
00:11:33,000 --> 00:11:39,720
the key thing is that if you did a load

283
00:11:35,399 --> 00:11:42,240
upper immediate a LUI on scale a then

284
00:11:39,720 --> 00:11:44,920
the bottom 12 bits would be set to zero

285
00:11:42,240 --> 00:11:47,399
and if you added that to this s extended

286
00:11:44,920 --> 00:11:49,079
b1e then you would get this result and

287
00:11:47,399 --> 00:11:54,880
you can check this with a calculator at

288
00:11:49,079 --> 00:11:57,240
your leisure You' get scale 9 b or b1e

289
00:11:54,880 --> 00:12:00,120
and that's obviously not correct so if

290
00:11:57,240 --> 00:12:02,279
instead we plus one this thing then when

291
00:12:00,120 --> 00:12:05,040
it's getting added to all of these FS

292
00:12:02,279 --> 00:12:08,399
then they'll all be carried over and

293
00:12:05,040 --> 00:12:11,279
ultimately you know so a plus f is going

294
00:12:08,399 --> 00:12:13,160
to carry over and be a b with a one and

295
00:12:11,279 --> 00:12:15,959
that one will carry over and make the

296
00:12:13,160 --> 00:12:17,760
essentially the zero and one carrying

297
00:12:15,959 --> 00:12:19,560
over and so forth and so the rest of the

298
00:12:17,760 --> 00:12:21,120
bits are not actually going to change so

299
00:12:19,560 --> 00:12:22,360
just in general you should expect that

300
00:12:21,120 --> 00:12:25,480
when you're seeing you know it's nice

301
00:12:22,360 --> 00:12:26,920
that the the disassembler told you like

302
00:12:25,480 --> 00:12:28,480
hey by the way whatever this thing just

303
00:12:26,920 --> 00:12:30,839
created that's going to be the the

304
00:12:28,480 --> 00:12:32,880
constant scalable but in general you

305
00:12:30,839 --> 00:12:36,440
should expect that if there's a Lou and

306
00:12:32,880 --> 00:12:38,720
an ADI then if the ADI is doing a signed

307
00:12:36,440 --> 00:12:40,880
value that would lead to this s

308
00:12:38,720 --> 00:12:43,760
extension then the constant for the

309
00:12:40,880 --> 00:12:46,440
upper 20 bits needs to be plus one in

310
00:12:43,760 --> 00:12:48,839
order to make the math all work out okay

311
00:12:46,440 --> 00:12:51,800
so we picked up the LUI assembly

312
00:12:48,839 --> 00:12:54,199
instruction in this example and we also

313
00:12:51,800 --> 00:12:57,040
picked up the load Word similar to the

314
00:12:54,199 --> 00:12:58,839
load dword we had before and the store

315
00:12:57,040 --> 00:13:00,839
word similar to the store dword from

316
00:12:58,839 --> 00:13:03,440
from before growing our instructions

317
00:13:00,839 --> 00:13:05,880
again we have the pop pop of store word

318
00:13:03,440 --> 00:13:08,959
and load Word down here in our load

319
00:13:05,880 --> 00:13:10,720
store area and then we have LUI off by

320
00:13:08,959 --> 00:13:12,959
its loansome for reasons that'll become

321
00:13:10,720 --> 00:13:14,720
clear later on and then we have the

322
00:13:12,959 --> 00:13:17,920
compressed ADI

323
00:13:14,720 --> 00:13:20,360
16sp which is really just a special form

324
00:13:17,920 --> 00:13:22,920
of AD ey that is operating specifically

325
00:13:20,360 --> 00:13:25,440
on the SP register and which is using an

326
00:13:22,920 --> 00:13:28,160
immediate that is scaled by 16 so that I

327
00:13:25,440 --> 00:13:30,160
can hit a larger range of possible

328
00:13:28,160 --> 00:13:32,120
additions in subtractions to the stack

329
00:13:30,160 --> 00:13:34,920
pointer and that's it for now and on we

330
00:13:32,120 --> 00:13:34,920
go

